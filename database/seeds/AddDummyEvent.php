<?php

use Illuminate\Database\Seeder;
use App\Event;

class AddDummyEvent extends Seeder
{
    /**
     * Run the database seeds.
     *
     * @return void
     */
    public function run()
    {
        $data = [
            ['title'=>'Rom Event', 'start_date'=>'2019-07-15', 'end_date'=>'2019-07-20'],

        	['title'=>'Coyala Event', 'start_date'=>'2019-07-25', 'end_date'=>'2019-07-30'],

        	['title'=>'Lara Event', 'start_date'=>'2019-08-15', 'end_date'=>'2019-08-20'],
        ];
        foreach($data as $key => $value) {
            Event::create($value);
        }
    }
}
