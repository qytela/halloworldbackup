<?php

/*
|--------------------------------------------------------------------------
| Web Routes
|--------------------------------------------------------------------------
|
| Here is where you can register web routes for your application. These
| routes are loaded by the RouteServiceProvider within a group which
| contains the "web" middleware group. Now create something great!
|
*/

Route::get('/', function () {
    return redirect('/user');
});

Route::get('/home', function() {
    return view('/layouts/home');
    // return redirect('/user');
});

Route::get('/livestream', function() {
    return view ('/layouts/livestream');
});

/**Cari TUTOR */
Route::get('/caritutor', function() {
    // return view ('/layouts/caritutor');
    return redirect('/user/caritutor');
});

/**Jadi Tutor */
Route::get('/jaditutor', function() {
    // return view('/layouts/jaditutor');
    return redirect('/user/jaditutor');
});

/** JADI MURID */
Route::get('/jadimurid', function() {
    // return view('/layouts/jadimurid');
    return redirect('/user/jadimurid');
});

/**ENTERPRISE */
Route::get('/enterprise', function() {
    return view('/layouts/enterprise');
});

/**IKLAN JADI TUTOR */
Route::get('/iklanjaditutor', function() {
    return view('/layouts/iklanjaditutor');
});

/**EVENTS VIEW SCHEDULE */
Route::get('events', 'EventController@index');
// Route::get('/viewschedule', function() {
//     return view('/layouts/events/viewschedule');
// });

/**SHOW PROFILE */
// Route::get('/profile', function() {
//     return view('/layouts/profiletutor');
// });
Route::get('/profile', function() {
    return view('/layouts/profiletutor');
});

// CHECK OUT PAGES
Route::get('/checkout', function() {
    return view('/layouts/checkout');
});


/**=====DASHBOARD ADMIN===============DASHBOARD ADMIN==========DASHBOARD ADMIN===========DASHBOARD ADMIN============ */

Route::get('/dashboardadmin', function() {
    return view('/layouts/dashboard/dashboard_admin/dashboardadmin');
});
Route::get('/admindaftartutor', function() {
    return view('/layouts/dashboard/dashboard_admin/daftartutor');
});
Route::get('/admindaftarmurid', function() {
    return view('/layouts/dashboard/dashboard_admin/daftarmurid');
});

/**=====DASHBOARD ADMIN===============DASHBOARD ADMIN==========DASHBOARD ADMIN===========DASHBOARD ADMIN============ */

/**=====DASHBOARD MURID===============DASHBOARD MURID==========DASHBOARD MURID===========DASHBOARD MURID============ */

Route::get('/dashboardmurid', function() {
    return view('/layouts/dashboard/dashboard_murid/dashboardmurid');
});
/**=====DASHBOARD MURID===============DASHBOARD MURID==========DASHBOARD MURID===========DASHBOARD MURID============ */

/**=====DASHBOARD TUTOR===============DASHBOARD TUTOR==========DASHBOARD TUTOR===========DASHBOARD TUTOR============ */

Route::get('/dashboardtutor', function() {
    return view('/layouts/dashboard/dashboard_tutor/dashboardtutor');
});
Route::get('/pesananmasuk', function() {
    return view('/layouts/dashboard/dashboard_tutor/pesananmasuk');
});
Route::get('/profiletutor', function() {
    return view('/layouts/dashboard/dashboard_tutor/profiletutor');
});
/**=====DASHBOARD TUTOR===============DASHBOARD TUTOR==========DASHBOARD TUTOR===========DASHBOARD TUTOR============ */


/**=====DASHBOARD MURID===============DASHBOARD MURID==========DASHBOARD MURID===========DASHBOARD MURID============ */
