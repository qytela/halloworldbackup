<?php

namespace Modules\Student\Http\Controllers;

use Illuminate\Http\Request;
use Illuminate\Http\Response;
use Illuminate\Routing\Controller;

use Modules\User\Entities\User;
use Modules\User\Entities\Order;
use Modules\Tutor\Entities\ProfileTutor;
use Modules\Student\Entities\ProfileStudent;

class StudentController extends Controller
{
    public function dashboardStudent(Request $request) {
        $dataTable = [];
        $dataOrder = Order::where('student_id', $request->session()->get('user_id'))->get();
        foreach ($dataOrder as $key => $value) {
            $dataTutor = ProfileTutor::where('user_id', $value->tutor_id)->first();
            $dataTable[] = [
                'id' => $value->id,
                'no_order' => $value->no_order,
                'transaction_date' => $value->transaction_date,
                'total_price' => $value->total_price,
                'name_tutor' => $dataTutor->fullname,
                'status' => $value->status,
                'time_create' => date('H:i:s')
            ];
        }
        $data['dataStudent'] = ProfileStudent::where('user_id', $request->session()->get('user_id'))->with('User')->first();
        $data['dataTable'] = $dataTable;
        return view('student::layouts.dashboard_murid.home', $data);
    }

    public function cancelOrder(Request $request) {
        $dataOrder = Order::where('id', $request->id)->first();
        if (!$dataOrder) {
            return redirect('/student/home');
        } else {
            Order::where('id', $request->id)->update(['status' => 4]);
            return redirect('/student/home');
        }
    }

    public function logout(Request $request) {
        $request->session()->flush();
        return redirect('/');
    }
}
