<!DOCTYPE html>
<html lang="en">

<head>
    <meta charset="utf-8">
    <meta http-equiv="X-UA-Compatible" content="IE=edge">
    <meta name="viewport" content="width=device-width, initial-scale=1">
    <title>Dashboard Murid</title>
    <!-- Favicon icon -->
    <!-- <link rel="icon" type="image/png" sizes="16x16" href="../assets/images/favicon.png"> -->
    <!-- Custom Stylesheet -->
    <link href="{{ asset('css/style.css') }}" rel="stylesheet">
    <script src="{{ asset('js/modernizr-3.6.0.min.js') }}"></script>

    <!-- Font Awesome -->
    <link rel="stylesheet" href="https://use.fontawesome.com/releases/v5.7.0/css/all.css">
    <link rel="stylesheet" href="https://use.fontawesome.com/releases/v5.7.2/css/all.css" integrity="sha384-fnmOCqbTlWIlj8LyTjo7mOUStjsKC4pOpQbqyi7RrhN7udi9RwhKkMHpvLbHG9Sr" crossorigin="anonymous">
</head>
<body>

    <div id="preloader">
        <div class="loader">
            <div class="loader__bar"></div>
            <div class="loader__bar"></div>
            <div class="loader__bar"></div>
            <div class="loader__bar"></div>
            <div class="loader__bar"></div>
            <div class="loader__ball"></div>
        </div>
    </div>

    <div id="main-wrapper">

        <!-- header -->
        <div class="header">
            <div class="nav-header">
                <div class="brand-logo">
                    <a href="index.html">
                        <!-- <i class="cc BTC"></i> -->
                        <img src="{{ asset('img/korea_tutor.png') }}" class="cc BTC"  alt="">
                        <span class="brand-title">
                            <img src="{{ asset('img/Logo-Halloworld.png') }}" alt="">
                        </span>
                    </a>
                </div>

                <div class="nav-control">
                    <div class="hamburger">
                        <span class="line"></span>
                        <span class="line"></span>
                        <span class="line"></span>
                    </div>
                </div>
            </div>

            <div class="header-content">
                <div class="header-left">
                    <ul>
                        <li class="icons position-relative">
                            <a href="javascript:void(0)">
                                <i class="icon-magnifier f-s-16"></i>
                            </a>
                            <div class="drop-down animated bounceInDown">
                                <div class="dropdown-content-body">
                                    <div class="header-search" id="header-search">
                                        <form action="#">
                                            <div class="input-group">
                                                <input type="text" class="form-control" placeholder="Search">
                                                <div class="input-group-append">
                                                    <span class="input-group-text">
                                                        <i class="icon-magnifier"></i>
                                                    </span>
                                                </div>
                                            </div>
                                        </form>
                                    </div>
                                </div>
                            </div>
                        </li>
                    </ul>

                </div>
                <div class="header-right">
                    <ul>
                        <li class="icons">
                            <a href="javascript:void(0)">
                                <i class="icon-bell f-s-18" aria-hidden="true"></i>
                                <div class="pulse-css"></div>
                            </a>
                            <div class="drop-down animated bounceInDown">
                                <div class="dropdown-content-heading">
                                    <span class="text-left">Recent Notifications</span>
                                </div>
                                <div class="dropdown-content-body">
                                    <ul>
                                        <li>
                                            <a href="#">
                                                <img class="pull-left m-r-10 avatar-img" src="../assets/images/avatar/1.jpg" alt="" />
                                                <div class="notification-content">
                                                    <small class="notification-timestamp pull-right">02:34 PM</small>
                                                    <div class="notification-heading">Mr. Saifun</div>
                                                    <div class="notification-text">5 members joined today </div>
                                                </div>
                                            </a>
                                        </li>
                                        <li>
                                            <a href="#">
                                                <img class="pull-left m-r-10 avatar-img" src="../assets/images/avatar/2.jpg" alt="" />
                                                <div class="notification-content">
                                                    <small class="notification-timestamp pull-right">02:34 PM</small>
                                                    <div class="notification-heading">Mariam</div>
                                                    <div class="notification-text">likes a photo of you</div>
                                                </div>
                                            </a>
                                        </li>
                                        <li>
                                            <a href="#">
                                                <img class="pull-left m-r-10 avatar-img" src="../assets/images/avatar/3.jpg" alt="" />
                                                <div class="notification-content">
                                                    <small class="notification-timestamp pull-right">02:34 PM</small>
                                                    <div class="notification-heading">Tasnim</div>
                                                    <div class="notification-text">Hi Teddy, Just wanted to let you ...</div>
                                                </div>
                                            </a>
                                        </li>
                                        <li>
                                            <a href="#">
                                                <img class="pull-left m-r-10 avatar-img" src="../assets/images/avatar/4.jpg" alt="" />
                                                <div class="notification-content">
                                                    <small class="notification-timestamp pull-right">02:34 PM</small>
                                                    <div class="notification-heading">Ishrat Jahan</div>
                                                    <div class="notification-text">Hi Teddy, Just wanted to let you ...</div>
                                                </div>
                                            </a>
                                        </li>
                                        <li class="text-center">
                                            <a href="#" class="more-link">See All</a>
                                        </li>
                                    </ul>
                                </div>
                            </div>
                        </li>
                        <li class="icons">
                            <a href="javascript:void(0)">
                                <i class="icon-envelope f-s-18" aria-hidden="true"></i>
                                <div class="pulse-css"></div>
                            </a>
                            <div class="drop-down animated bounceInDown">
                                <div class="dropdown-content-heading">
                                    <span class="text-left">2 New Messages</span>
                                </div>
                                <div class="dropdown-content-body">
                                    <ul>
                                        <li class="notification-unread">
                                            <a href="#">
                                                <img class="pull-left m-r-10 avatar-img" src="../assets/images/avatar/1.jpg" alt="" />
                                                <div class="notification-content">
                                                    <small class="notification-timestamp pull-right">02:34 PM</small>
                                                    <div class="notification-heading">Saiul Islam</div>
                                                    <div class="notification-text">Hi Teddy, Just wanted to let you ...</div>
                                                </div>
                                            </a>
                                        </li>
                                        <li class="notification-unread">
                                            <a href="#">
                                                <img class="pull-left m-r-10 avatar-img" src="../assets/images/avatar/2.jpg" alt="" />
                                                <div class="notification-content">
                                                    <small class="notification-timestamp pull-right">02:34 PM</small>
                                                    <div class="notification-heading">Ishrat Jahan</div>
                                                    <div class="notification-text">Hi Teddy, Just wanted to let you ...</div>
                                                </div>
                                            </a>
                                        </li>
                                        <li>
                                            <a href="#">
                                                <img class="pull-left m-r-10 avatar-img" src="../assets/images/avatar/3.jpg" alt="" />
                                                <div class="notification-content">
                                                    <small class="notification-timestamp pull-right">02:34 PM</small>
                                                    <div class="notification-heading">Saiul Islam</div>
                                                    <div class="notification-text">Hi Teddy, Just wanted to let you ...</div>
                                                </div>
                                            </a>
                                        </li>
                                        <li>
                                            <a href="#">
                                                <img class="pull-left m-r-10 avatar-img" src="../assets/images/avatar/4.jpg" alt="" />
                                                <div class="notification-content">
                                                    <small class="notification-timestamp pull-right">02:34 PM</small>
                                                    <div class="notification-heading">Ishrat Jahan</div>
                                                    <div class="notification-text">Hi Teddy, Just wanted to let you ...</div>
                                                </div>
                                            </a>
                                        </li>
                                        <li class="text-center">
                                            <a href="#" class="more-link">See All</a>
                                        </li>
                                    </ul>
                                </div>
                            </div>
                        </li>
                        <li class="icons">
                            <a href="javascript:void(0)">
                                <i class="icon-note f-s-18" aria-hidden="true"></i>
                                <div class="pulse-css"></div>
                            </a>
                            <div class="drop-down dropdown-task animated bounceInDown">
                                <div class="dropdown-content-heading">
                                    <span class="text-left">Task Update</span>
                                </div>
                                <div class="dropdown-content-body">
                                    <ul>
                                        <li>
                                            <a href="#">
                                                <div class="notification-content">
                                                    <small class="notification-timestamp pull-right">85% Complete</small>
                                                    <div class="notification-heading">Task One</div>
                                                    <div class="progress">
                                                        <div style="width: 85%;" aria-valuemax="100" aria-valuemin="0" aria-valuenow="85" role="progressbar" class="progress-bar progress-bar-success"></div>
                                                    </div>
                                                </div>
                                            </a>
                                        </li>
                                        <li>
                                            <a href="#">
                                                <div class="notification-content">
                                                    <small class="notification-timestamp pull-right">60% Complete</small>
                                                    <div class="notification-heading">Task Two</div>
                                                    <div class="progress">
                                                        <div style="width: 60%;" aria-valuemax="100" aria-valuemin="0" aria-valuenow="60" role="progressbar" class="progress-bar progress-bar-primary"></div>
                                                    </div>
                                                </div>
                                            </a>
                                        </li>
                                        <li>
                                            <a href="#">
                                                <div class="notification-content">
                                                    <small class="notification-timestamp pull-right">25% Complete</small>
                                                    <div class="notification-heading">Task Three</div>
                                                    <div class="progress">
                                                        <div style="width: 25%;" aria-valuemax="100" aria-valuemin="0" aria-valuenow="25" role="progressbar" class="progress-bar progress-bar-warning"></div>
                                                    </div>
                                                </div>
                                            </a>
                                        </li>
                                        <li>
                                            <a href="#">
                                                <div class="notification-content">
                                                    <small class="notification-timestamp pull-right">75% Complete</small>
                                                    <div class="notification-heading">Task Four</div>
                                                    <div class="progress">
                                                        <div style="width: 75%;" aria-valuemax="100" aria-valuemin="0" aria-valuenow="75" role="progressbar" class="progress-bar progress-bar-danger"></div>
                                                    </div>
                                                </div>
                                            </a>
                                        </li>
                                        <li class="text-center">
                                            <a href="#" class="more-link">See All</a>
                                        </li>
                                    </ul>
                                </div>
                            </div>
                        </li>
                        <li class="icons">
                            <a href="javascript:void(0)">
                                <i class="icon-user f-s-18" aria-hidden="true"></i>
                            </a>
                            <div class="drop-down dropdown-profile animated bounceInDown">
                                <div class="dropdown-content-body">
                                    <ul>
                                        <li>
                                            <a href="#">
                                                <i class="icon-envelope"></i>
                                                <span>Inbox</span>
                                            </a>
                                        </li>
                                        <li>
                                            <a href="#">
                                                <i class="icon-settings"></i>
                                                <span>Setting</span>
                                            </a>
                                        </li>
                                        <li>
                                            <a href="#">
                                                <i class="icon-lock"></i>
                                                <span>Lock Screen</span>
                                            </a>
                                        </li>
                                        <li>
                                            <a href="home">
                                                <i class="icon-power"></i>
                                                <span>Logout</span>
                                            </a>
                                        </li>
                                    </ul>
                                </div>
                            </div>
                        </li>
                    </ul>
                </div>
            </div>
        </div>
        <!-- #/ header -->


        <!-- sidebar -->
        <div class="nk-sidebar">
            <div class="nk-nav-scroll">

                <div class="nav-user">
                    <img src="{{ asset('img/reza.jpeg') }}" alt="" class="rounded-circle">
                    <h5 style="color: #F6921E;">{{ $dataStudent->fullname }}</h5>
                    <div class="row mt-5">
                        <div class="col-xl-3 pr-0">
                            <i class="fas fa-envelope"></i>
                        </div>
                        <div class="col-xl-9 pl-0">
                            <p class="text-left center__text_">{{ $dataStudent->User->email }}</p>
                        </div>
                    </div>
                    <div class="row mt-2">
                        <div class="col-xl-3 pr-0">
                            <i class="fas fa-mobile"></i>
                        </div>
                        <div class="col-xl-9 pl-0">
                            <p class="text-left center__text_">{{ $dataStudent->phone_number }}</p>
                        </div>
                    </div>
                    <div class="row mt-2">
                        <div class="col-xl-3 pr-0">
                            <i class="fas fa-venus-mars"></i>
                        </div>
                        <div class="col-xl-9 pl-0">
                            <p class="text-left center__text_">{{ $dataStudent->gender }}</p>
                        </div>
                    </div>
                    <div class="row mt-2">
                        <div class="col-xl-3 pr-0">
                            <i class="fas fa-calendar-alt"></i>
                        </div>
                        <div class="col-xl-9 pl-0">
                            <p class="text-left center__text_">{{ $dataStudent->birthday }}</p>
                        </div>
                    </div>
                    <div class="row mt-2">
                        <div class="col-xl-6">
                            <a href="" class="btn btn_edit_murid__">Edit</a>
                        </div>
                        <div class="col-xl-6">
                            <a href="" class="btn btn__perbarui__">Perbarui</a>
                        </div>
                    </div>
                </div>

                <ul class="metismenu" id="menu">
                    <li class="keluar_dashboard">
                        <a href="{{ URL::to('/student/logout') }}" class="d-flex justify-content-center mt-2">
                            <i class="icon-logout "></i>
                            <!-- <i class="fas fa-sign-out-alt"></i> -->
                            <span class="nav-text">Keluar</span>
                        </a>
                    </li>
                </ul>
            </div>
            <!-- #/ nk nav scroll -->
        </div>
        <!-- #/ sidebar -->

        <!-- content body -->
        <div class="content-body">
            <div class="container-fluid">
                <div class="row page-titles">
                    <div class="col-md-3 col-sm-4 col-lg-3 col-xl-2 p-r-0 align-self-center">
                        <h3 class="text-primary">Dashboard</h3>
                        <ol class="breadcrumb">
                            <li class="breadcrumb-item">
                                <a href="javascript:void(0)">Home</a>
                            </li>
                            <li class="breadcrumb-item active">Murid</li>
                        </ol>
                    </div>
                </div>
                @foreach ($dataTable as $rows)
                <div class="row">
                    <div class="col-xl-2">
                        <div class="card item_background__color__murid">
                            <div class="card-body">
                                <h1 class="text-center text-white font-weight-bold" style="font-size: 24px;">{{ date('d M', strtotime($rows['transaction_date'])) }}</h1>
                            </div>
                        </div>
                    </div>
                    <div class="col-xl-10">
                        <div class="card">
                            <div class="card-body">
                                <h4 class="card-title font-weight-bold">Kelas Dengan {{ $rows['name_tutor'] }} Pukul {{ $rows['time_create'] }} WIB</h4>
                                <p><small>Kelas dimulai dalam Pukul {{ $rows['time_create'] }}</small></p>
                            </div>
                        </div>
                    </div>
                </div>
                @endforeach

                <div class="row">
                    <div class="col-lg-12">
                        <div class="card">
                            <div class="card-body">
                                <h4 class="card-title dashboard_admin">Pesanan Kamu</h4>
                                <div class="table-responsive">
                                    <table id="myTable" class="table table-xs">
                                        <thead>
                                            <tr>
                                                <th class="text-center dashboard_admin">No Tagihan</th>
                                                <th class="text-center dashboard_admin">Tanggal Transaksi</th>
                                                <th class="text-center dashboard_admin">Total Tagihan</th>
                                                <th class="text-center dashboard_admin">Nama Tutor</th>
                                                <th class="text-center dashboard_admin">Status</th>
                                                <th class="text-center dashboard_admin">Action</th>
                                                <!-- <th class="text-right">Change % (7D)</th>
                                                <th class="text-right">Chart</th> -->
                                            </tr>
                                        </thead>
                                        <tbody>
                                        @foreach ($dataTable as $rows)
                                            <tr>
                                                <td>
                                                    <!-- <a href="#" class="color-warning"> Bitcoin</a> -->
                                                    <p class="text-muted text-center">{{ $rows['no_order'] }}</p>
                                                </td>
                                                <td class="text-center">
                                                    <p>
                                                        <!-- <span>$</span> 25,458.40 -->
                                                        {{ $rows['transaction_date'].' '.$rows['time_create'] }}
                                                    </p>
                                                </td>
                                                <td class="text-center">
                                                    <p>
                                                        <!-- <span>$</span> 232,078,267,295 -->
                                                        {{ number_format($rows['total_price']) }}
                                                    </p>
                                                </td>
                                                <td class="text-center">
                                                    <p>
                                                        <!-- <span>$</span> 23,568,900,000</p> -->
                                                        {{ $rows['name_tutor'] }}
                                                    </p>
                                                </td>
                                                <td class="text-center">
                                                    <p style="width: 200px;">
                                                        <!-- <span>$</span> 23,568,900,000</p> -->
                                                        @if ($rows['status'] === 3)
                                                            Room Untuk Pembelajaran Telah Siap.
                                                        @elseif ($rows['status'] === 2)
                                                            Proses Pemesanan Tutor Telah Dilakukan. Menunggu Konfirmasi Ketersediaan Tutor.
                                                        @elseif ($rows['status'] === 1)
                                                            Menunggu Pembayaran.
                                                        @endif
                                                    </p>
                                                </td>
                                                @if ($rows['status'] === 4)
                                                    <td class="text-center">
                                                        <p style="color: #E53935; font-weight: bold; font-size: 20px;">Batal</p>
                                                    </td>
                                                @elseif ($rows['status'] === 3)
                                                    <td class="text-center">
                                                        <div class="text-center">
                                                            <a href="" type="submit" class="btn btn_masuk__">Masuk</a>
                                                        </div>
                                                        <div class="text-center">
                                                            <a href="" type="submit" class="btn btn_reschedule__">Reschedule</a>
                                                        </div>
                                                    </td>
                                                @elseif ($rows['status'] === 2)
                                                    <td class="text-center">
                                                        <div class="text-center">
                                                            <!-- <a href="" type="submit" class="btn btn_batalkan__">Batalkan</a> -->
                                                            <p style="color: #F9A825; font-weight: bold; font-size: 20px;">Pending</p>
                                                        </div>
                                                    </td>
                                                @elseif ($rows['status'] === 1)
                                                    <td class="text-center">
                                                        <div class="text-center">
                                                            <a href="{{ URL::to('/student/cancel_order/'.$rows['id']) }}" class="btn btn_batalkan__">Batal</a>
                                                        </div>
                                                    </td>
                                                @endif
                                            </tr>
                                            @endforeach
                                        </tbody>
                                    </table>
                                </div>
                            </div>
                        </div>
                    </div>
                </div>
            </div>
            <!-- #/ container -->
        </div>
        <!-- #/ content body -->

        <!-- footer -->
        <div class="footer">
            <div class="copyright d-flex justify-content-center">
                <p>Copyright &copy;
                    <a href="home">Halloworld</a> 2019</p>
            </div>
        </div>
        <!-- #/ footer -->

    </div>

    <!-- Common JS -->
    <script src="{{ asset('plugins/common/common.min.js') }}"></script>
    <!-- Custom script -->
    <script src="{{ asset('js/custom.min.js') }}"></script>

    <!-- Morris Chart -->
    <script src="{{ asset('plugins/morris/raphael-min.js') }}"></script>
    <script src="{{ asset('plugins/morris/morris.js') }}"></script> 
    <!-- Custom dashboard script -->
    <script src="{{ asset('js/dashboard-1.js') }}"></script>
    <script src="{{ asset('js/dashboard-2.js') }}"></script>

    <!-- dataTables -->
    <script src="{{ asset('plugins/datatables/datatables.min.js') }}"></script>
    <script src="{{ asset('plugins/datatables/datatables-init.js') }}"></script>

    <script src="{{ asset('plugins/sparkline/jquery.sparkline.min.js') }}"></script>

    <script>
</script>

</body>

</html>