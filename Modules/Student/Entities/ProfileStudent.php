<?php

namespace Modules\Student\Entities;

use Illuminate\Database\Eloquent\Model;

use Modules\User\Entities\User;

class ProfileStudent extends Model
{
    protected $table = 'profile_student';

    protected $fillable = [
        'user_id',
        'fullname',
        'gender',
        'address',
        'birthday',
        'country',
        'about_me',
        'phone_number',
        'photo_profile'
    ];

    public function User() {
        return $this->belongsTo(User::class, 'user_id', 'id');
    }
}
