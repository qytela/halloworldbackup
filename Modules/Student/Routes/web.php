<?php

/*
|--------------------------------------------------------------------------
| Web Routes
|--------------------------------------------------------------------------
|
| Here is where you can register web routes for your application. These
| routes are loaded by the RouteServiceProvider within a group which
| contains the "web" middleware group. Now create something great!
|
*/

Route::group(['prefix' => 'student', 'middleware' => ['checkauth', 'checkrole:STUDENT']], function() {
    Route::get('/home', 'StudentController@dashboardStudent');
    Route::get('/cancel_order/{id}', 'StudentController@cancelOrder');
    Route::get('/logout', 'StudentController@logout');
});
