@extends('user::base.jaditutorbase')
@push('styles')

@endpush
@section('img_header')
<div id="background_pages_jaditutor"  class="background_orange_jaditutor">
    <h4>KAMU MEMILIKI KEMAMPUAN BERBAHASA ASING?</h4>
    <h6>DAFTARKAN DIRI KAMU SEGERA DAN DAPATKAN PENGHASILAN</h6>
</div>


@section('content')
<div id="section_1" class="inputan_jaditutor__pg mt-5">
    <div class="container test">
        @if ($errors->any())
        <div class="row form-group">
            <div class="alert alert-danger alert-dismissible fade show" role="alert">
                <strong>Gagal mendaftar!</strong> Periksa kembali inputan anda dengan benar.
                <button type="button" class="close" data-dismiss="alert" aria-label="Close">
                    <span aria-hidden="true">&times;</span>
                </button>
            </div>
        </div>
        @endif
        @if (Session::has('error'))
        <div class="row form-group">
            <div class="alert alert-danger alert-dismissible fade show" role="alert">
                <strong>Gagal mendaftar!</strong> {{ Session::get('error') }}.
                <button type="button" class="close" data-dismiss="alert" aria-label="Close">
                    <span aria-hidden="true">&times;</span>
                </button>
            </div>
        </div>
        @endif
        <form method="POST" action="do_register_tutor" enctype="multipart/form-data">
            @csrf
            <div class="row form-group">
                <div class="input__namadepan">
                    <p>NAMA DEPAN</p>
                    <input type="text" class="form-control" placeholder="" name="firstname">
                </div>
                <div class="input_namabelakang">
                    <p>NAMA BELAKANG</p>
                    <input type="text" class="form-control" placeholde="" name="lastname">
                </div>
            </div>
            <div class="row form-group">
                <div class="input__email mt-5">
                    <p>EMAIL</p>
                    <input type="email" class="form-control" id="email" placeholder="" name="email">
                </div>
                <div class="input__phone mt-5">
                    <p>Nomor Handphone</p>
                    <input type="number" class="form-control" placeholde="" name="phone_number">
                </div>
            </div>
            <div class="row form-group">
                <div class="choose__jenis-kelamin mt-5">
                    <p>JENIS KELAMIN</p>
                    <div class="row">
                        <div class="custom-control custom-radio mt-2 laki-laki-radiobutton">
                            <input type="radio" class="custom-control-input" id="defaultUnchecked" name="gender" value="LAKI-LAKI">
                            <label class="custom-control-label lbl-radiobutton-lakilaki" for="defaultUnchecked">LAKI-LAKI</label>
                        </div>

                        <div class="custom-control custom-radio mt-2 perempuan-radiobutton">
                            <input type="radio" class="custom-control-input" id="defaultChecked" name="gender" value="PEREMPUAN">
                            <label class="custom-control-label lbl-radiobutton-perempuan" for="defaultChecked">PEREMPUAN</label>
                        </div>
                    </div>
                </div>
            </div>
            <div class="row form-group">
                <div class="input_tgl-lahir mt-5">
                    <p>TEMPAT, TANGGAL LAHIR</p>
                
                    <div class="row">
                        <div class="col col-sm-5">
                            <div class="input_tempat">
                                <input type="text" class="form-control" name="address">
                            </div>
                        </div>
                        <div class="col col-sm-5">
                            <div class="input_tgl">
                                <input type="date" class="form-control"  id="tgl_lahir" name="birthday">
                            </div>
                        </div>
                    </div>
                </div>
            </div>
            <div class="row form-group">
                <div class="input_tgl-lahir mt-5">
                    <p>Negara</p>
                    <div class="row">
                        <div class="col col-sm-5">
                            <div class="input_tempat">
                                <input type="text" class="form-control" name="country">
                            </div>
                        </div>
                    </div>
                </div>
            </div>
            <div class="row form-group">
                <div class="upload__KTP-tutor mt-5">
                    <p>Lampirkan KTP</p>
                    <div class="file-upload-wrapper">
                        <input type="file" id="input-file-now-custom-2" class="file-upload"
                        data-height="500" data-default-file="https://mdbootstrap.com/img/Photos/Others/images/89.jpg" name="photo_ktp" />
                    </div>
                </div>
            </div>
            <div class="row form-group">
                <div class="input_bahasayang_dikuasai mt-5">
                    <p>BAHASA YANG DIKUASAI</p>
                    <div class="append_bahasa">
                        <select class="form-control mb-3" name="mastered_language[]">
                            <option value="English (British)">English (Bristish)</option>
                            <option value="English (American)">English (American)</option>
                            <option value="Chinese (Mandarin)">Chinese (Mandarin)</option>
                            <option value="Japanese">Japanese</option>
                            <option value="South Korea">South Korea</option>
                            <option value="Arabic">Arabic</option>
                            <option value="French">French</option>
                            <option value="German">German</option>
                            <option value="Indonesia">Indonesia</option>
                        </select>
                    </div>
                    <button type="button" class="btn btn_tambah_bahasa" id="tambah_bahasa">Tambah Bahasa Lain</button>
                </div>
            </div>
            <div class="row form-group">
                <div class="input_bahasayang_dikuasai mt-5">
                    <p>MATERI PENGAJARAN YANG DIKUASAI</p>
                    <div class="append_expertise">
                        <div class="row">
                            <div class="col">
                                <select class="form-control mb-3" id="inputGroupSelect01" name="materi[]">
                                    <option value="Academic">Bahasa Keperluan Akademik(Academic)</option>
                                    <option value="Career Builder">Bahasa Keperluan Karir(Career Builder)</option>
                                    <option value="Traveler">Bahasa Keperluan Perjalanan(Traveler)</option>
                                    <option value="Language Lover">Bahasa Keperluan Umum(Language Lover)</option>
                                </select>
                            </div>
                            <div class="col">
                                <select class="form-control mb-3" id="inputGroupSelect02" name="sub_materi[]">
                                    <option value="Materi Beginner">Materi Beginner</option>
                                    <option value="Materi Intermide">Materi Intermediate</option>
                                    <option value="Materi Profiecient">Materi Profiecient</option>
                                </select>
                            </div>
                        </div>
                    </div>
                    <button type="button" class="btn btn_tambah_bahasa" id="tambah_expertise">Tambah Kategori Lain</button>
                </div>
            </div>
            <!-- <div class="row form-group">
                <div class="input_bahasayang_dikuasai mt-5">
                    <p>MATERI PENGAJARAN YANG DIKUASAI</p>
                    <div class="append_expertise">
                        <select class="form-control mb-3" id="inputGroupSelect01" name="expertise[]">
                            <option value="Academic">Academic</option>
                            <option value="Career Builder">Career Builder</option>
                            <option value="Traveler">Traveler</option>
                            <option value="Language Lover">Language Lover</option>
                        </select>
                    </div>
                    <button type="button" class="btn btn_tambah_bahasa" id="tambah_expertise">Tambah Kategori Lain</button>
                </div>
            </div> -->
            <div class="row form-group">
                <div class="link_url-introduction mt-5">
                    <p>LINK URL INTRODUCTION</p>
                    <input type="text" class="form-control" placeholder="" name="link_introduction">
                </div>
            </div>
            <div class="row form-group">
                <div class="upload__image-tutor mt-5">
                    <p>Lampirkan Foto</p>
                    <p class="notify__lampiran_foto">(Ukuran Foto Persegi, Maks. 10 MB, JPG/PNG)</p>
                    <div class="file-upload-wrapper">
                        <input type="file" id="input-file-now-custom-2" class="file-upload"
                        data-height="500" data-default-file="https://mdbootstrap.com/img/Photos/Others/images/89.jpg" name="photo_profile" />
                    </div>
                </div>
            </div>
            <div class="row form-group">
                <div class="tentang__kamu mt-5">
                    <p>TENTANG KAMU</p>
                    <textarea class="form-control" cols="30" rows="10" name="about_me"></textarea>
                </div>
            </div>
            <div class="row form-group">
                <div class="data_pendidikan_terakhir mt-5">
                    <p>PENDIDIKAN TERAKHIR</p>
                </div>
            </div>
            <div class="row form-group">
                <div class="gelar__ mt-2">
                    <p>Gelar</p>
                    <input type="text" class="form-control" placeholder="" name="degree">
                </div>
            </div>
            <div class="row form-group">
                <div class="nama_institusi__ mt-5">
                    <p>Nama Institusi</p>
                    <input type="text" class="form-control" placeholder="" name="name_of_institusi">
                </div>
            </div>
            <div class="row form-group">
                <div class="jurusan__ mt-5">
                    <p>Jurusan</p>
                    <input type="text" class="form-control" placeholder="" name="jurusan">
                </div>
            </div>
            <div class="row form-group">
                <div class="tahun__masuk__ mt-5">
                    <p>Tahun Masuk</p>
                    <input type="date" class="form-control" placeholder="" name="year_entry_education">
                </div>
                <div class="tahun__keluar__ mt-5">
                    <p>Tahun Keluar</p>
                    <input type="date" class="form-control" placeholder="" name="year_out_education">
                </div>
            </div>
            <div class="row form-group">
                <div class="upload_ijazah_pendidikan_terakhir__ mt-5">
                    <p>Lampirkan Ijazah Pendidikan Terakhir</p>
                    <div class="file-upload-wrapper">
                        <input type="file" id="input-file-now-custom-2" class="file-upload" name="photo_ijazah" />
                    </div>
                </div>
            </div>
            <div class="row form-group">
                <div class="data_pengalaman_kerja mt-5">
                    <p>PENGALAMAN KERJA</p>
                </div>
            </div>
            <div class="row form-group">
                <div class="pengalaman__kerja__">
                    <div class="append_workexperience">
                        <div class="row">
                            <div class="jabatan__ col">
                                <p>Jabatan</p>
                                <input type="text" class="form-control" placeholder="" name="position[]"> 
                            </div>
                            <div class="instansi__  col">
                                <p>Instansi</p>
                                <input type="text" class="form-control" placeholder="" name="instansi[]">
                            </div>
                            <div class="tahun_masuk_pengalan_kerja__  col">
                                <p>Tahun Masuk</p>
                                <input type="date" class="form-control" placeholder="" name="year_entry[]">
                            </div>
                        </div>
                    </div>
                    <button type="button" class="btn btn_tambah__pengalaman_kerja__ mt-5" id="tambah_workexperience">Tambah Lainnya</button>
                </div>
            </div>
            <div class="row form-group">
                <div class="data_pengalaman_kerja mt-5">
                    <p>PENGALAMAN PELATIHAN DAN TES BAHASA</p>
                </div>
            </div>
            <div class="row form-group">
                <div class="pengalaman__kerja__ mt-2">
                    <div class="append_training">
                        <div class="row">
                            <div class="pelatihan__bahasa col">
                                <p>Nama Pelatihan / Tes Bahasa</p>
                                <input type="text" class="form-control" placeholder="" name="name_of_training[]">
                            </div>
                            <div class="tahun__pelatihan col">
                                <p>Tahun</p>
                                <input type="date" class="form-control" placeholder="" name="year_training[]">
                            </div>
                        </div>
                        <div class="row">
                            <div class="col">
                                <div class="upload_ijazah_pendidikan_terakhir__ mt-5">
                                    <p>Lampirkan Bukti Sertifikat</p>
                                    <div class="file-upload-wrapper">
                                        <input type="file" id="input-file-now-custom-2" class="file-upload" name="photo_certificate[]" />
                                    </div>
                                </div>
                            </div>
                        </div>
                    </div>
                    <button type="button" class="btn btn_tambah__pengalaman_kerja__ mt-5" id="tambah_training">Tambah Lainnya</button>
                </div>
            </div>
            <div class="row form-group">
                <div class="biaya__ mt-5">
                    <p>BIAYA PER/JAM</p>
                    <input type="number" class="form-control" placeholder="" name="hourly_fee">
                </div>
            </div>
            <div class="row form-group">
                <div class="ketersediaan_mengajar mt-5">
                    <p>KETERSEDIAN MENGAJAR</p>
                    <div class="append_jam">
                        <div class="row">
                            <div class="col mt-2">
                                <input type="date" name="date[]" id="dateformat" class="form-control" placeholder="">
                            </div>
                            <div class="col mt-2">
                                <input type="time" name="time[]" id="timeformat" class="form-control" placeholder="">
                            </div>
                        </div>
                    </div>
                    <button type="button" class="btn btn_tambah_jadwal mt-5" id="tambah_jam">Tambah Jadwal Lain</button>
                </div>
            </div>
            <div class="row form-group">
                <div class="data_pengiriman_insentif__mengajar mt-5">
                    <p>DATA PENGIRIMAN INSENTIF MENGAJAR</p>
                </div>
            </div>
            <div class="row form-group">
                <div class="nama__bank mt-5">
                    <p>NAMA BANK</p>
                    <input type="text" class="form-control" placeholder="" name="bank_name">
                </div>
            </div>
            <div class="row form-group">
                <div class="nomor__bank mt-5">
                    <p>NOMOR REKENING</p>
                    <input type="number" class="form-control" placeholder="" name="no_rekening">
                </div>
            </div>
            <div class="row form-group">
                <div class="atas__nama_bank mt-5">
                    <p>ATAS NAMA REKENING</p>
                    <input type="text" class="form-control" placeholder="" name="card_holder_name">
                </div>
            </div>
            <div class="row form-group">
                <div class="upload__buku_rekening-tutor mt-5">
                    <p>Lampirkan Buku Rekening</p>
                    <div class="file-upload-wrapper">
                        <input type="file" id="input-file-now-custom-2" class="file-upload"
                        data-height="500" data-default-file="https://mdbootstrap.com/img/Photos/Others/images/89.jpg" name="photo_book_rekening" />
                    </div>
                </div>
            </div>
            <div class="row form-group">
                <div class="kata__sandi__ mt-5">
                    <p>KATA SANDI</p>
                    <input type="password" class="form-control" placeholder="" name="password">
                </div>
            </div>
            <div class="row form-group">
                <div class="konfirmasi__kata_sandi__ mt-5">
                    <p>KONFIRMASI KATA SANDI</p>
                    <input type="password" class="form-control" placeholder="" name="re_password">
                </div>
            </div>
            <div class="row form-group">
                <div class="col">
                    <div class="check__ mt-5 mb-3">
                        <input type="checkbox" name="check" id="check">
                        <label for="check" class="lbl-text-check">TERMS AND CONDITION</label>
                    </div>
                </div>
            </div>
            <div class="row form-group">     
                <div class="button_submit">
                    <button type="submit" class="btn btn_submit__">SUBMIT</button>
                </div>
            </div>
        </form>
    </div>
</div>
@endsection

@push('scripts')
<script src="https://cdnjs.cloudflare.com/ajax/libs/select2/4.0.7/js/select2.min.js"></script>
    <script>
        $(document).ready(function() {
            var limit = 1;

            $('#tambah_bahasa').click(function() {
                $('.append_bahasa').append(`
                    <select class="form-control mb-3" name="mastered_language[]">
                        <option value="#">Pilih Bahasa</option>
                        <option value="English (British)">English (Bristish)</option>
                        <option value="English (American)">English (American)</option>
                        <option value="Chinese (Mandarin)">Chinese (Mandarin)</option>
                        <option value="Japanese">Japanese</option>
                        <option value="South Korea">South Korea</option>
                        <option value="Arabic">Arabic</option>
                        <option value="French">French</option>
                        <option value="German">German</option>
                        <option value="Indonesia">Indonesia</option>
                    </select>
                `);
            });

            $('#tambah_expertise').click(function() {
                $('.append_expertise').append(`
                    <div class="row">
                        <div class="col">
                            <select class="form-control mb-3" id="inputGroupSelect01" name="materi[]">
                                <option value="">Pilih Materi</option>
                                <option value="Academic">Bahasa Keperluan Akademik(Academic)</option>
                                <option value="Career Builder">Bahasa Keperluan Karir(Career Builder)</option>
                                <option value="Traveler">Bahasa Keperluan Perjalanan(Traveler)</option>
                                <option value="Language Lover">Bahasa Keperluan Umum(Language Lover)</option>
                            </select>
                        </div>
                        <div class="col">
                            <select class="form-control mb-3" id="inputGroupSelect02" name="sub_materi[]">
                                <option value="">Pilih Materi</option>
                                <option value="Materi Beginner">Materi Beginner</option>
                                <option value="Materi Intermide">Materi Intermediate</option>
                                <option value="Materi Profiecient">Materi Profiecient</option>
                            </select>
                        </div>
                    </div>
                `);
            });

            $('#tambah_jam').click(function() {
                $('.append_jam').append(`
                    <div class="row">
                        <div class="col mt-2">
                            <input type="date" name="date[]" id="dateformat" class="form-control" placeholder="">
                        </div>
                        <div class="col mt-2">
                            <input type="time" name="time[]" id="timeformat" class="form-control" placeholder="">
                        </div>
                    </div>
                `);
            });

            $('#tambah_workexperience').click(function() {
                $('.append_workexperience').append(`
                    <div class="row">
                        <div class="jabatan__ col">
                            <p>Jabatan</p>
                            <input type="text" class="form-control" placeholder="" name="position[]">
                        </div>
                        <div class="instansi__  col">
                            <p>Instansi</p>
                            <input type="text" class="form-control" placeholder="" name="instansi[]">
                        </div>
                        <div class="tahun_masuk_pengalan_kerja__  col">
                            <p>Tahun Masuk</p>
                            <input type="date" class="form-control" placeholder="" name="year_entry[]">
                        </div>
                    </div>
                `);
            });

            $('#tambah_training').click(function() {
                if (limit >= 3) return;
                $('.append_training').append(`
                    <div class="row">
                        <div class="pelatihan__bahasa col">
                            <p>Nama Pelatihan / Tes Bahasa</p>
                            <input type="text" class="form-control" placeholder="" name="name_of_training[]">
                        </div>
                        <div class="tahun__pelatihan col">
                            <p>Tahun</p>
                            <input type="date" class="form-control" placeholder="" name="year_training[]">
                        </div>
                    </div>
                    <div class="row">
                        <div class="col">
                            <div class="upload_ijazah_pendidikan_terakhir__ mt-5">
                                <p>Lampirkan Bukti Sertifikat</p>
                                <div class="file-upload-wrapper">
                                    <input type="file" id="input-file-now-custom-2" class="file-upload" name="photo_certificate[]" />
                                </div>
                            </div>
                        </div>
                    </div>
                `);
                limit++;
            });
        });
    </script>
@endpush