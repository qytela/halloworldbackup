@extends('user::base.apps')
@push('styles')

@endpush
@section('img_header')
<div id="background_pages_jaditutor"  class="background_orange_jaditutor">
    <h4>TINGKATKAN KEMAMPUAN BERBAHASA ASING KAMU</h4>
    <h6>DAFTARKAN DIRI KAMU SEGERA</h6>
</div>


@section('content')
<div id="section_1" class="inputan_jaditutor__pg mt-5">
    <div class="container test">
        @if ($errors->any())
        <div class="row form-group">
            <div class="alert alert-danger alert-dismissible fade show" role="alert">
                <strong>Gagal mendaftar!</strong> Periksa kembali inputan anda dengan benar.
                <button type="button" class="close" data-dismiss="alert" aria-label="Close">
                    <span aria-hidden="true">&times;</span>
                </button>
            </div>
        </div>
        @endif
        @if (Session::has('error'))
        <div class="row form-group">
            <div class="alert alert-danger alert-dismissible fade show" role="alert">
                <strong>Gagal mendaftar!</strong> {{ Session::get('error') }}.
                <button type="button" class="close" data-dismiss="alert" aria-label="Close">
                    <span aria-hidden="true">&times;</span>
                </button>
            </div>
        </div>
        @endif
        <form method="POST" action="{{ URL::to('/user/do_register_student') }}">
        @csrf
            <div class="row">
                <div class="input__namadepan">
                    <p>NAMA DEPAN</p>
                    <input type="text" class="form-control" placeholder="" name="firstname">
                </div>
                <div class="input_namabelakang">
                    <p>NAMA BELAKANG</p>
                    <input type="text" class="form-control" placeholde="" name="lastname">
                </div>
            </div>
            <div class="row">
                <div class="input__email mt-5">
                    <p>EMAIL</p>
                    <input type="email" name="email" class="form-control" id="email" placeholder="">
                </div>
                <div class="input__phone mt-5">
                    <p>Nomor Handphone</p>
                    <input type="number" class="form-control" placeholde="" name="phone_number">
                </div>
            </div>
            <div class="row">
                <div class="choose__jenis-kelamin mt-5">
                    <p>JENIS KELAMIN</p>
                    <div class="row">
                        <div class="custom-control custom-radio mt-2 laki-laki-radiobutton">
                            <input type="radio" class="custom-control-input" id="defaultUnchecked" name="gender" value="LAKI-LAKI">
                            <label class="custom-control-label lbl-radiobutton-lakilaki" for="defaultUnchecked">LAKI-LAKI</label>
                        </div>

                        <div class="custom-control custom-radio mt-2 perempuan-radiobutton">
                            <input type="radio" class="custom-control-input" id="defaultChecked" name="gender" value="PEREMPUAN">
                            <label class="custom-control-label lbl-radiobutton-perempuan" for="defaultChecked">PEREMPUAN</label>
                        </div>
                    </div>
                </div>
            </div>
            <div class="row">
                <div class="input_tgl-lahir mt-5">
                    <p>TEMPAT, TANGGAL LAHIR</p>
                
                    <div class="row">
                        <div class="col col-sm-5">
                            <div class="input_tempat">
                                <input type="text" class="form-control" name="address">
                            </div>
                        </div>
                        <div class="col col-sm-5">
                            <div class="input_tgl">
                                <input type="date" name="birthday" class="form-control"  id="tgl_lahir">
                            </div>
                        </div>
                    </div>
                </div>
            </div>
            <div class="row">
                <div class="kata__sandi__ mt-5">
                    <p>KATA SANDI</p>
                    <input type="password" class="form-control" placeholder="" name="password">
                </div>
                <div class="konfirmasi__kata_sandi__ mt-5">
                    <p>KONFIRMASI KATA SANDI</p>
                    <input type="password" class="form-control" placeholder="" name="re_password">
                </div>
            </div>
            <div class="row">
                <div class="col">
                    <div class="check__ mt-5 mb-3">
                        <input type="checkbox" name="check" id="check">
                        <label for="check" class="lbl-text-check">TERMS AND CONDITION</label>
                    </div>
                </div>
            </div>
            <div class="row">     
                <div class="button_submit">
                    <button type="submit" class="btn btn_submit__">SUBMIT</button>
                </div>
            </div>
        </form>
    </div>
</div>

@push('scripts')
    <script type="text/javascript">
        $('.file-upload').file_upload();
        $('#dateformat').datepicker({
            format: 'dd-mm-yyyy'
        });
    </script>
@endpush