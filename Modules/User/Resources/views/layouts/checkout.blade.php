@extends('base.apps')
@push('styles')
<style>
</style>

@endpush
@section('img_header')
<div id="background_pages_profiletutor"  class="background_orange_profile">
    
</div>
@endsection


@section('content')
<div id="section_1" class="main__section_1_checkout_pages">
    <div class="container">
        <h4 class="text_payment__checkout">Payment Details</h4>
            <div class="card card__checkOut__">
                <div class="card-body">
                    <div class="kode__checkOut__right">
                        <div class="row justify-content-end">
                            <div class="col-md-2 pr-0">
                                <p>Nomor Tagihan</p>
                            </div>
                            <div class="col-xs-2">
                                <p>:</p>
                            </div>
                            <div class="col-sm-6 col-xs-5 col-md-3">
                                <p>{{ $noOrder }}</p>
                            </div>
                        </div>
                        <div class="row justify-content-end">
                            <div class="col-md-2 pr-0">
                                <p>Tanggal Transaksi</p>
                            </div>
                            <div class="col-xs-2">
                                <p>:</p>
                            </div>
                            <div class="col-sm-6 col-xs-5 col-md-3">
                                <p>{{ date('Y M d') }}</p>
                            </div>
                        </div>
                    </div>
                    <form method="POST" action="{{ URL::to('/user/do_checkout') }}">
                        @csrf
                        <input type="hidden" value="{{ Request::segment(3) }}" name="id">
                        <input type="hidden" value="{{ $noOrder }}" name="no_order">
                        <hr class="mb-0">
                        <div class="table-responsive">
                            <table class="table ">
                                <thead>
                                    <tr>
                                        <th class="text-center">Tutor</th>
                                        <th class="text-center">Bahasa Yang Dipelajari</th>
                                        <th class="text-center">Biaya Pengajaran/Jam</th>
                                        <th class="text-center">Durasi/Jam</th>
                                        <th class="text-center">Total</th>
                                        <!-- <th>Action</th> -->
                                    </tr>
                                </thead>
                                <tbody>
                                    <tr class="text-center">
                                        <td>
                                            <img src="{{ asset('img/reza.jpeg') }}" class="rounded__image_checkout">
                                            <p class="mt-2">Jongsuk Kim</p>
                                        </td>
                                        <td class="pt-4 d-flex justify-content-center">
                                            <select class="form-control mb-3 custon__width__dropdownCheckout" name="language">
                                                @php
                                                    $explode = explode(",", $profileTutor->mastered_language);
                                                @endphp
                                                @foreach ($explode as $rows)
                                                    <option value="{{ $rows }}">{{ $rows }}</option>
                                                @endforeach
                                                <!-- <option value="1">English (Bristish)</option>
                                                <option value="2">English (American)</option>
                                                <option value="3">Chinese (Mandarin)</option> -->
                                            </select>
                                        </td>
                                        <td class="pt-4">Rp {{ number_format($profileTutor->hourly_fee) }}</td>
                                        <td class="pt-4">1/Jam</td>
                                        <td class="pt-4">Rp {{ number_format($profileTutor->hourly_fee) }}</td>
                                    </tr>
                                </tbody>
                            </table>
                        </div>
                        <hr>
                        <div class="penjumlahan__total__checkout">
                            <div class="container">
                                <div class="row">
                                    <div class="col-xs-12 col-md-7 col-lg-6">
                                        <div class="pencarian__kode__promo__">
                                            <p>Kode Promo</p>
                                            <div class="input__text__kode mode__float__left">
                                                <input type="number" class="form-control">
                                            </div>
                                            <div class="button__search__kode mode__float__right">
                                                <button type="submit" class="btn btn_gunakan__kode_promo">Gunakan</button>
                                            </div>
                                        </div>
                                    </div>
                                    <div class="col-xs-12 col-md-7 col-lg-6">
                                        <div class="list__total__checkout__item__ mr-4">
                                            <div class="row justify-content-end text-right">
                                                <div class="col-sm-8 col-md-5 col-xs-5">
                                                    <p style=" font-weight: bold;">Subtotal</p>
                                                </div>
                                                <div class="col-md-3 pl-0 pr-0">
                                                    <p>Rp {{ number_format($profileTutor->hourly_fee) }}</p>
                                                </div>
                                            </div>
                                            <!-- <div class="row justify-content-end text-right">
                                                <div class="col-sm-8 col-md-5 col-xs-5">
                                                    <p style=" font-weight: bold;">Kode Unik</p>
                                                </div>
                                                <div class="col-md-3 pl-0 pr-0">
                                                    <p>Rp 62</p>
                                                </div>
                                            </div>
                                            <div class="row justify-content-end text-right">
                                                <div class="col-sm-8 col-md-5 col-xs-5">
                                                    <p style="font-weight: bold;">Diskon</p>
                                                </div>
                                                <div class="col-md-3 pl-0 pr-0">
                                                    <p style="color: #EF5350;">Rp 100.000</p>
                                                </div>
                                            </div> -->
                                            <div class="row justify-content-end text-right">
                                                <div class="col-sm-8 col-md-5 col-xs-5">
                                                    <p style="font-weight: bold;">Total Tagihan</p>
                                                </div>
                                                <div class="col-md-3 pl-0 pr-0">
                                                    <p style="font-weight: bold; font-size: 20px;">Rp {{ number_format($profileTutor->hourly_fee) }}</p>
                                                </div>
                                            </div>
                                        </div>
                                    </div>
                                </div>
                            </div>
                        </div>
                        <hr>
                        <div class="list_item__dipilih__">
                            <div class="container">
                                <div class="row">
                                    <div class="col-xs-6 col-md-5 col-lg-4">
                                        <div class="list__jadwal_pengajaran__">
                                            <p style="font-weight:bold;">Jadwal Pengajaran</p>
                                            <div class="top__header__list_item_dipilih">
                                                Pilih Tanggal dan Waktu
                                                <div class="icon__top_header">
                                                    <i class="fas fa-calendar-alt"></i>
                                                </div>
                                            </div>
                                            <div class="body__header_list__item__dipilih">
                                                <select class="form-control mb-3" name="date_time">
                                                @foreach ($avaibilityTeachingTutor as $rows)
                                                    <option value="{{ $rows->date.'/'.$rows->time }}">{{ $rows->date.'/'.$rows->time }}</option>
                                                @endforeach
                                                    <!-- <option value="1">30 Juli 2019, Pukul 19.00 WIB</option>
                                                    <option value="2">30 Juli 2019, Pukul 19.00 WIB</option>
                                                    <option value="3">30 Juli 2019, Pukul 19.00 WIB</option> -->
                                                </select>
                                                <!-- <p style="font-size: 14px;">30 Juli 2019, Pukul 19.00 WIB</p> -->
                                            </div>
                                        </div>
                                    </div>

                                    <div class="col-xs-6 col-md-5 col-lg-4">
                                        <div class="list__jadwal_pengajaran__">
                                            <p style="font-weight:bold;">Jadwal Pengajaran</p>
                                            <div class="top__header__list_item_dipilih">
                                                Pilih Tanggal dan Waktu
                                                <div class="icon__top_header">
                                                    <i class="fas fa-caret-down"></i>
                                                </div>
                                            </div>
                                            <div class="body__header_list__item__dipilih">
                                                <select class="form-control mb-3" name="materi">
                                                @foreach ($expertiseTutor as $rows)
                                                    <option value="{{ $rows->materi }}">{{ $rows->materi }}</option>
                                                @endforeach
                                                    <!-- <option value="1">Bahasa Keperluan Karir (Career Builder)</option>
                                                    <option value="2">Bahasa Percakapan Perjalanan (Traveller)</option>
                                                    <option value="3">Bahasa Percakapan Umum (Language Lover)</option> -->
                                                </select>
                                            </div>
                                        </div>
                                    </div>
                                    
                                    <div class="col-xs-6 col-md-5 col-lg-4">
                                        <div class="list__jadwal_pengajaran__" style="margin-top: 40px;">
                                            
                                            <div class="top__header__list_item_dipilih">
                                                Pilih Tanggal dan Waktu
                                                <div class="icon__top_header">
                                                    <i class="fas fa-caret-down"></i>
                                                </div>
                                            </div>
                                            <div class="body__header_list__item__dipilih">
                                                <select class="form-control mb-3" name="sub_materi">
                                                @foreach ($expertiseTutor as $rows)
                                                    <option value="{{ $rows->sub_materi }}">{{ $rows->sub_materi }}</option>
                                                @endforeach
                                                    <!-- <option value="1">Materi Intermediete</option>
                                                    <option value="2">Materi Profiecient</option> -->
                                                </select>
                                            </div>
                                        </div>
                                    </div>
                                </div>
                            </div>
                        </div>
                        <hr>
                        <div class="pilih_metode__pembayaran_dicheckout">
                            <p>Pilih Cara Pembayaran</p>
                            <div class="form-group">
                                <label class="radio-inline mr-3 off__position">
                                    <input type="radio" class="off__position" name="optradio">Bank BNI <br> No Rek 182020202 <br>A/N Halloworld Indonesia</label>
                                <label class="radio-inline mr-3 off__position">
                                    <input type="radio" class="off__position" name="optradio"> Bank BCA <br> No Rek 12314141 <br> A/N Halloworld Indoensia</label>
                            </div>
                            <div class="form-group">
                                <label class="radio-inline mr-3 off__position">
                                    <input type="radio" class="off__position" name="optradio">Bank Mandiri <br> No Rek 182020202 <br>A/N Halloworld Indonesia</label>
                                <label class="radio-inline mr-3 off__position">
                                    <input type="radio" class="off__position" name="optradio"> Bank Syariah Mandiri <br> No Rek 12314141 <br> A/N Halloworld Indoensia</label>
                            </div>
                        </div>
                        <div class="click__event__checkout mt-5 mr-4">
                            <div class="row justify-content-end">
                                <button type="submit" class="btn btn__pesan__checkout">Pesan</button>
                                <button type="submit" class="btn btn__batal__checkout">Batal</button>
                            </div>
                        </div>
                    </form>
                </div>
            </div>
        </div>
    </div>
</div>
@endsection