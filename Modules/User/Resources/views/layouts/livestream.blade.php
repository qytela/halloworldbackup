@extends('user::base.livebase')

@section('content')
<div id="live-section-1">
    <div class="bg_livestream">
        <div class="header custom-header-top full">
            <div class="row justify-content-between">
                <div class="col col-md-6">
                    <!-- <div id="local-videos-container"></div> -->
                    <div class="coba" style="margin-left: 20px; margin-top: 50px; ">
                    <!-- <iframe width="840" height="413" 
                    src="https://www.youtube.com/embed/FcOctsNXyjk" 
                    frameborder="0" allow="accelerometer; autoplay; encrypted-media; gyroscope; picture-in-picture" 
                    allowfullscreen></iframe> -->              
                    <iframe
                    src="https://tokbox.com/embed/embed/ot-embed.js?embedId=3e43909b-f791-4e74-b379-52420af3aa10&room={{ $roomId }}&iframe=true"
                    width=840
                    height=413
                    scrolling="auto"
                    allow="microphone; camera"
                    ></iframe>
                    </div>
                    <div class="row">
                        <img class="img__user_active user_img" src="https://i.pinimg.com/originals/54/6e/6d/546e6d4c6ce4322e6aa3b2f8ca73ac28.jpg" alt="">
                        <div class="details_user_on__roomlivestream">
                            <div class="user__name_active">
                                Reza Ahmad Fauzi
                            </div>
                            <div class="on__user">
                                Online
                            </div>
                        </div>
                    </div>
                </div>
                <div class="col col-md-6">
                    <div class="chatting_messages mr-auto">
                        <div class="row">
                            <div class="img_tutor_header">
                                <img class="rounded-circle user_img" src="https://i.pinimg.com/originals/54/6e/6d/546e6d4c6ce4322e6aa3b2f8ca73ac28.jpg" alt="">
                            </div>
                            <div class="available_tutor">
                                <p class="tutor_style_name">Dafina</p>
                                <p class="available_active">Online</p>
                            </div>
                            <!-- <div class="action_btn__close">
                                <button type="submit" class="icon_btn_close"><i class="fas fa-window-close"></i></button>
                            </div> -->
                        </div>
                        <hr style="margin-top: -8px;">
                        <div class="message_on___ test1">
                            <div class="row">
                                <div class="img_message">
                                    <img class="rounded-circle user_img" src="https://i.pinimg.com/originals/54/6e/6d/546e6d4c6ce4322e6aa3b2f8ca73ac28.jpg" alt="">
                                </div>
                                <div class="active__message">
                                    <p class="tutor_style_name">Dafina</p>
                                    <p class="available_active">Online</p>
                                </div>
                                <!-- <div class="action_btn__close">
                                    <button type="submit" class="icon_btn_close"><i class="fas fa-window-close"></i></button>
                                </div> -->
                            </div>
                            <div class="row">
                                <div class="img_message">
                                    <img class="rounded-circle user_img" src="https://i.pinimg.com/originals/54/6e/6d/546e6d4c6ce4322e6aa3b2f8ca73ac28.jpg" alt="">
                                </div>
                                <div class="active__message">
                                    <p class="tutor_style_name">Dafina</p>
                                    <p class="available_active">Online</p>
                                </div>
                                <!-- <div class="action_btn__close">
                                    <button type="submit" class="icon_btn_close"><i class="fas fa-window-close"></i></button>
                                </div> -->
                            </div>
                            <div class="row">
                                <div class="img_message">
                                    <img class="rounded-circle user_img" src="https://i.pinimg.com/originals/54/6e/6d/546e6d4c6ce4322e6aa3b2f8ca73ac28.jpg" alt="">
                                </div>
                                <div class="active__message">
                                    <p class="tutor_style_name">Dafina</p>
                                    <p class="available_active">Online</p>
                                </div>
                                <!-- <div class="action_btn__close">
                                    <button type="submit" class="icon_btn_close"><i class="fas fa-window-close"></i></button>
                                </div> -->
                            </div>
                            <div class="row">
                                <div class="img_message">
                                    <img class="rounded-circle user_img" src="https://i.pinimg.com/originals/54/6e/6d/546e6d4c6ce4322e6aa3b2f8ca73ac28.jpg" alt="">
                                </div>
                                <div class="active__message">
                                    <p class="tutor_style_name">Dafina</p>
                                    <p class="available_active">Online</p>
                                </div>
                                <!-- <div class="action_btn__close">
                                    <button type="submit" class="icon_btn_close"><i class="fas fa-window-close"></i></button>
                                </div> -->
                            </div>
                            <div class="row">
                                <div class="img_message">
                                    <img class="rounded-circle user_img" src="https://i.pinimg.com/originals/54/6e/6d/546e6d4c6ce4322e6aa3b2f8ca73ac28.jpg" alt="">
                                </div>
                                <div class="active__message">
                                    <p class="tutor_style_name">Dafina</p>
                                    <p class="available_active">Online</p>
                                </div>
                                <!-- <div class="action_btn__close">
                                    <button type="submit" class="icon_btn_close"><i class="fas fa-window-close"></i></button>
                                </div> -->
                            </div>
                            <div class="row">
                                <div class="img_message">
                                    <img class="rounded-circle user_img" src="https://i.pinimg.com/originals/54/6e/6d/546e6d4c6ce4322e6aa3b2f8ca73ac28.jpg" alt="">
                                </div>
                                <div class="active__message">
                                    <p class="tutor_style_name">Dafina</p>
                                    <p class="available_active">Online</p>
                                </div>
                                <!-- <div class="action_btn__close">
                                    <button type="submit" class="icon_btn_close"><i class="fas fa-window-close"></i></button>
                                </div> -->
                            </div>
                            <div class="row">
                                <div class="img_message">
                                    <img class="rounded-circle user_img" src="https://i.pinimg.com/originals/54/6e/6d/546e6d4c6ce4322e6aa3b2f8ca73ac28.jpg" alt="">
                                </div>
                                <div class="active__message">
                                    <p class="tutor_style_name">Dafina</p>
                                    <p class="available_active">Online</p>
                                </div>
                                <!-- <div class="action_btn__close">
                                    <button type="submit" class="icon_btn_close"><i class="fas fa-window-close"></i></button>
                                </div> -->
                            </div>
                            
                            <div class="row">
                                <div class="img_message">
                                    <img class="rounded-circle user_img" src="https://i.pinimg.com/originals/54/6e/6d/546e6d4c6ce4322e6aa3b2f8ca73ac28.jpg" alt="">
                                </div>
                                <div class="active__message">
                                    <p class="tutor_style_name">Dafina</p>
                                    <p class="available_active">Online</p>
                                </div>
                                <!-- <div class="action_btn__close">
                                    <button type="submit" class="icon_btn_close"><i class="fas fa-window-close"></i></button>
                                </div> -->
                            </div>
                        </div>
                        <div class="messages__actions">
                            <div class="row">
                                <button class="btn_attachment_links style_btn_attach" type="submit">
                                    <i class="fas fa-paperclip"></i>
                                </button>
                                <input type="text" class="form-control input__message_action_user" placeholder="Enter your message">
                            </div>
                        </div>
                    </div>
                </div>
            </div>
            
            
        </div>
    </div>
</div>
@endsection
