@extends('base.apps')
@push('styles')
<style>
</style>

@endpush
@section('img_header')
<div id="background_pages_profiletutor"  class="background_orange_profile">
    
</div>


@section('content')
<div id="section_1" class="main_content_parent_profile_1">
    <div class="container">
        <div class="row">
            <div class="col-md-8">
                <div class="card card__profile__">
                    <div class="card-body">
                            <div class="list">
                                <iframe width="690" height="415"
                                    class="video__profile_tutor"
                                    src="https://www.youtube.com/embed/FcOctsNXyjk" 
                                    frameborder="0" allow="accelerometer; autoplay; 
                                    encrypted-media; gyroscope; picture-in-picture"
                                    allowfullscreen>
                                </iframe>
                            </div>
                            
                            <div class="row">
                                <div class="col-md-3 pr-0 pl-0">
                                    <div class="d-flex justify-content-center">
                                        <img src="{{ asset('images/'.$profileTutor->photo_profile) }}" class="img-thumbnail img__photo_profile__" alt="">
                                    </div>
                                    <div class="d-flex justify-content-center">
                                        <!-- <small class="text-muted" style="text-align: center;">BRITISH ENGLISH</small> -->
                                    </div>
                                    <div class="d-flex justify-content-center">
                                        <!-- <small class="text-muted" style="text-align: center;">05:49 UTC +06:00</small> -->
                                    </div>
                                </div>
                                <div class="col-md-8 pl-0 pr-0">
                                    <div class="row col">
                                        <p style="margin-top: 20px; font-weight: bold; color: #B5B5B5; font-size: 26px;">{{ $profileTutor->fullname }}</p>
                                        <div class="d-flex justify-content-flex-end mr-auto" style="position: absolute; right: 5px; margin-top: 10px;">
                                            <a href="{{ URL::to('/user/checkout/'.$profileTutor->user_id) }}" class="btn btn-pesan__profile">PESAN</a>
                                        </div>
                                    </div>
                                    <p style="margin-top: -15px; margin-bottom: 0px;">{{ $profileTutor->status }}</p>
                                    <p style="margin-top: -0px; margin-bottom: 0px; color: #F6921E; font-weight: 'bold;">Harga</p>
                                    <p style="color: #F6921E; font-weight: 'bold; margin-top: -25px;">Rp. {{ number_format($profileTutor->hourly_fee) }}/Jam</p>
                                    <!-- <div class="row col-md-8" style="margin-top: -6px;">
                                        <p style="color: blue;">Tutor Komunitas</p>
                                        <p style="margin-left: 5px; margin-right: 5px; color: blue">From</p>
                                        <p class="text-muted">Cardiff, England</p>
                                    </div> -->
                                    <div class="row col-md-8" style="margin-top: -6px;">
                                        <p class="text-muted" style="margin-right: 5px;">Tutor Bahasa</p>
                                        <p class="text-muted">{{ $profileTutor->mastered_language }}</p>
                                    </div>
                                        <div class="feature__tutor-penilaian" style="margin-top: -10px;">
                                            <div class="row">
                                                <div class="penilaian__tutor-modal">
                                                    <small class="penilai__feature">Penilaian</small>
                                                    <div class="d-flex justify-content-center">
                                                        <div class="row">
                                                            <div id="rateYo" class="rating__jquery-modal mb-4"></div>
                                                            <div style="font-size: 12px; font-weight: bold ">{{ $profileTutor->rating }}</div>
                                                        </div>
                                                    </div>
                                                </div>

                                                <div class="murid__tutor-modal">
                                                    <small class="penilai__feature">Murid</small>
                                                    <div class="d-flex justify-content-center">
                                                        <div class="row">
                                                            
                                                            <div style="font-size: 16px; font-weight: bold ">{{ $profileTutor->students }}</div>
                                                        </div>
                                                    </div>
                                                </div>

                                                <div class="kelas__tutor-modal">
                                                    <small class="penilai__feature">Kelas</small>
                                                    <div class="d-flex justify-content-center">
                                                        <div class="row">
                                                        
                                                            <div style="font-size: 16px; font-weight: bold ">{{ $profileTutor->class }}</div>
                                                        </div>
                                                    </div>
                                                </div>
                                            </div>
                                        </div>
                                </div>
                            </div>
                        <!-- <img src="{{ asset('img/tutor_kelasbahasa_1.jpg') }}" class="img-thumbnail img__photo_profile__" alt="">
                                    <small class="text-muted" style="text-align: center;">BRITISH ENGLISH</small>
                                    <small class="text-muted" style="text-align: center;">05:49 UTC +06:00</small>
                                 -->
                      
                    </div>
                </div>
            </div>
            <div class="col-md-4">
                <div class="card responsive_mobile">
                    <div class="card-body">
                        <div class="d-flex justify-content-center">
                            <p>Harga Per/Jam Mulai Dari</p>
                        </div>
                        <div class="row d-flex justify-content-center">
                            <p style="font-weight: bold; font-size: 22px;">RP</p>
                            <p style="font-weight: bold; margin-left: 5px; font-size: 22px;">{{ number_format($profileTutor->hourly_fee) }}</p>
                        </div>
                        <div class="d-flex justify-content-center">
                            <button type="submit" class="btn btn-jadwalkelas">JADWAL KELAS</button>
                        </div>
                        <div class="d-flex justify-content-center">
                            <button type="submit" class="btn btn-hubungitutor">HUBUNGI TUTOR</button>
                        </div>
                        <div class="d-flex justify-content-center">
                            <button type="submit" class="btn btn-lihatketersediaan">LIHAT KETERSEDIAAN</button>
                        </div>
                        <div class="d-flex justify-content-center">
                            <p style="margin-top: 15px; ">KEBIJAKAN PEMBATALAN</p>
                        </div>
                        <div class="d-flex justify-content-center">
                            <p style="text-align: center;">Bagikan Profile Tutor<br/>Dengan Teman-Teman <br/>Kamu</p>
                        </div>
                        <div class="d-flex justify-content-center">
                            <a href="" class="btn btn-outline__bagikan__">BAGIKAN</a>
                        </div>
                    </div>
                </div>
            </div>
        </div>
    </div>
</div>
<div id="section_2" class="mt-5">
    <div class="container">
        <div class="row center__custom">
            <div class="col-md-4 pr-0 __kemampuan_1">
                <p class="text-muted text_center_responsive" style="font-weight:bold">PENGALAMAN PELATIHAN :</p>
            </div>
            <div class="col-md-8 pr-0">
                <p class="text-muted text_center_responsive" >{{ $profileTutor->mastered_language }}</p>
            </div>
        </div>
        <div class="row center__custom">
            <div class="col-md-4 pr-0 __kemampuan_1">
                <p class="text-muted text_center_responsive" style="font-weight:bold">MATERI PENGAJARAN :</p>
            </div>
            <div class="col-md-8 pr-0 ">
                @foreach ($expertiseTutor as $rowExpertise)
                    <p class="text-muted text_center_responsive">{{ $rowExpertise->materi.', ' }}</p>
                @endforeach
                <!-- <p class="text-muted text_center_responsive">BUSINESS, CONVERSATION</p> -->
            </div>
        </div>
        <!-- <div class="row center__custom">
            <div class="col-md-4 pr-0  __kemampuan_1">
                <p class="text-muted text_center_responsive" style="font-weight:bold">BERGABUNG SEJAK :</p>
            </div>
            <div class="col-md-8 pr-0 ">
                <p class="text-muted text_center_responsive">18 MARET 2019</p>
            </div>
        </div> -->
       
        <div class="line__ mt-2" style="height: 2; background-color: #F6921E;"></div>
        <div class="tentang__tutor mt-5">
            <p class="text-muted" style="font-weight:bold">TENTANG TUTOR</p>
            <p style="margin-top: 5px; color: #F6921E">TENTANG SAYA</p>
            <p class="text-muted">{{ $profileTutor->about_me }}
            </p>
            <a href="" class="text_show__more">+Lanjutkan Membaca</a>
        </div>
        <div class="line__ mt-2" style="height: 2; background-color: #F6921E;"></div>
        <div class="education__ mt-5">
            <div class="row ml-1">
                <img src="{{asset('img/icon_education.png')}}" class="icon__education" alt="">
                <p style="margin-top: 8px; color: #F6921E; margin-left: 15px;">Education</p>
            </div>
            <div class="row ml-1">
                <div class="col-sm-2 pl-0">
                    <small style="color: #F6921E">{{ $lastEducationTutor->year_entry }}</small>
                    <small style="margin-right: 5px; margin-left: 5px; color: #F6921E">-</small>
                    <small style="color: #F6921E">{{ $lastEducationTutor->year_out }}</small>
                </div>
                <div class="col-md-8 pl-0">
                    <div class="list__education mb-2">
                        <small class="list__judul__"><strong>{{ $lastEducationTutor->degree }}</strong></small>
                    </div>
                    <small class="sub_list__judul">{{ $lastEducationTutor->name_of_institusi }}</small>
                </div>
            </div>
        </div>
        <div class="education__ mt-5">
            <div class="row ml-1">
                <img src="{{asset('img/icon_work_experience.png')}}" class="icon__education" alt="">
                <p style="margin-top: 8px; color: #F6921E; margin-left: 15px;">Work Experience</p>
            </div>
            @foreach ($workExperienceTutor as $rows)
            <div class="row ml-1">
                <div class="col-sm-2 pl-0">
                    <small style="color: #F6921E">{{ $rows->year_entry }}</small>
                </div>
                <div class="col-md-8 pl-0">
                    <div class="list__education mb-2">
                        <small class="list__judul__"><strong>{{ $rows->position }}</strong></small>
                    </div>
                    <small class="sub_list__judul">{{ $rows->instansi }}</small>
                </div>
            </div>
            @endforeach
        </div>
        <div class="education__ mt-5">
            <div class="row ml-1">
                <img src="{{asset('img/icon_certification.png')}}" class="icon__education" alt="">
                <p style="margin-top: 8px; color: #F6921E; margin-left: 15px;">Certifications</p>
            </div>
            @foreach ($trainingTutor as $rows)
            <div class="row ml-1">
                <div class="col-sm-2 pl-0">
                    <small style="color: #F6921E">{{ $rows->year }}</small>
                </div>
                <div class="col-md-8 pl-0">
                    <div class="list__education mb-2">
                        <small class="list__judul__"><strong>{{ $rows->name_of_training }}</strong></small>
                    </div>
                </div>
            </div>
            @endforeach
        </div>
        <div class="line__ mt-2" style="height: 2; background-color: #F6921E;"></div>
        <div class="history__tutor__ mt-5">
            <p class="text-muted" style="font-weight:bold">HISTORI PENGAJARAN</p>
            <div class="row">
                <div class="col-md-4">
                    <div class="d-flex justify-content-center">
                        <h4 style="font-size: 28px; font-weight: bold; color: #F6921E">{{ $profileTutor->class }}</h4>
                    </div>
                    <div class="d-flex justify-content-center">
                        <p style="font-size: 20px; font-weight: bold; color: #F6921E">KELAS SELESAI</p>
                    </div>
                </div>
                <div class="col-md-4">
                    <div class="d-flex justify-content-center">
                        <h4 style="font-size: 28px; font-weight: bold; color: #F6921E">100%</h4>
                    </div>
                    <div class="d-flex justify-content-center">
                        <p style="font-size: 20px; font-weight: bold; color: #F6921E">TINGKAT RESPON</p>
                    </div>
                </div>
                <div class="col-md-4">
                    <div class="d-flex justify-content-center">
                        <h4 style="font-size: 28px; font-weight: bold; color: #F6921E">99%</h4>
                    </div>
                    <div class="d-flex justify-content-center">
                        <p style="font-size: 20px; font-weight: bold; color: #F6921E">TINGKAT KEHADIRAN</p>
                    </div>
                </div>
            </div>
        </div>
        <div class="line__ mt-2" style="height: 2; background-color: #F6921E;"></div>
        <div class="review__tutor mt-5">
        
            <div class="row">
                <div class="col-md-3">
                    <p style="text-align: right;" class="text_center_responsive">RAHMAN</p>
                    <div class="row mr-auto right__position_custom center__custom">
                        <p style="text-align: right; margin-right: 5px;" class="text_center_responsive">1</p>
                        <p style="text-align: right" class="text_center_responsive">KELAS</p>
                    </div>
                    <p style="text-align: right; margin-bottom: -3px;" class="text_center_responsive">ENGLISH</p>
                    <p style="text-align: right; font-size: 12px;" class="text-muted text_center_responsive">1 MARET 2019</p>
                </div>
                <div class="line__custom mt-2" style="width: 2px; background-color: #F6921E; height: 50px;"></div>
  
                
                <div class="col-md-7">
                    <div class="row">
                        <img src="{{ asset('img/cari_tutor_2.png') }}" class="img-thumbnail img__review" alt="">
                    
                    
                        <p style="width: 400px; margin-left: 5" class="custom__margin_top">
                            Setelah mengikuti kelas MS Britney, KEMAMPUAN
                            berbahasa inggris saya meningkat pesat. Tutor sangat membantu
                            dan respontif
                        </p>
                    </div>
                </div>
            </div>
        </div>
        <div class="line__ mt-2" style="height: 2; background-color: #F6921E;"></div>
    </div>
</div>


<!--=========MODAL===============MODAL===========MODAL==========MODAL============MODAL======-->
<div class="modal fade" id="perkenalantutor" tabindex="-1" role="dialog" ria-labelledby="exampleModalLabel" aria-hidden="true">
    <div class="modal-dialog modal-dialog_caritutor_perkenalan" role="document">
        <div class="modal-content modal__cariTutor-content">
            <div class="modal-header">
                <div class="d-flex justify-content-center video-container">
                <iframe 
                    class="video__yt"
                    width="600" height="370" 
                    src="https://www.youtube.com/embed/FcOctsNXyjk" frameborder="0" 
                    allow="accelerometer; autoplay; encrypted-media; 
                    gyroscope; picture-in-picture" allowfullscreen>
                </iframe>
                    
                </div>
                <button 
                    type="button" class="close btn_close-modal-caritutor" style="margin-left: -10px;" data-dismiss="modal" aria-label="Close">
                    <span aria-hidden="true">&times;</span>
                </button>
            </div>
            <div class="modal-body">
                <div class="my__information__modal">
                    <div class="row">
                        <div class="modal__profile-image__tutor">
                            <img src="{{ asset('img/tutor_kelasbahasa_3.jpg') }}" class="img__modal-tutor img-thumbnail" alt="img-thumbnail">
                            <div class="d-flex justify-content-center">
                                <div class="row">
                                    <small class="text-muted">BRITISH ENGLISH</small>
                                </div>
                            </div>
                            <div class="d-flex justify-content-center">
                                <div class="row">
                                    <small class="modal_tutor-item-waktu text-muted">05:49</small>
                                    <small class="text-muted" style="margin-left: 4px; margin-right: 4px;">UTC+</small>
                                    <small class="text-muted">06:00</small>
                                </div>
                            </div>
                        </div>
                        <div class="modal__profile-name">
                            <div class="row">
                                <h4 class="name__tutor-modal">BRITNEY</h4>
                                <div class="mr-auto" style="right: 15px; position: absolute;">
                                    <button type="submit" class="btn btn-modal__pesan btn_text-modal">
                                        <small class="text-button_modal">PESAN</small>
                                    </button>
                                </div>
                            </div>
                            <small class="text-muted">Online</small>
                            <div class="row">
                                <small style="margin-left: 15px; margin-top:15px; color: #01579B;" class="tutor___desc">Tutor Komunitas</small>
                                <small style="margin-left: 5px; margin-right: 5px; margin-top:15px; color: #01579B;" class="">From</small>
                                <small class="text-muted" style="margin-top:15px;">Cardiff,</small>
                                <small class="text-muted" style="margin-top:15px;">England</small>
                            </div>
                            <div class="row">
                                <small class="text-muted" style="margin-left: 15px; margin-top:15px;">Tutor Bahasa</small>
                                <small class="text-muted" style="margin-left: 5px; margin-top:15px;">Inggris (British)</small>
                            </div>
                            <div class="feature__tutor-penilaian">
                                <div class="row">
                                    <div class="penilaian__tutor-modal">
                                        <small class="penilai__feature">Penilaian</small>
                                        <div class="d-flex justify-content-center">
                                            <div class="row">
                                                <div id="rateYo" class="rating__jquery-modal mb-4"></div>
                                                <div style="font-size: 12px; font-weight: bold ">5.0</div>
                                            </div>
                                        </div>
                                    </div>

                                    <div class="murid__tutor-modal">
                                        <small class="penilai__feature">Murid</small>
                                        <div class="d-flex justify-content-center">
                                            <div class="row">
                                                
                                                <div style="font-size: 16px; font-weight: bold ">71</div>
                                            </div>
                                        </div>
                                    </div>

                                    <div class="kelas__tutor-modal">
                                        <small class="penilai__feature">Kelas</small>
                                        <div class="d-flex justify-content-center">
                                            <div class="row">
                                               
                                                <div style="font-size: 16px; font-weight: bold ">50</div>
                                            </div>
                                        </div>
                                    </div>
                                </div>
                            </div>
                        </div>
                    </div>
                </div>
            </div>
            <div class="modal-footer">
                <a href="#">Lupa Password?</a>
                
            </div>
        </div>
    </div>
</div>
<div class="modal fade" id="doc-cal" role="dialog">
  <div class="modal-dialog">

    <!-- Modal content-->
    <div class="modal-content">
      <div class="modal-header">
        <button type="button" class="close" data-dismiss="modal">&times;</button>
        <h4 class="modal-title">Doctor's Appointments</h4>
      </div>

      <div class="modal-body">
        <div id='calendar'></div>
      </div>
      <div class="modal-footer">
        <!-- <input type="submit" class="btn btn-warning" id="doc-update" value="Update"> -->
        <button type="button" class="btn btn-default" id="plist-close" data-dismiss="modal">Close</button>
      </div>
    </div>
  </div>
</div>

<!--==========MODAL=======MODAL============MODAL===========MODAL================MODAL=======-->


@push('scripts')
    <script type="text/javascript">
        document.addEventListener('DOMContentLoaded', function() {
            var calendarEl = document.getElementById('calendar');

            var calendar = new FullCalendar.Calendar(calendarEl, {
            plugins: [ 'interaction', 'dayGrid', 'timeGrid', 'list' ],
            header: {
                left: 'prev,next today',
                center: 'title',
                right: 'dayGridMonth,timeGridWeek,timeGridDay,listMonth'
            },
            defaultDate: '2019-06-12',
            navLinks: true, // can click day/week names to navigate views
            businessHours: true, // display business hours
            editable: true,
            events: [
                {
                title: 'Business Lunch',
                start: '2019-06-03T13:00:00',
                constraint: 'businessHours'
                },
                {
                title: 'Meeting',
                start: '2019-06-13T11:00:00',
                constraint: 'availableForMeeting', // defined below
                color: '#257e4a'
                },
                {
                title: 'Conference',
                start: '2019-06-18',
                end: '2019-06-20'
                },
                {
                title: 'Party',
                start: '2019-06-29T20:00:00'
                },

                // areas where "Meeting" must be dropped
                {
                groupId: 'availableForMeeting',
                start: '2019-06-11T10:00:00',
                end: '2019-06-11T16:00:00',
                rendering: 'background'
                },
                {
                groupId: 'availableForMeeting',
                start: '2019-06-13T10:00:00',
                end: '2019-06-13T16:00:00',
                rendering: 'background'
                },

                // red areas where no events can be dropped
                {
                start: '2019-06-24',
                end: '2019-06-28',
                overlap: false,
                rendering: 'background',
                color: '#ff9f89'
                },
                {
                start: '2019-06-06',
                end: '2019-06-08',
                overlap: false,
                rendering: 'background',
                color: '#ff9f89'
                }
            ]
            });

            calendar.render();
        });
        
        $(function () {
 
            $("#rateYo").rateYo({
                rating: 2,
                fullStar: true,
                starWidth: "14px",
                readOnly: true
            });
            $("#rating__tutor").rateYo({
                rating: 5,
                fullStar: true,
                starWidth: "18px",
            });
            $("#rating__tutor_2").rateYo({
                rating: 4,
                fullStar: true,
                starWidth: "18px",
            });
            $("#rating__tutor_3").rateYo({
                rating: 3,
                fullStar: true,
                starWidth: "18px",
            });
        });
        // Getter
        var normalFill = $("#rateYo").rateYo("option", "fullStar"); //returns true
        
        // Setter
        $("#rateYo").rateYo("option", "fullStar", true); //returns a jQuery Element
    </script>
@endpush