<?php

/*
|--------------------------------------------------------------------------
| Web Routes
|--------------------------------------------------------------------------
|
| Here is where you can register web routes for your application. These
| routes are loaded by the RouteServiceProvider within a group which
| contains the "web" middleware group. Now create something great!
|
*/

Route::prefix('user')->group(function() {
    Route::get('/', 'UserController@index');
    Route::get('/jaditutor', 'UserController@registerTutor');
    Route::get('/jadimurid', 'UserController@registerStudent');
    Route::get('/caritutor', 'UserController@cariTutor');
    Route::get('/profile/{id}', 'UserController@profileTutor');
    Route::get('/livestream/{id}', 'RealtimeController@VidCallChat');

    Route::post('/do_login', 'UserController@doLogin');
    Route::post('/do_register_tutor', 'UserController@doRegisterTutor');
    Route::post('/do_register_student', 'UserController@doRegisterStudent');
});

Route::group(['prefix' => 'user', 'middleware' => 'checkauth'], function() {
    Route::get('/checkout/{id}', 'UserController@checkoutTutor');

    Route::post('/do_checkout', 'UserController@doCheckout');
});
