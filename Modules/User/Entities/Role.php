<?php

namespace Modules\User\Entities;

use Illuminate\Database\Eloquent\Model;

use Modules\User\Entities\User;

class Role extends Model
{
    protected $fillable = [];

    public function User() {
        return $this->belongsToMany(User::class, 'role_user');
    }
}
