<?php

namespace Modules\User\Entities;

use Illuminate\Database\Eloquent\Model;

use Modules\User\Entities\User;

class FavoriteTutor extends Model
{
    protected $table = 'favorite_tutor';

    protected $fillable = [];

    public function UserTutor() {
        return $this->belongsTo(User::class, 'tutor_user_id', 'id');
    }

    public function UserStudent() {
        return $this->belongsTo(User::class, 'student_user_id', 'id');
    }
}
