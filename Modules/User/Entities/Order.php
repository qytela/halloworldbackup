<?php

namespace Modules\User\Entities;

use Illuminate\Database\Eloquent\Model;

use Modules\User\Entities\User;
use Modules\User\Entities\DetailOrder;

class Order extends Model
{
    protected $fillable = [
        'id',
        'student_id',
        'tutor_id',
        'no_order',
        'transaction_date',
        'total_price',
        'status',
        'time_create'
    ];

    public function DetailOrder() {
        return $this->hasOne(DetailOrder::class, 'order_id', 'id');
    }

    public function OrderByStudent() {
        return $this->belongsTo(Order::class, 'student_id', 'id');
    }

    public function OrderByTUtor() {
        return $this->belongsTo(Order::class, 'tutor_id', 'id');
    }
}
