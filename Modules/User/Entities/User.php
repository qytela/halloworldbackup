<?php

namespace Modules\User\Entities;

use Illuminate\Database\Eloquent\Model;

use Modules\User\Entities\FavoriteTutor;
use Modules\User\Entities\Role;
use Modules\User\Entities\Order;
use Modules\Tutor\Entities\IntensifTutor;
use Modules\Tutor\Entities\ExpertiseTutor;
use Modules\Tutor\Entities\AvaibilityTeachingTutor;
use Modules\Tutor\Entities\LastEducationTutor;
use Modules\Tutor\Entities\TrainingAndTestExperienceTutor;
use Modules\Tutor\Entities\WorkExperienceTutor;
use Modules\Tutor\Entities\ProfileTutor;
use Modules\Student\Entities\ProfileStudent;

class User extends Model
{
    protected $fillable = [
        'id',
        'email',
        'password'
    ];

    public function ProfileTutor() {
        return $this->hasOne(ProfileTutor::class, 'user_id', 'id');
    }

    public function ProfileStudent() {
        return $this->hasOne(ProfileStudent::class, 'user_id', 'id');
    }

    public function IntensifTutor() {
        return $this->hasOne(IntensifTutor::class, 'user_id', 'id');
    }

    public function LastEducationTutor() {
        return $this->hasOne(LastEducationTutor::class, 'user_id', 'id');
    }
    
    public function AvaibilityTeachingTutor() {
        return $this->hasMany(AvaibilityTeachingTutor::class, 'user_id', 'id');
    }

    public function FavoriteTutor() {
        return $this->hasMany(FavoriteTutor::class, 'tutor_user_id', 'id');
    }

    public function TrainingAndTestExperienceTutor() {
        return $this->hasMany(TrainingAndTestExperienceTutor::class, 'user_id', 'id');
    }

    public function WorkExperienceTutor() {
        return $this->hasMany(WorkExperienceTutor::class, 'user_id', 'id');
    }

    public function ExpertiseTutor() {
        return $this->hasMany(ExpertiseTutor::class, 'user_id', 'id');
    }

    public function FavoriteStudent() {
        return $this->hasMany(FavoriteTutor::class, 'student_user_id', 'id');
    }

    public function OrderByStudent() {
        return $this->hasMany(Order::class, 'student_id', 'id');
    }

    public function OrderByTutor() {
        return $this->hasMany(Order::class, 'tutor_id', 'id');
    }

    public function Role() {
        return $this->belongsToMany(Role::class, 'role_user');
    }
}
