<?php

namespace Modules\User\Entities;

use Illuminate\Database\Eloquent\Model;

use Modules\User\Entities\Order;

class DetailOrder extends Model
{
    protected $fillable = [
        'order_id',
        'language',
        'date',
        'time',
        'materi',
        'sub_materi'
    ];

    public function DetailOrder() {
        return $this->belongsToMany(Order::class, 'order_id', 'id');
    }
}
