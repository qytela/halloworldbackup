<?php

namespace Modules\User\Http\Controllers;

use Illuminate\Http\Request;
use Illuminate\Http\Response;
use Illuminate\Routing\Controller;
use Illuminate\Support\Facades\DB;

use Modules\User\Entities\FavoriteTutor;
use Modules\User\Entities\User;
use Modules\User\Entities\RoleUser;
use Modules\User\Entities\Order;
use Modules\User\Entities\DetailOrder;
use Modules\Tutor\Entities\IntensifTutor;
use Modules\Tutor\Entities\AvaibilityTeachingTutor;
use Modules\Tutor\Entities\ProfileTutor;
use Modules\Tutor\Entities\LastEducationTutor;
use Modules\Tutor\Entities\TrainingAndTestExperienceTutor;
use Modules\Tutor\Entities\WorkExperienceTutor;
use Modules\Tutor\Entities\ExpertiseTutor;
use Modules\Student\Entities\ProfileStudent;

class UserController extends Controller
{   
    /**
     * Home
     */
    public function index() {
        return view('user::layouts.home');
    }

    /**
     * Login
     */
    public function doLogin(Request $request) {
        $auth = User::where(['email' => $request->email, 'password' => $request->password])->with('Role')->first();
        if (!$auth) {
            return redirect('user')->with('alert', 'Email atau Kata sandi anda salah');
        } else {
            $request->session()->put([
                'user_id' => $auth->id,
                'role' => $auth->Role[0]->slug
            ]);
            if ($auth->Role[0]->id === 2) {
                return redirect('/tutor/home');
            } else if ($auth->Role[0]->id === 3) {
                return redirect('/student/home');
            } else {
                return redirect('/');
            }
        }
    }

    /**
     * Register Tutor
     */
    public function registerTutor(Request $request) {
        return view('user::layouts.jaditutor');
    }

    /**
     * Cari All Tutor
     */
    public function cariTutor(Request $request) {
        $search = $request->search;
        $search_language = $request->search_language;
        $fromDate = $request->date;
        $toTime = $request->time;
        $fromPrice = $request->from_price;
        $toPrice = $request->to_price;
        if ($search) {
            $data['allTutor'] = ProfileTutor::where('fullname', 'LIKE', '%'.ucfirst($search).'%')->get();
        } else if ($search_language) {
            $data['allTutor'] = ProfileTutor::where('mastered_language', 'LIKE', '%'.ucfirst($search_language).'%')->get();
        } else if ($fromDate && $toTime) {
            $getData = AvaibilityTeachingTutor::where('date', 'LIKE', '%'.$fromDate.'%')->where('time', 'LIKE', '%'.$toTime.'%')->with('User.ProfileTutor')->get();
            $data['allTutor'] = $getData->pluck('User.ProfileTutor');
        } else if ($fromPrice && $toPrice) {
            $data['allTutor'] = ProfileTutor::whereBetween('hourly_fee', [$fromPrice, $toPrice])->get();
        } else {
            $data['allTutor'] = ProfileTutor::with('User.ExpertiseTutor')->get();
        }
        return view('user::layouts.caritutor', $data);
    }

    /**
     * Profile Tutor
     */
    public function profileTutor(Request $request) {
        $data['profileTutor'] = ProfileTutor::where('user_id', $request->id)->first();
        $data['expertiseTutor'] = ExpertiseTutor::where('user_id', $request->id)->get();
        $data['lastEducationTutor'] = LastEducationTutor::where('user_id', $request->id)->first();
        $data['workExperienceTutor'] = WorkExperienceTutor::where('user_id', $request->id)->get();
        $data['trainingTutor'] = TrainingAndTestExperienceTutor::where('user_id', $request->id)->get();
        if (!$data['profileTutor']) {
            return redirect('/user/caritutor');
        }
        return view('user::layouts.profiletutor', $data);
    }

    /**
     * Proses Register Tutor
     */
    public function doRegisterTutor(Request $request) {
        $request->validate([
            'firstname' => 'required',
            'lastname' => 'required',
            'email' => 'required|email',
            'phone_number' => 'required|numeric',
            'gender' => 'required',
            'address' => 'required',
            'birthday' => 'required',
            'country' => 'required',
            'photo_ktp' => 'required|image|mimes:jpeg,jpg,png|max:2048',
            'mastered_language' => 'required',
            'materi' => 'required',
            'sub_materi' => 'required',
            'link_introduction' => 'required',
            'photo_profile' => 'required|image|mimes:jpeg,jpg,png|max:2048',
            'about_me' => 'required',
            'degree' => 'required',
            'name_of_institusi' => 'required',
            'jurusan' => 'required',
            'year_entry_education' => 'required',
            'year_out_education' => 'required',
            'photo_ijazah' => 'required|image|mimes:jpeg,jpg,png|max:2048',
            'position' => 'required',
            'instansi' => 'required',
            'year_entry' => 'required',
            'name_of_training' => 'required',
            'year_training' => 'required',
            'photo_certificate' => 'required',
            'photo_certificate.*' => 'required|image|mimes:jpeg,jpg,png|max:2048',
            'hourly_fee' => 'required|numeric',
            'date' => 'required',
            'time' => 'required',
            'bank_name' => 'required',
            'no_rekening' => 'required',
            'card_holder_name' => 'required',
            'photo_book_rekening' => 'required|image|mimes:jpeg,jpg,png|max:2048',
            'password' => 'required',
            're_password' => 'required'
        ]);

        $checkEmail = User::where(['email' => $request->email])->first();
        $checkPhone = profileTutor::where(['phone_number' => $request->phone_number])->first();
        if ($checkEmail) {
            return redirect('/user/jadimurid')->with('error', 'Email telah di gunakan.');
        } else if ($checkPhone) {
            return redirect('/user/jadimurid')->with('error', 'Nomor Handphone telah di gunakan.');
        }

        $generateId = rand(0, 999999);
        $position = $request->position;
        $instansi = $request->instansi;
        $year_entry = $request->year_entry;
        $dataWorkExperience = [];
        foreach ($position as $key => $value) {
            $dataWorkExperience[] = [
                'user_id' => $generateId,
                'position' => $value,
                'instansi' => $instansi[$key],
                'year_entry' => $year_entry[$key],
                'created_at' => date('Y-m-d H:i:s'),
                'updated_at' => date('Y-m-d H:i:s')
            ];
        }
        $nameOfTraining = $request->name_of_training;
        $yearTraining = $request->year_training;
        $dataTrainingTutor = [];
        foreach ($request->file('photo_certificate') as $key => $value) {
            $photoCertificate = md5($value->getClientOriginalName()).'.'.$value->getClientOriginalExtension();
            $value->move('images_certificate', $photoCertificate);
            $dataTrainingTutor[] = [
                'user_id' => $generateId,
                'name_of_training' => $nameOfTraining[$key],
                'year' => $yearTraining[$key],
                'photo_certificate' => $photoCertificate,
                'created_at' => date('Y-m-d H:i:s'),
                'updated_at' => date('Y-m-d H:i:s')
            ];
        }
        $materi = $request->materi;
        $sub_materi = $request->sub_materi;
        $materiSubmateri = [];
        foreach ($materi as $key => $value) {
            $materiSubmateri[] = [
                'user_id' => $generateId,
                'materi' => $value,
                'sub_materi' => $sub_materi[$key],
                'created_at' => date('Y-m-d H:i:s'),
                'updated_at' => date('Y-m-d H:i:s')
            ];
        }
        $date = $request->date;
        $time = $request->time;
        $dateTime = [];
        foreach ($date as $key => $value) {
            $dateTime[] = [
                'user_id' => $generateId,
                'date' => $value,
                'time' => $time[$key],
                'created_at' => date('Y-m-d H:i:s'),
                'updated_at' => date('Y-m-d H:i:s')
            ];
        }
        $filePhotoProfile = $request->file('photo_profile');
        $filePhotoProfile = $request->file('photo_profile');
        $filePhotoKtp = $request->file('photo_ktp');
        $filePhotoIjazah = $request->file('photo_ijazah');
        $fileBookRekening = $request->file('photo_book_rekening');
        $photoIjazah = md5($filePhotoIjazah->getClientOriginalName()).'.'.$filePhotoIjazah->getClientOriginalExtension();
        $photoProfile = md5($filePhotoProfile->getClientOriginalName()).'.'.$filePhotoProfile->getClientOriginalExtension();
        $photoKtp = md5($filePhotoKtp->getClientOriginalName()).'.'.$filePhotoKtp->getClientOriginalExtension();
        $photoBookRekening = md5($fileBookRekening->getClientOriginalName()).'.'.$fileBookRekening->getClientOriginalExtension();

        $filterMasteredLanguage = array_unique($request->mastered_language);
        // $filterExpertise = array_unique($materiSubmateri);

        $user = User::create([
            'id' => $generateId,
            'email' => $request->email,
            'password' => $request->password
        ]);

        $profileTutor = ProfileTutor::create([
            'user_id' => $generateId,
            'fullname' => $request->firstname.' '.$request->lastname,
            'gender' => $request->gender,
            'address' => $request->address,
            'birthday' => $request->birthday,
            'mastered_language' => join(', ', $filterMasteredLanguage),
            // 'expertise' => join(', ', $filterExpertise),
            'about_me' => $request->about_me,
            'link_introduction' => $request->link_introduction,
            'hourly_fee' => $request->hourly_fee,
            'country' => $request->country,
            'phone_number' => $request->phone_number,
            'rating' => 0,
            'students' => 0,
            'class' => 0,
            'status' => 'Online',
            'photo_profile' => $photoProfile,
            'photo_ktp' => $photoKtp
        ]);

        $lastEducationTutor = LastEducationTutor::create([
            'user_id' => $generateId,
            'degree' => $request->degree,
            'name_of_institusi' => $request->name_of_institusi,
            'jurusan' => $request->jurusan,
            'year_entry' => $request->year_entry_education,
            'year_out' => $request->year_out_education,
            'photo_ijazah' => $photoIjazah
        ]);

        $trainingTutor = DB::table('training_and_test_experience_tutor')->insert($dataTrainingTutor);

        $workExperienceTutor = DB::table('work_experience_tutor')->insert($dataWorkExperience);

        $expertiseTutor = DB::table('expertise_tutor')->insert($materiSubmateri);

        $avaibilityTeachingTutor = DB::table('avaibility_teaching_tutor')->insert($dateTime);

        $intensifTutor = IntensifTutor::create([
            'user_id' => $generateId,
            'bank_name' => $request->bank_name,
            'no_rekening' => $request->no_rekening,
            'card_holder_name' => $request->card_holder_name,
            'photo_book_rekening' => $photoBookRekening
        ]);

        $roleUser = RoleUser::create([
            'user_id' => $generateId,
            'role_id' => 2
        ]);
        
        if ($user && $profileTutor && $lastEducationTutor && $trainingTutor && $workExperienceTutor && $expertiseTutor && $avaibilityTeachingTutor && $intensifTutor && $roleUser) {
            $filePhotoProfile->move('images', $photoProfile);
            $filePhotoIjazah->move('images_ijazah', $photoIjazah);
            $filePhotoKtp->move('images_ktp', $photoKtp);
            $fileBookRekening->move('images_intensif', $photoBookRekening);
            return 'OK';
        } else {
            return 'ERROR';
        }
    }

    public function registerStudent() {
        return view('user::layouts.jadimurid');
    }

    public function doRegisterStudent(Request $request) {
        $request->validate([
            'firstname' => 'required',
            'lastname' => 'required',
            'email' => 'required',
            'phone_number' => 'required|numeric',
            'gender' => 'required',
            'address' => 'required',
            'birthday' => 'required',
            'password' => 'required'
        ]);

        $checkEmail = User::where(['email' => $request->email])->first();
        $checkPhone = ProfileStudent::where(['phone_number' => $request->phone_number])->first();
        if ($checkEmail) {
            return redirect('/user/jadimurid')->with('error', 'Email telah di gunakan.');
        } else if ($checkPhone) {
            return redirect('/user/jadimurid')->with('error', 'Nomor Handphone telah di gunakan.');
        }
        
        $generateId = rand(0, 999999);
        
        $user = User::create([
            'id' => $generateId,
            'email' => $request->email,
            'password' => $request->password
        ]);

        
        $profileStudent = ProfileStudent::create([
            'user_id' => $generateId,
            'fullname' => $request->firstname.' '.$request->lastname,
            'gender' => $request->gender,
            'phone_number' => $request->phone_number,
            'address' => $request->address,
            'birthday' => $request->birthday,
            'country' => '',
            'about_me' => '',
            'password' => $request->password,
            'photo_profile' => '' 
        ]);

        $roleUser = RoleUser::create([
            'user_id' => $generateId,
            'role_id' => 3
        ]);

        if ($user && $profileStudent && $roleUser) {
            $request->session()->put([
                'user_id' => $generateId
            ]);
            return redirect('/student/home');
        } else {
            return redirect('/user/jadimurid')->with('error', 'Gagal mendaftar, silahkan coba lagi.');
        }
    }

    public function checkoutTutor(Request $request) {
        $data['noOrder'] = 'INV/'.date('Ymd' ).'/VII/'.rand(0, 9999);
        $data['profileTutor'] = ProfileTutor::where('user_id', $request->id)->first();
        $data['avaibilityTeachingTutor'] = AvaibilityTeachingTutor::where('user_id', $request->id)->get();
        $data['expertiseTutor'] = ExpertiseTutor::where('user_id', $request->id)->get();
        if (!$data['profileTutor']) {
            return redirect('/user/caritutor');
        }
        return view('user::layouts.checkout', $data);
    }

    public function doCheckout(Request $request) {
        $orderId = rand(0, 999999);
        $explodeDateTime = explode("/", $request->date_time);
        $profileTutor = ProfileTutor::where('user_id', $request->id)->first();
        if (!$profileTutor) {
            return redirect('/user/caritutor');
        } else {
            $createOrder = Order::create([
                'id' => $orderId,
                'student_id' => $request->session()->get('user_id'),
                'tutor_id' => $request->id,
                'no_order' => $request->no_order,
                'transaction_date' => date('Y-m-d'),
                'total_price' => $profileTutor->hourly_fee,
                'status' => 1,
                'time_create' => date('H:i:s')
            ]);
            $createDetailOrder = DetailOrder::create([
                'order_id' => $orderId,
                'language' => $request->language,
                'date' => $explodeDateTime[0],
                'time' => $explodeDateTime[1],
                'materi' => $request->materi,
                'sub_materi' => $request->sub_materi
            ]);
            if (!$createOrder && !$createDetailOrder) {
                return redirect('/user/caritutor');
            } else {
                return redirect('/student/home');
            }
        }
    }
}
