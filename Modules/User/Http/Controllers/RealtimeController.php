<?php

namespace Modules\User\Http\Controllers;

use Illuminate\Http\Request;
use Illuminate\Http\Response;
use Illuminate\Routing\Controller;

class RealtimeController extends Controller
{
    /**
     * Video Call and Chat Beetwen Student and Tutor
     */
    public function VidCallChat(Request $request) {
        $data['roomId'] = $request->id;
        return view('user::layouts.livestream', $data);
    }
}
