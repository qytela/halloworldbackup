<?php

use Illuminate\Support\Facades\Schema;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

class CreateFavoriteTutorsTable extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::create('favorite_tutor', function (Blueprint $table) {
            $table->bigIncrements('id');
            $table->integer('tutor_user_id')->unsigned()->index();
            $table->integer('student_user_id')->unsigned()->index();

            $table->foreign('tutor_user_id')->references('id')->on('users')->onDelete('cascade');
            $table->foreign('student_user_id')->references('id')->on('users')->onDelete('cascade');
            $table->timestamps();
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::dropIfExists('favorite_tutor');
    }
}
