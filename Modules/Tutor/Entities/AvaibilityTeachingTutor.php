<?php

namespace Modules\Tutor\Entities;

use Illuminate\Database\Eloquent\Model;

use Modules\User\Entities\User;

class AvaibilityTeachingTutor extends Model
{
    protected $table = 'avaibility_teaching_tutor';

    protected $fillable = [
        'user_id',
        'date',
        'time'
    ];

    public function User() {
        return $this->belongsTo(User::class, 'user_id', 'id');
    }
}