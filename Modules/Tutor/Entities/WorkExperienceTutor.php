<?php

namespace Modules\Tutor\Entities;

use Illuminate\Database\Eloquent\Model;

use Modules\User\Entities\User;

class WorkExperienceTutor extends Model
{
    protected $table = 'work_experience_tutor';

    protected $fillable = [
        'user_id',
        'position',
        'instansi',
        'year_entry'
    ];
    
    public function User() {
        return $this->belongsTo(User::class, 'user_id', 'id');
    }
}
