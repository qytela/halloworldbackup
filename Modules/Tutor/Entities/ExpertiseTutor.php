<?php

namespace Modules\Tutor\Entities;

use Illuminate\Database\Eloquent\Model;

use Modules\User\Entities\User;

class ExpertiseTutor extends Model
{
    protected $table = 'expertise_tutor';

    protected $fillable = [
        'user_id',
        'materi',
        'sub_materi'
    ];

    public function User() {
        return $this->belongsTo(User::class, 'user_id', 'id');
    }
}
