<?php

namespace Modules\Tutor\Entities;

use Illuminate\Database\Eloquent\Model;

use Modules\User\Entities\User;

class IntensifTutor extends Model
{
    protected $table = 'intensif_tutor';

    protected $fillable = [
        'user_id',
        'bank_name',
        'no_rekening',
        'card_holder_name',
        'photo_book_rekening'
    ];

    public function User() {
        return $this->belongsTo(User::class, 'user_id', 'id');
    }
}
