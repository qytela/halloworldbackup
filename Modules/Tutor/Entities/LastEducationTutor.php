<?php

namespace Modules\Tutor\Entities;

use Illuminate\Database\Eloquent\Model;

use Modules\User\Entities\User;

class LastEducationTutor extends Model
{
    protected $table = 'last_education_tutor';
    
    protected $fillable = [
        'user_id',
        'degree',
        'name_of_institusi',
        'jurusan',
        'year_entry',
        'year_out',
        'photo_ijazah'
    ];
    
    public function User() {
        return $this->belongsTo(User::class, 'user_id', 'id');
    }
}
