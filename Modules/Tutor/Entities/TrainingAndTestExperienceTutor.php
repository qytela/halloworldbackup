<?php

namespace Modules\Tutor\Entities;

use Illuminate\Database\Eloquent\Model;

use Modules\User\Entities\User;

class TrainingAndTestExperienceTutor extends Model
{
    protected $table = 'training_and_test_experience_tutor';

    protected $fillable = [
        'user_id',
        'name_of_training',
        'year',
        'photo_certificate'
    ];
    
    public function User() {
        return $this->belongsTo(User::class, 'user_id', 'id');
    }
}
