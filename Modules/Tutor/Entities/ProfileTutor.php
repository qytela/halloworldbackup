<?php

namespace Modules\Tutor\Entities;

use Illuminate\Database\Eloquent\Model;

use Modules\User\Entities\User;

class ProfileTutor extends Model
{
    protected $table = 'profile_tutor';

    protected $fillable = [
        'user_id',
        'fullname',
        'gender',
        'address',
        'birthday',
        'mastered_language',
        'expertise',
        'about_me',
        'link_introduction',
        'hourly_fee',
        'country',
        'phone_number',
        'rating',
        'students',
        'class',
        'status',
        'photo_profile',
        'photo_ktp'
    ];

    public function User() {
        return $this->belongsTo(User::class, 'user_id', 'id');
    }
}
