<?php

namespace Modules\Tutor\Http\Controllers;

use Illuminate\Http\Request;
use Illuminate\Http\Response;
use Illuminate\Routing\Controller;

use Modules\User\Entities\Order;
use Modules\User\Entities\DetailOrder;
use Modules\Student\Entities\ProfileStudent;
use Modules\Tutor\Entities\ProfileTutor;

class TutorController extends Controller
{
    public function home(Request $request) {
        $dataTable = [];
        $dataOrder = Order::where('tutor_id', $request->session()->get('user_id'))->get();
        foreach ($dataOrder as $key => $value) {
            $dataStudent = ProfileStudent::where('user_id', $value->student_id)->first();
            $dataTable[] = [
                'no_order' => $value->no_order,
                'transaction_date' => $value->transaction_date,
                'total_price' => $value->total_price,
                'name_student' => $dataStudent->fullname,
                'status' => $value->status,
                'time_create' => date('H:i:s')
            ];
        }
        $data['dataTutor'] = ProfileTutor::where('user_id', $request->session()->get('user_id'))->with('User')->first();
        $data['dataTable'] = $dataTable;
        return view('tutor::layouts.dashboard_tutor.home', $data);
    }

    public function table(Request $request) {
        $dataTable = [];
        $dataOrder = Order::where('tutor_id', $request->session()->get('user_id'))->with('DetailOrder')->get();
        foreach ($dataOrder as $key => $value) {
            $dataStudent = ProfileStudent::where('user_id', $value->student_id)->first();
            $dataTable[] = [
                'no_order' => $value->no_order,
                'transaction_date' => $value->transaction_date,
                'total_price' => $value->total_price,
                'name_student' => $dataStudent->fullname,
                'status' => $value->status,
                'time_create' => date('H:i:s'),
                'materi' => $dataOrder->pluck('DetailOrder')[$key]->materi,
                'sub_materi' => $dataOrder->pluck('DetailOrder')[$key]->sub_materi
            ];
        }
        $data['dataTutor'] = ProfileTutor::where('user_id', 35832)->with('User')->first();
        $data['dataTable'] = $dataTable;
        return view('tutor::layouts.dashboard_tutor.pesananmasuk', $data);
    }

    public function logout(Request $request) {
        $request->session()->flush();
        return redirect('/');
    }
}
