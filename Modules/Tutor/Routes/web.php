<?php

/*
|--------------------------------------------------------------------------
| Web Routes
|--------------------------------------------------------------------------
|
| Here is where you can register web routes for your application. These
| routes are loaded by the RouteServiceProvider within a group which
| contains the "web" middleware group. Now create something great!
|
*/

Route::group(['prefix' => 'tutor', 'middleware' => ['checkauth', 'checkrole:TUTOR']], function() {
    Route::get('/home', 'TutorController@home');
    Route::get('/table', 'TutorController@table');
    Route::get('/logout', 'TutorController@logout');
});
