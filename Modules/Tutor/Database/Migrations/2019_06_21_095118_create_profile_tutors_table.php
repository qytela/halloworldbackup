<?php

use Illuminate\Support\Facades\Schema;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

class CreateProfileTutorsTable extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::create('profile_tutor', function (Blueprint $table) {
            $table->bigIncrements('id');
            $table->integer('user_id')->unsigned()->index();
            $table->string('fullname');
            $table->string('gender');
            $table->string('address');
            $table->date('birthday');
            $table->string('mastered_language');
            // $table->string('expertise');
            $table->string('link_introduction');
            $table->string('about_me');
            $table->integer('hourly_fee');
            $table->string('country');
            $table->bigInteger('phone_number');
            $table->integer('rating');
            $table->integer('students');
            $table->integer('class');
            $table->string('status');
            $table->string('photo_profile');
            $table->string('photo_ktp');

            $table->foreign('user_id')->references('id')->on('users')->onDelete('cascade');
            $table->timestamps();
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::dropIfExists('profile_tutor');
    }
}
