<?php

namespace Modules\Tutor\Database\Seeders;

use Illuminate\Database\Seeder;
use Illuminate\Database\Eloquent\Model;
use Illuminate\Support\Facades\DB;
use Faker\Factory as Faker;
use Carbon\Carbon;

use Modules\User\Entities\User;
use Modules\User\Entities\RoleUser;
use Modules\Tutor\Entities\IntensifTutor;
use Modules\Tutor\Entities\AvaibilityTeachingTutor;
use Modules\Tutor\Entities\ProfileTutor;
use Modules\Tutor\Entities\ExpertiseTutor;
use Modules\Tutor\Entities\LastEducationTutor;
use Modules\Tutor\Entities\TrainingAndTestExperienceTutor;
use Modules\Tutor\Entities\WorkExperienceTutor;

class JadiTutorTableSeeder extends Seeder
{
    /**
     * Run the database seeds.
     *
     * @return void
     */
    public function run()
    {
        Model::unguard();

        // $this->call("OthersTableSeeder");
        $faker = Faker::create('id_ID');
        
        $masteredLanguage = ['English (British)', 'English (American)', 'Chinese (Mandarin)', 'Japanese', 'South Korea', 'Arabic', 'French', 'German', 'Indonesia'];
        $materi = ['Academic', 'Career Builder', 'Traveler', 'Language Lover', 'Acamedic', 'Career Builder', 'Traveler', 'Language Lover', 'Academic'];
        $subMateri = ['Materi Beginner', 'Materi Intermediate', 'Materi Profiecient', 'Materi Beginner', 'Materi Intermediate', 'Materi Profiecient', 'Materi Beginner', 'Materi Intermediate', 'Materi Profiecient'];
        
        for ($i = 0; $i <= 8; $i++) {
            $generateId = rand(0, 999999);
            
            $user = new User;
            $user->id = $generateId;
            $user->email = $faker->email;
            $user->password = 'Halloworld';

            $profileTutor = new ProfileTutor;
            $profileTutor->user_id = $generateId;
            $profileTutor->fullname = $faker->name;
            $profileTutor->gender = 'Laki-laki';
            $profileTutor->address = $faker->address;
            $profileTutor->birthday = date('Y-m-d');
            $profileTutor->mastered_language = $masteredLanguage[$i];
            // $profileTutor->expertise = $expertise[$i];
            $profileTutor->link_introduction = 'https://www.youtube.com/watch?v=isM-r6B1wz4';
            $profileTutor->about_me = 'Lorem ipsum is ipsum bla bla ipsum bla ipsum bla bla bla.';
            $profileTutor->hourly_fee = 70000;
            $profileTutor->country = 'Indonesia';
            $profileTutor->phone_number = rand(0, 999999999);
            $profileTutor->rating = $faker->numberBetween(0, 5);
            $profileTutor->students = $faker->numberBetween(0, 10);
            $profileTutor->class = $faker->numberBetween(0, 15);
            $profileTutor->status = 'Online';
            $profileTutor->photo_profile = '675c937a666080b5fb48f812e4ca4505.png';
            $profileTutor->photo_ktp = 'deb3c0979dd33fd606201022616bf06b.png';

            $expertiseTutor = new ExpertiseTutor;
            $expertiseTutor->user_id = $generateId;
            $expertiseTutor->materi = $materi[$i];
            $expertiseTutor->sub_materi = $subMateri[$i];

            $avaibilityTeachingTutor = new AvaibilityTeachingTutor;
            $avaibilityTeachingTutor->user_id = $generateId;
            $avaibilityTeachingTutor->date = date('Y-m-d');
            $avaibilityTeachingTutor->time = date('H:i:s');

            $intensifTutor = new IntensifTutor;
            $intensifTutor->user_id = $generateId;
            $intensifTutor->bank_name = 'Bank BNI';
            $intensifTutor->no_rekening = rand(0, 9999999999);
            $intensifTutor->card_holder_name = $faker->name;
            $intensifTutor->photo_book_rekening = '94e4afd368e612cdb728ee9ca89ddf67.png';

            $roleUser = new RoleUser;
            $roleUser->user_id = $generateId;
            $roleUser->role_id = 2;

            $lastEducation = new LastEducationTutor;
            $lastEducation->user_id = $generateId;
            $lastEducation->degree = 'B.A. Spanish and History';
            $lastEducation->name_of_institusi = 'University of Wiscornsin - Platter, WI';
            $lastEducation->jurusan = '';
            $lastEducation->year_entry = date('Y-m-d');
            $lastEducation->year_out = date('Y-m-d');
            $lastEducation->photo_ijazah = 'test.png';

            $workExperienceTutor = new WorkExperienceTutor;
            $workExperienceTutor->user_id = $generateId;
            $workExperienceTutor->position = 'Online English Teacher';
            $workExperienceTutor->instansi = 'Italki';
            $workExperienceTutor->year_entry = date('Y-m-d');

            $trainingTutor = new TrainingAndTestExperienceTutor;
            $trainingTutor->user_id = $generateId;
            $trainingTutor->name_of_training = 'TESOL in China';
            $trainingTutor->year = date('Y-m-d');
            $trainingTutor->photo_certificate = 'test.png';

            $user->save();
            $profileTutor->save();
            $expertiseTutor->save();
            $avaibilityTeachingTutor->save();
            $intensifTutor->save();
            $roleUser->save();
            $lastEducation->save();
            $workExperienceTutor->save();
            $trainingTutor->save();
        }
    }
}
