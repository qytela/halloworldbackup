<!DOCTYPE html>
<html lang="en">

<head>
    <meta charset="utf-8">
    <meta http-equiv="X-UA-Compatible" content="IE=edge">
    <meta name="viewport" content="width=device-width, initial-scale=1">
    <title>Dashboard Murid</title>
    <!-- Favicon icon -->
    <!-- <link rel="icon" type="image/png" sizes="16x16" href="../assets/images/favicon.png"> -->
    <!-- Custom Stylesheet -->
    <link href="css/style.css" rel="stylesheet">
    <script src="js/modernizr-3.6.0.min.js"></script>

    <!-- Font Awesome -->
    <link rel="stylesheet" href="https://use.fontawesome.com/releases/v5.7.0/css/all.css">
    <link rel="stylesheet" href="https://use.fontawesome.com/releases/v5.7.2/css/all.css" integrity="sha384-fnmOCqbTlWIlj8LyTjo7mOUStjsKC4pOpQbqyi7RrhN7udi9RwhKkMHpvLbHG9Sr" crossorigin="anonymous">
</head>

<body>

    <div id="preloader">
        <div class="loader">
            <div class="loader__bar"></div>
            <div class="loader__bar"></div>
            <div class="loader__bar"></div>
            <div class="loader__bar"></div>
            <div class="loader__bar"></div>
            <div class="loader__ball"></div>
        </div>
    </div>

    <div id="main-wrapper">

        <!-- header -->
        <div class="header">
            <div class="nav-header">
                <div class="brand-logo">
                    <a href="index.html">
                        <!-- <i class="cc BTC"></i> -->
                        <img src="{{ asset('img/korea_tutor.png') }}" class="cc BTC"  alt="">
                        <span class="brand-title">
                            <img src="{{ asset('img/Logo-Halloworld.png') }}" alt="">
                        </span>
                    </a>
                </div>

                <div class="nav-control">
                    <div class="hamburger">
                        <span class="line"></span>
                        <span class="line"></span>
                        <span class="line"></span>
                    </div>
                </div>
            </div>

            <div class="header-content">
                <div class="header-left">
                    <ul>
                        <li class="icons position-relative">
                            <a href="javascript:void(0)">
                                <i class="icon-magnifier f-s-16"></i>
                            </a>
                            <div class="drop-down animated bounceInDown">
                                <div class="dropdown-content-body">
                                    <div class="header-search" id="header-search">
                                        <form action="#">
                                            <div class="input-group">
                                                <input type="text" class="form-control" placeholder="Search">
                                                <div class="input-group-append">
                                                    <span class="input-group-text">
                                                        <i class="icon-magnifier"></i>
                                                    </span>
                                                </div>
                                            </div>
                                        </form>
                                    </div>
                                </div>
                            </div>
                        </li>
                    </ul>

                </div>
                <div class="header-right">
                    <ul>
                        <li class="icons">
                            <a href="javascript:void(0)">
                                <i class="icon-bell f-s-18" aria-hidden="true"></i>
                                <div class="pulse-css"></div>
                            </a>
                            <div class="drop-down animated bounceInDown">
                                <div class="dropdown-content-heading">
                                    <span class="text-left">Recent Notifications</span>
                                </div>
                                <div class="dropdown-content-body">
                                    <ul>
                                        <li>
                                            <a href="#">
                                                <img class="pull-left m-r-10 avatar-img" src="../assets/images/avatar/1.jpg" alt="" />
                                                <div class="notification-content">
                                                    <small class="notification-timestamp pull-right">02:34 PM</small>
                                                    <div class="notification-heading">Mr. Saifun</div>
                                                    <div class="notification-text">5 members joined today </div>
                                                </div>
                                            </a>
                                        </li>
                                        <li>
                                            <a href="#">
                                                <img class="pull-left m-r-10 avatar-img" src="../assets/images/avatar/2.jpg" alt="" />
                                                <div class="notification-content">
                                                    <small class="notification-timestamp pull-right">02:34 PM</small>
                                                    <div class="notification-heading">Mariam</div>
                                                    <div class="notification-text">likes a photo of you</div>
                                                </div>
                                            </a>
                                        </li>
                                        <li>
                                            <a href="#">
                                                <img class="pull-left m-r-10 avatar-img" src="../assets/images/avatar/3.jpg" alt="" />
                                                <div class="notification-content">
                                                    <small class="notification-timestamp pull-right">02:34 PM</small>
                                                    <div class="notification-heading">Tasnim</div>
                                                    <div class="notification-text">Hi Teddy, Just wanted to let you ...</div>
                                                </div>
                                            </a>
                                        </li>
                                        <li>
                                            <a href="#">
                                                <img class="pull-left m-r-10 avatar-img" src="../assets/images/avatar/4.jpg" alt="" />
                                                <div class="notification-content">
                                                    <small class="notification-timestamp pull-right">02:34 PM</small>
                                                    <div class="notification-heading">Ishrat Jahan</div>
                                                    <div class="notification-text">Hi Teddy, Just wanted to let you ...</div>
                                                </div>
                                            </a>
                                        </li>
                                        <li class="text-center">
                                            <a href="#" class="more-link">See All</a>
                                        </li>
                                    </ul>
                                </div>
                            </div>
                        </li>
                        <li class="icons">
                            <a href="javascript:void(0)">
                                <i class="icon-envelope f-s-18" aria-hidden="true"></i>
                                <div class="pulse-css"></div>
                            </a>
                            <div class="drop-down animated bounceInDown">
                                <div class="dropdown-content-heading">
                                    <span class="text-left">2 New Messages</span>
                                </div>
                                <div class="dropdown-content-body">
                                    <ul>
                                        <li class="notification-unread">
                                            <a href="#">
                                                <img class="pull-left m-r-10 avatar-img" src="../assets/images/avatar/1.jpg" alt="" />
                                                <div class="notification-content">
                                                    <small class="notification-timestamp pull-right">02:34 PM</small>
                                                    <div class="notification-heading">Saiul Islam</div>
                                                    <div class="notification-text">Hi Teddy, Just wanted to let you ...</div>
                                                </div>
                                            </a>
                                        </li>
                                        <li class="notification-unread">
                                            <a href="#">
                                                <img class="pull-left m-r-10 avatar-img" src="../assets/images/avatar/2.jpg" alt="" />
                                                <div class="notification-content">
                                                    <small class="notification-timestamp pull-right">02:34 PM</small>
                                                    <div class="notification-heading">Ishrat Jahan</div>
                                                    <div class="notification-text">Hi Teddy, Just wanted to let you ...</div>
                                                </div>
                                            </a>
                                        </li>
                                        <li>
                                            <a href="#">
                                                <img class="pull-left m-r-10 avatar-img" src="../assets/images/avatar/3.jpg" alt="" />
                                                <div class="notification-content">
                                                    <small class="notification-timestamp pull-right">02:34 PM</small>
                                                    <div class="notification-heading">Saiul Islam</div>
                                                    <div class="notification-text">Hi Teddy, Just wanted to let you ...</div>
                                                </div>
                                            </a>
                                        </li>
                                        <li>
                                            <a href="#">
                                                <img class="pull-left m-r-10 avatar-img" src="../assets/images/avatar/4.jpg" alt="" />
                                                <div class="notification-content">
                                                    <small class="notification-timestamp pull-right">02:34 PM</small>
                                                    <div class="notification-heading">Ishrat Jahan</div>
                                                    <div class="notification-text">Hi Teddy, Just wanted to let you ...</div>
                                                </div>
                                            </a>
                                        </li>
                                        <li class="text-center">
                                            <a href="#" class="more-link">See All</a>
                                        </li>
                                    </ul>
                                </div>
                            </div>
                        </li>
                        <li class="icons">
                            <a href="javascript:void(0)">
                                <i class="icon-note f-s-18" aria-hidden="true"></i>
                                <div class="pulse-css"></div>
                            </a>
                            <div class="drop-down dropdown-task animated bounceInDown">
                                <div class="dropdown-content-heading">
                                    <span class="text-left">Task Update</span>
                                </div>
                                <div class="dropdown-content-body">
                                    <ul>
                                        <li>
                                            <a href="#">
                                                <div class="notification-content">
                                                    <small class="notification-timestamp pull-right">85% Complete</small>
                                                    <div class="notification-heading">Task One</div>
                                                    <div class="progress">
                                                        <div style="width: 85%;" aria-valuemax="100" aria-valuemin="0" aria-valuenow="85" role="progressbar" class="progress-bar progress-bar-success"></div>
                                                    </div>
                                                </div>
                                            </a>
                                        </li>
                                        <li>
                                            <a href="#">
                                                <div class="notification-content">
                                                    <small class="notification-timestamp pull-right">60% Complete</small>
                                                    <div class="notification-heading">Task Two</div>
                                                    <div class="progress">
                                                        <div style="width: 60%;" aria-valuemax="100" aria-valuemin="0" aria-valuenow="60" role="progressbar" class="progress-bar progress-bar-primary"></div>
                                                    </div>
                                                </div>
                                            </a>
                                        </li>
                                        <li>
                                            <a href="#">
                                                <div class="notification-content">
                                                    <small class="notification-timestamp pull-right">25% Complete</small>
                                                    <div class="notification-heading">Task Three</div>
                                                    <div class="progress">
                                                        <div style="width: 25%;" aria-valuemax="100" aria-valuemin="0" aria-valuenow="25" role="progressbar" class="progress-bar progress-bar-warning"></div>
                                                    </div>
                                                </div>
                                            </a>
                                        </li>
                                        <li>
                                            <a href="#">
                                                <div class="notification-content">
                                                    <small class="notification-timestamp pull-right">75% Complete</small>
                                                    <div class="notification-heading">Task Four</div>
                                                    <div class="progress">
                                                        <div style="width: 75%;" aria-valuemax="100" aria-valuemin="0" aria-valuenow="75" role="progressbar" class="progress-bar progress-bar-danger"></div>
                                                    </div>
                                                </div>
                                            </a>
                                        </li>
                                        <li class="text-center">
                                            <a href="#" class="more-link">See All</a>
                                        </li>
                                    </ul>
                                </div>
                            </div>
                        </li>
                        <li class="icons">
                            <a href="javascript:void(0)">
                                <i class="icon-user f-s-18" aria-hidden="true"></i>
                            </a>
                            <div class="drop-down dropdown-profile animated bounceInDown">
                                <div class="dropdown-content-body">
                                    <ul>
                                        <li>
                                            <a href="#">
                                                <i class="icon-envelope"></i>
                                                <span>Inbox</span>
                                            </a>
                                        </li>
                                        <li>
                                            <a href="#">
                                                <i class="icon-settings"></i>
                                                <span>Setting</span>
                                            </a>
                                        </li>
                                        <li>
                                            <a href="#">
                                                <i class="icon-lock"></i>
                                                <span>Lock Screen</span>
                                            </a>
                                        </li>
                                        <li>
                                            <a href="home">
                                                <i class="icon-power"></i>
                                                <span>Logout</span>
                                            </a>
                                        </li>
                                    </ul>
                                </div>
                            </div>
                        </li>
                    </ul>
                </div>
            </div>
        </div>
        <!-- #/ header -->


        <!-- sidebar -->
        <div class="nk-sidebar">
            <div class="nk-nav-scroll">

                <div class="nav-user">
                    <img src="{{ asset('img/reza.jpeg') }}" alt="" class="rounded-circle">
                    <h5>Hi, Jongsuk</h5>
                    <!-- <p>Web Developer</p> -->

                    <div class="nav-user-option" style="margin-top: 10px;">
                        <div class="setting-option">
                            <div data-toggle="dropdown">
                                <i class="icon-settings"></i>
                            </div>
                            <div class="dropdown-menu animated flipInX">
                                <a class="dropdown-item" href="#">Account</a>
                                <a class="dropdown-item" href="#">Lock</a>
                                <a class="dropdown-item" href="home">Logout</a>
                            </div>
                        </div>
                        <div class="notification-option">
                            <div data-toggle="dropdown">
                                <i class="icon-bell"></i>
                            </div>
                            <div class="dropdown-menu animated flipInX">
                                <a class="dropdown-item" href="#">Email
                                    <span class="badge badge-primary pull-right m-t-3">05</span>
                                </a>
                                <a class="dropdown-item" href="#">Feed back
                                    <span class="badge badge-danger pull-right m-t-3">02</span>
                                </a>
                                <a class="dropdown-item" href="#">Report
                                    <span class="badge badge-warning pull-right m-t-3">02</span>
                                </a>
                            </div>
                        </div>
                    </div>
                </div>

                <ul class="metismenu" id="menu">
                    <li class="nav-label">Menu</li>
                    <li>
                        <a href="dashboardtutor" class="text-center">
                            <span class="nav-text text-center">Dashboard</span>
                        </a>
                    </li>
                    <li>
                        <a href="pesananmasuk" class="text-center">
                            <span class="nav-text text-center">Pesanan Masuk</span>
                        </a>
                    </li>
                    <li>
                        <a href="profiletutor" class="text-center">
                            <span class="nav-text text-center">Profil</span>
                        </a>
                    </li>
                    <li class="keluar_dashboard">
                        <a href="home" class="d-flex justify-content-center mt-2">
                            <i class="icon-logout "></i>
                            <!-- <i class="fas fa-sign-out-alt"></i> -->
                            <span class="nav-text">Keluar</span>
                        </a>
                    </li>

                    <!-- <li class="nav-label">Main</li>
                    <li>
                        <a class="has-arrow" href="#" aria-expanded="false">
                            <i class="icon-speedometer"></i>
                            <span class="nav-text">Dashboard</span>
                            <span class="badge badge-danger nav-badge">10</span>
                        </a>
                        <ul aria-expanded="false">
                            <li>
                                <a href="index.html">Dashnoard 1</a>
                            </li>
                            <li>
                                <a href="index2.html">Dashnoard 2</a>
                            </li>
                            <li>
                                <a href="index3.html">Dashnoard 3</a>
                            </li>
                            <li>
                                <a href="index4.html">Dashnoard 4</a>
                            </li>
                            <li>
                                <a href="index5.html">Dashnoard 5</a>
                            </li>
                            <li>
                                <a href="index6.html">Dashnoard 6</a>
                            </li>
                            <li>
                                <a href="index7.html">Dashnoard 7</a>
                            </li>
                            <li>
                                <a href="index8.html">Dashnoard 8</a>
                            </li>
                            <li>
                                <a href="index9.html">Dashnoard 9</a>
                            </li>
                            <li>
                                <a href="index10.html">Dashnoard 10</a>
                            </li>
                        </ul>
                    </li>

                    <li class="nav-label">Crypto</li>

                    <li>
                        <a class="has-arrow" href="#" aria-expanded="false">
                            <i class="icon-cursor"></i>
                            <span class="nav-text">Cryptocurrency</span>
                            <span class="badge badge-primary nav-badge">18</span>

                        </a>
                        <ul aria-expanded="false">
                            <li>
                                <a href="ico-countdown.html">Countdown</a>
                            </li>
                            <li>
                                <a href="ico-market-cap.html">Market Capitalization</a>
                            </li>
                            <li>
                                <a href="ico-transaction.html">Transaction</a>
                            </li>
                            <li>
                                <a href="ico-gainers-loosers.html">Gainers / Loosers</a>
                            </li>
                            <li>
                                <a href="ico-timeline.html">Timeline</a>
                            </li>
                            <li>
                                <a href="ico-progressbar.html">Progressbar</a>
                            </li>
                            <li>
                                <a href="ico-exchange.html">Exchange</a>
                            </li>
                            <li>
                                <a href="ico-advisor.html">Advisor</a>
                            </li>
                            <li>
                                <a href="ico-advisor-profile.html">Advisor Profile</a>
                            </li>
                            <li>
                                <a href="ico-counter.html">Counters</a>
                            </li>
                            <li>
                                <a href="ico-trading-view.html">Trading View</a>
                            </li>
                            <li>
                                <a href="ico-payment-gateway.html">Payment Gateway</a>
                            </li>
                            <li>
                                <a href="ico-wallet.html">Wallet</a>
                            </li>
                        </ul>
                    </li>

                    <li class="nav-label">Layout</li>

                    <li>
                        <a class="has-arrow" href="#" aria-expanded="false">
                            <i class="icon-layers"></i>
                            <span class="nav-text">Layouts</span>
                        </a>
                        <ul aria-expanded="false">
                            <li>
                                <a href="layout-blank.html">Blank</a>
                            </li>
                            <li>
                                <a href="layout-boxed.html">Boxed</a>
                            </li>
                            <li>
                                <a href="layout-fixed-header.html">Fixed Header</a>
                            </li>
                            <li>
                                <a href="layout-fixed-sidebar.html">Fixed Sidebar</a>
                            </li>
                            <li>
                                <a href="layout-starter-kit.html">Starter Kit</a>
                            </li>
                        </ul>
                    </li>

                    <li class="nav-label">APPS & CHARTS</li>
                    <li>
                        <a href="app-profile.html">
                            <i class="icon-user"></i>
                            <span class="nav-text">Profile</span>
                        </a>
                    </li>
                    <li>
                        <a class="has-arrow" href="#" aria-expanded="false">
                            <i class="icon-graph"></i>
                            <span class="nav-text">Charts</span>
                            <span class="badge badge-warning nav-badge">14</span>
                        </a>
                        <ul aria-expanded="false">
                            <li>
                                <a href="chart-amchart.html">AmChart</a>
                            </li>
                            <li>
                                <a href="chart-c3.html">C3 Chart</a>
                            </li>
                            <li>
                                <a href="chart-flot.html">Flot</a>
                            </li>
                            <li>
                                <a href="chart-morris.html">Morris</a>
                            </li>
                            <li>
                                <a href="chart-chartjs.html">Chartjs</a>
                            </li>
                            <li>
                                <a href="chart-chartjs-candlestick.html">Candlestick</a>
                            </li>
                            <li>
                                <a href="chart-chartjs-data-labels.html"> Datalabels</a>
                            </li>
                            <li>
                                <a href="chart-chartjs-stream.html">Live Stream</a>
                            </li>
                            <li>
                                <a href="chart-chartist.html">Chartist </a>
                            </li>
                            <li>
                                <a href="chart-sparkline.html">Sparkline </a>
                            </li>
                            <li>
                                <a href="chart-echart.html">Echart </a>
                            </li>
                            <li>
                                <a href="chart-google.html">Google </a>
                            </li>
                            <li>
                                <a href="chart-highchart.html">Highchart </a>
                            </li>
                            <li>
                                <a href="chart-rickshaw.html">Rickshaw </a>
                            </li>
                            <li>
                                <a href="chart-justgage.html">Justgage </a>
                            </li>
                            <li>
                                <a href="chart-knob.html">Knob </a>
                            </li>
                            <li>
                                <a href="chart-peity.html">Peity</a>
                            </li>
                        </ul>
                    </li>

                    <li>
                        <a class="has-arrow" href="#" aria-expanded="false">
                            <i class="icon-envelope"></i>
                            <span class="nav-text">Email</span>
                        </a>
                        <ul aria-expanded="false">
                            <li>
                                <a href="email-inbox.html">Inbox</a>
                            </li>
                            <li>
                                <a href="email-read.html">Read</a>
                            </li>
                            <li>
                                <a href="email-compose.html">Compose</a>
                            </li>
                        </ul>
                    </li>
                    <li>
                        <a class="has-arrow" href="#" aria-expanded="false">
                            <i class="icon-calender"></i>
                            <span class="nav-text">Calendar</span>
                        </a>
                        <ul aria-expanded="false">
                            <li>
                                <a href="calender-event.html">Event</a>
                            </li>
                            <li>
                                <a href="calender-date.html">Date</a>
                            </li>
                        </ul>
                    </li>
                    <li>
                        <a class="has-arrow" href="#" aria-expanded="false">
                            <i class="icon-layers"></i>
                            <span class="nav-text">Basic Widget</span>
                        </a>
                        <ul aria-expanded="false">
                            <li>
                                <a href="widget-basic-card.html">Card</a>
                            </li>
                        </ul>
                    </li>
                    <li>
                        <a class="has-arrow" href="#" aria-expanded="false">
                            <i class="icon-pie-chart"></i>
                            <span class="nav-text"> Advanced Widget</span>
                            
                        </a>
                        <ul aria-expanded="false">
                            <li>
                                <a href="widget-advanced-chartjs.html">Chartjs</a>
                            </li>
                            <li>
                                <a href="widget-advanced-media.html">Media Object</a>
                            </li>
                        </ul>
                    </li>

                    <li class="nav-label"> UI &amp; Plugin</li>
                    <li>
                        <a class="has-arrow" href="#" aria-expanded="false">
                            <i class="icon-puzzle"></i>
                            <span class="nav-text">Components</span>
                            
                        </a>
                        <ul aria-expanded="false">
                            <li>
                                <a href="uc-nestedable.html">Nestedable</a>
                            </li>
                            <li>
                                <a href="uc-sweetalert.html"> Sweetalert</a>
                            </li>
                            <li>
                                <a href="uc-toastr.html">Toastr</a>
                            </li>
                            <li>
                                <a href="uc-weather.html">Weather</a>
                            </li>
                        </ul>
                    </li>

                    <li>
                        <a class="has-arrow" href="#" aria-expanded="false">
                            <i class="icon-diamond"></i>
                            <span class="nav-text">UI Features</span>
                            <span class="badge badge-success nav-badge">16</span>
                        </a>
                        <ul aria-expanded="false">
                            <li>
                                <a href="ui-accordion.html">Accordion</a>
                            </li>
                            <li>
                                <a href="ui-alert.html">Alert</a>
                            </li>
                            <li>
                                <a href="ui-badge.html">Badge</a>
                            </li>
                            <li>
                                <a href="ui-button.html">Button</a>
                            </li>
                            <li>
                                <a href="ui-button-group.html">Button Group</a>
                            </li>
                            <li>
                                <a href="ui-cards.html">Cards</a>
                            </li>
                            <li>
                                <a href="ui-carousel.html">Carousel</a>
                            </li>
                            <li>
                                <a href="ui-dropdown.html">Dropdown</a>
                            </li>
                            <li>
                                <a href="ui-list-group.html">List Group</a>
                            </li>
                            <li>
                                <a href="ui-media-object.html">Media Object</a>
                            </li>
                            <li>
                                <a href="ui-modal.html">Modal</a>
                            </li>
                            <li>
                                <a href="ui-pagination.html">Pagination</a>
                            </li>
                            <li>
                                <a href="ui-popover.html">Popover</a>
                            </li>
                            <li>
                                <a href="ui-progressbar.html">Progressbar</a>
                            </li>
                            <li>
                                <a href="ui-tab.html">Tab</a>
                            </li>
                            <li>
                                <a href="ui-typography.html">Typography</a>
                            </li>
                        </ul>
                    </li>

                    <li>
                        <a class="has-arrow" href="#" aria-expanded="false">
                            <i class="icon-bulb"></i>
                            <span class="nav-text">Form Stuff</span>
                            <span class="badge badge-primary nav-badge">14</span>
                        </a>
                        <ul aria-expanded="false">
                            <li>
                                <a href="form-basic.html">Basic Forms</a>
                            </li>
                            <li>
                                <a href="form-layout.html">Form Layout</a>
                            </li>
                            <li>
                                <a href="form-addons.html">Form Addons</a>
                            </li>
                            <li>
                                <a href="form-checkbox.html">Form Checkbox</a>
                            </li>
                            <li>
                                <a href="form-validation.html">Form Validation</a>
                            </li>
                            <li>
                                <a href="form-editor.html">Form Editor</a>
                            </li>
                            <li>
                                <a href="form-icheck.html">Form iCheck</a>
                            </li>
                            <li>
                                <a href="form-pickers.html">Form Pickers</a>
                            </li>
                            <li>
                                <a href="form-radio-button.html">Form Radio Button</a>
                            </li>
                            <li>
                                <a href="form-summernote.html">Form Summernote</a>
                            </li>
                            <li>
                                <a href="form-switch.html">Form Switch</a>
                            </li>
                            <li>
                                <a href="form-typehead.html">Form Typehead</a>
                            </li>
                            <li>
                                <a href="form-xeditable.html">Form Xeditable</a>
                            </li>
                            <li>
                                <a href="form-dropzone.html">Form Dropzone</a>
                            </li>
                        </ul>
                    </li>

                    <li>
                        <a class="has-arrow" href="#" aria-expanded="false">
                            <i class="icon-notebook"></i>
                            <span class="nav-text">BS Table</span>
                        </a>
                        <ul aria-expanded="false">
                            <li>
                                <a href="table-basic.html">
                                    Basic </a>
                            </li>
                            <li>
                                <a href="table-layout.html">
                                    Table Layout </a>
                            </li>
                        </ul>
                    </li>

                    <li>
                        <a class="has-arrow" href="#" aria-expanded="false">
                            <i class="icon-heart"></i>
                            <span class="nav-text">Datatable</span>
                        </a>
                        <ul aria-expanded="false">
                            <li>
                                <a href="table-dt-basic.html">
                                    Datatable Basic </a>
                            </li>
                            <li>
                                <a href="table-dt-basic-button.html">
                                    Datatable Basic Button </a>
                            </li>
                            <li>
                                <a href="table-dt-data-source.html">
                                    Datatable Data Source </a>
                            </li>
                            <li>
                                <a href="table-dt-styling.html">
                                    Datatable Style </a>
                            </li>
                        </ul>
                    </li>
                    <li>
                        <a class="has-arrow" href="#" aria-expanded="false">
                            <i class="icon-plus"></i>
                            <span class="nav-text">Datatable Extension</span>
                            <span class="badge badge-warning nav-badge">10</span>
                        </a>
                        <ul aria-expanded="false">
                            <li>
                                <a href="table-dt-extension-autofill.html">
                                    Autofill </a>
                            </li>
                            <li class="">
                                <a href="table-dt-extension-button-api.html">
                                    Button API </a>
                            </li>
                            <li>
                                <a href="table-dt-extension-button-basic.html">
                                    Button Basic </a>
                            </li>
                            <li>
                                <a href="table-dt-extension-button-column-visibility.html">
                                    Button Column Visibility </a>
                            </li>
                            <li>
                                <a href="table-dt-extension-button-flash-data-export.html">
                                    Flash Data Export </a>
                            </li>
                            <li>
                                <a href="table-dt-extension-button-html5-data-export.html">
                                    HTML5 Data Export </a>
                            </li>
                            <li>
                                <a href="table-dt-extension-button-print.html">
                                    Button Print </a>
                            </li>
                            <li>
                                <a href="table-dt-extension-column-reorder.html">
                                    Column Render </a>
                            </li>
                            <li>
                                <a href="table-dt-extension-fixed-column.html">
                                    Fixed Column </a>
                            </li>
                            <li>
                                <a href="table-dt-extension-key-table.html">
                                    Key Table </a>
                            </li>
                        </ul>
                    </li>

                    <li>
                        <a class="has-arrow" href="#" aria-expanded="false">
                            <i class="icon-map"></i>
                            <span class="nav-text">Map</span>
                        </a>
                        <ul aria-expanded="false">
                            <li>
                                <a href="map-datamap.html">Data Map </a>
                            </li>
                            <li>
                                <a href="map-vectormap.html">Vector Map</a>
                            </li>
                        </ul>
                    </li>

                    <li class="nav-label">Extra</li>

                    <li>
                        <a class="has-arrow" href="#" aria-expanded="false">
                            <i class="icon-layers"></i>
                            <span class="nav-text">Pages</span>
                            
                        </a>
                        <ul aria-expanded="false">
                            <li>
                                <a href="page-pricing.html">Pricing</a>
                            </li>
                            <li>
                                <a href="page-one-column.html">One Column</a>
                            </li>
                        </ul>
                    </li>

                    <li>
                        <a href="page-invoice.html">
                            <i class="icon-note"></i>
                            <span class="nav-text">Invoice Summary</span>
                        </a>
                    </li>

                    <li>
                        <a class="has-arrow" href="#" aria-expanded="false">
                            <i class="icon-arrow-down-circle"></i>
                            <span class="nav-text">Multi Level</span>
                        </a>
                        <ul aria-expanded="false">
                            <li>
                                <a href="#">item 1.1</a>
                            </li>
                            <li>
                                <a href="#">item 1.2</a>
                            </li>
                            <li>
                                <a class="has-arrow" href="#" aria-expanded="false">Menu 1.3</a>
                                <ul aria-expanded="false">
                                    <li>
                                        <a href="#">item 1.3.1</a>
                                    </li>
                                    <li>
                                        <a href="#">item 1.3.2</a>
                                    </li>
                                    <li>
                                        <a href="#">item 1.3.3</a>
                                    </li>
                                    <li>
                                        <a href="#">item 1.3.4</a>
                                    </li>
                                </ul>
                            </li>
                            <li>
                                <a href="#">Last Nav</a>
                            </li>
                        </ul>
                    </li>

                    <li class="">
                        <a href="documentation.html">
                            <i class="icon-book-open"></i>
                            <span class="nav-text">Documentation</span>
                        </a>
                    </li> -->
                </ul>
            </div>
            <!-- #/ nk nav scroll -->
        </div>
        <!-- #/ sidebar -->

        <!-- content body -->
        <div class="content-body">
            <div class="container-fluid">
                <!-- <div class="row page-titles">
                    <div class="col-md-3 col-sm-4 col-lg-3 col-xl-2 p-r-0 align-self-center">
                        <h3 class="text-primary">Dashboard</h3>
                        <ol class="breadcrumb">
                            <li class="breadcrumb-item">
                                <a href="javascript:void(0)">Home</a>
                            </li>
                            <li class="breadcrumb-item active">Murid</li>
                        </ol>
                    </div>
                </div>
                <div class="row">
                    <div class="col-xl-2">
                        <div class="card item_background__color__murid">
                            <div class="card-body">
                                <h1 class="text-center text-white font-weight-bold" style="font-size: 24px;">30 Juli</h1>
                            </div>
                        </div>
                    </div>
                    <div class="col-xl-10">
                        <div class="card">
                            <div class="card-body">
                                <h4 class="card-title font-weight-bold">Kelas Dengan Jongsuk Kim Pukul 19.00 WIB</h4>
                                <p><small>Kelas dimulai dalam Besok Pukul 19.00</small></p>
                            </div>
                        </div>
                    </div>
                </div>

                <div class="row">
                    <div class="col-xl-2">
                        <div class="card item_background__color__murid">
                            <div class="card-body">
                                <h1 class="text-center text-white font-weight-bold" style="font-size: 24px;">29 Juli</h1>
                            </div>
                        </div>
                    </div>
                    <div class="col-xl-10">
                        <div class="card">
                            <div class="card-body">
                                <h4 class="card-title font-weight-bold">Kelas Dengan Jongsuk Kim Pukul 20.30 WIB</h4>
                                <p><small>Kelas dimulai dalam 1 Jam 30 Menit</small></p>
                            </div>
                        </div>
                    </div>
                </div> -->

                <div class="row">
                    <div class="col-lg-12">
                        <div class="card">
                            <div class="card-body">
                                <h4 class="card-title dashboard_admin">DATA PRIBADI</h4>
                                <div class="row">
                                    <img src="{{ asset('img/reza.jpeg') }}" alt="" class="custom__profile_tutor">
                                    <form style="margin-left: 30px;">
                                        <div class="form-group">
                                            <div class="row none__mode text-center">
                                                <div class="col-xs-3 col-lg-4 col-sm-4">
                                                    <div class="nama__parent text-center" style="justify-content: center; align-items: center; margin-right: 25px;">
                                                        <p class="text-center font-weight-bold" style="justify-content: center; align-items: center; margin-top: 8px; color: black;">Nama</p>
                                                    </div>
                                                </div>
                                                <div class="col-xs-9 col-lg-10 col-sm-10">
                                                    <input type="email" disabled class="form-control email__ text__input__height" id="exampleInputEmail1" aria-describedby="emailHelp" placeholder="Jongsuk Kim">
                                                </div>
                                            </div>
                                        </div>
                                        <div class="form-group">
                                            <div class="row none__mode text-center">
                                                <div class="col-xs-3 col-lg-4 col-sm-4">
                                                    <div class="nama__parent text-center" style="justify-content: center; align-items: center; margin-right: 25px;">
                                                        <p class="text-center font-weight-bold" style="justify-content: center; align-items: center; margin-top: 8px; color: black;">Email</p>
                                                    </div>
                                                </div>
                                                <div class="col-xs-9 col-lg-10 col-sm-10">
                                                    <input type="email" disabled class="form-control email__ text__input__height" id="exampleInputEmail1" aria-describedby="emailHelp" placeholder="Jongsuk@gmail.com">
                                                </div>
                                            </div>
                                        </div>

                                        <div class="form-group">
                                            <div class="row none__mode text-center">
                                                <div class="col-xs-3 col-lg-4 col-sm-4">
                                                    <div class="nama__parent text-center" style="justify-content: center; align-items: center; margin-right: 25px;">
                                                        <p class="text-center font-weight-bold" style="justify-content: center; align-items: center; margin-top: 8px; color: black;">Phone</p>
                                                    </div>
                                                </div>
                                                <div class="col-xs-9 col-lg-10 col-sm-10">
                                                    <input type="email" disabled class="form-control email__ text__input__height" id="exampleInputEmail1" aria-describedby="emailHelp" placeholder="083878331195">
                                                </div>
                                            </div>
                                        </div>

                                        <div class="form-group">
                                            <div class="row none__mode text-center">
                                                <div class="col-xs-3 col-lg-4 col-sm-4">
                                                    <div class="nama__parent text-center" style="justify-content: center; align-items: center; margin-right: 25px;">
                                                        <p class="text-center font-weight-bold" style="justify-content: center; align-items: center; margin-top: 8px; color: black;">Jenis Kelamin</p>
                                                    </div>
                                                </div>
                                                <div class="col-xs-9 col-lg-10 col-sm-10">
                                                    <input type="email" disabled class="form-control email__ text__input__height" id="exampleInputEmail1" aria-describedby="emailHelp" placeholder="Laki-Laki">
                                                </div>
                                            </div>
                                        </div>

                                        <div class="form-group">
                                            <div class="row none__mode text-center">
                                                <div class="col-xs-3 col-lg-4 col-sm-4">
                                                    <div class="nama__parent text-center" style="justify-content: center; align-items: center; margin-right: 25px;">
                                                        <p class="font-weight-bold" style="justify-content: center; align-items: center; margin-top: 8px; color: black;">Tempat, Tanggal Lahir</p>
                                                    </div>
                                                </div>
                                                <div class="col-xs-9 col-lg-10 col-sm-10">
                                                    <input type="email" disabled class="form-control email__ text__input__height" id="exampleInputEmail1" aria-describedby="emailHelp" placeholder="Jakarta, 22 Januari 2000">
                                                </div>
                                            </div>
                                        </div>
                                        <div class="button__mode__choose">
                                            <div class="row">
                                                <button type="submit" class="btn btn_edit_murid__" style="margin-right: 15px;">Edit</button>
                                                <button type="submit" class="btn btn__perbarui___profile" style="">Perbaruai</button>
                                            </div>
                                        </div>
                                    </form>
                                </div>
                                <hr style="margin-top: 55px;">
                                <h4 class="dashboard_admin">DATA PENGIRIMAN INSENTIF MENGAJAR</h4>
                                <div class="row mt-4">
                                    <img src="{{ asset('img/gambar_buku_tambungan.jpg') }}" alt="" class="custom__profile_tutor_bukutabungan">
                                    <form style="margin-left: 30px;">
                                        <div class="form-group">
                                            <div class="row none__mode text-center">
                                                <div class="col-xs-3 col-lg-4 col-sm-4">
                                                    <div class="nama__parent text-center" style="justify-content: center; align-items: center; margin-right: 25px;">
                                                        <p class="text-center font-weight-bold" style="justify-content: center; align-items: center; margin-top: 8px; color: black;">Rekening Bank</p>
                                                    </div>
                                                </div>
                                                <div class="col-xs-9 col-lg-10 col-sm-10">
                                                    <input type="email" disabled class="form-control email__ text__input__height" id="exampleInputEmail1" aria-describedby="emailHelp" placeholder="Mandiri">
                                                </div>
                                            </div>
                                        </div>
                                        <div class="form-group">
                                            <div class="row none__mode text-center">
                                                <div class="col-xs-3 col-lg-4 col-sm-4">
                                                    <div class="nama__parent text-center" style="justify-content: center; align-items: center; margin-right: 25px;">
                                                        <p class="text-center font-weight-bold" style="justify-content: center; align-items: center; margin-top: 8px; color: black;">Nomor Rekening</p>
                                                    </div>
                                                </div>
                                                <div class="col-xs-9 col-lg-10 col-sm-10">
                                                    <input type="email" disabled class="form-control email__ text__input__height" id="exampleInputEmail1" aria-describedby="emailHelp" placeholder="234894">
                                                </div>
                                            </div>
                                        </div>

                                        <div class="form-group">
                                            <div class="row none__mode text-center">
                                                <div class="col-xs-3 col-lg-4 col-sm-4">
                                                    <div class="nama__parent text-center" style="justify-content: center; align-items: center; margin-right: 25px;">
                                                        <p class="text-center font-weight-bold" style="justify-content: center; align-items: center; margin-top: 8px; color: black;">Nama Pemilik</p>
                                                    </div>
                                                </div>
                                                <div class="col-xs-9 col-lg-10 col-sm-10">
                                                    <input type="email" disabled class="form-control email__ text__input__height" id="exampleInputEmail1" aria-describedby="emailHelp" placeholder="Jongsuk Kim">
                                                </div>
                                            </div>
                                        </div>

                                        <div class="button__mode__choose">
                                            <div class="row">
                                                <button type="submit" class="btn btn_edit_murid__" style="margin-right: 15px;">Edit</button>
                                                <button type="submit" class="btn btn__perbarui___profile" style="">Perbaruai</button>
                                            </div>
                                        </div>
                                    </form>
                                </div>
                                <hr style="margin-top: 55px;">
                                <h4 class="dashboard_admin">DATA KOMPETENSI</h4>
                                <div class="row mt-4">
                                    <form style="margin-left: 30px;">
                                        <div class="form-group">
                                            <div class="row none__mode">
                                                <div class="col-xs-3 col-lg-3 col-sm-3">
                                                    <div class="nama__parent " style="margin-right: 25px;">
                                                        <p class="font-weight-bold" style="margin-top: 8px; color: black;">Link URL Video</p>
                                                    </div>
                                                </div>
                                                <div class="col-xs-9 col-lg-10 col-sm-10 pr-0">
                                                    <input type="email" disabled class="form-control email__ text__input__height" id="exampleInputEmail1" aria-describedby="emailHelp" placeholder="www.youtube.com/jongsuk-kim">
                                                    
                                                </div>
                                                <div class="btn__delete">
                                                    <button type="submit" class="btn btn__delete__"><i class="fas fa-pencil-alt"></i></button>
                                                </div>
                                            </div>
                                        </div>
                                        <div class="form-group">
                                            <div class="row none__mode">
                                                <div class="col-xs-3 col-lg-3 col-sm-3">
                                                    <div class="nama__parent " style="margin-right: 25px;">
                                                        <p class="font-weight-bold" style="margin-top: 8px; color: black;">Tentang Kamu</p>
                                                    </div>
                                                </div>
                                                <div class="col-xs-9 col-lg-10 col-sm-10 pr-0">
                                                    <textarea name="tentangkamu" disabled class="form-control" id="" cols="30" rows="10">Saat ini saya merupakan pengajar bahasa korea di beberapa lembaga bahasa asing. Profesi sebagai pengajar bahasa korea sudah saya lakukan sejak 5 tahun lalu. Mengajar adalah passion saya.
                                                    </textarea>
                                                </div>
                                                <div class="btn__delete">
                                                    <button type="submit" class="btn btn__delete__"><i class="fas fa-pencil-alt"></i></button>
                                                </div>
                                            </div>
                                        </div>

                                        <div class="form-group">
                                            <div class="row none__mode">
                                                <div class="col-xs-3 col-lg-3 col-sm-3">
                                                    <div class="nama__parent" style="margin-right: 25px;">
                                                        <p class="font-weight-bold" style="margin-top: 8px; color: black;">Bahasa Yang Dikuasai</p>
                                                    </div>
                                                </div>
                                                <div class="bahasa_yang_dikuasai">
                                                    <div class="row  ml-4">
                                                        <div class="item__list_1">
                                                            <div class="row">
                                                                <div class="text__input_form_bahasa_yang_dikuasai">
                                                                    <input type="email" disabled class="form-control email__ text__input__height" id="exampleInputEmail1" aria-describedby="emailHelp" placeholder="Korean">
                                                                </div>
                                                                <div class="btn__delete">
                                                                    <button type="submit" class="btn btn__delete__"><i class="fas fa-times"></i></button>
                                                                </div>
                                                            </div>
                                                        </div>
                                                        <div class="item__list_1 ml-5">
                                                            <div class="row">
                                                                <div class="text__input_form_bahasa_yang_dikuasai">
                                                                    <input type="email" disabled class="form-control email__ text__input__height" id="exampleInputEmail1" aria-describedby="emailHelp" placeholder="Japanese">
                                                                </div>
                                                                <div class="btn__delete">
                                                                    <button type="submit" class="btn btn__delete__"><i class="fas fa-times"></i></button>
                                                                </div>
                                                            </div>
                                                        </div>
                                                    </div>

                                                    <div class="row ml-4 mt-4">
                                                        <div class="item__list_1">
                                                            <div class="row">
                                                                <div class="text__input_form_bahasa_yang_dikuasai">
                                                                    <input type="email" disabled class="form-control email__ text__input__height" id="exampleInputEmail1" aria-describedby="emailHelp" placeholder="English">
                                                                </div>
                                                                <div class="btn__delete">
                                                                    <button type="submit" class="btn btn__delete__"><i class="fas fa-times"></i></button>
                                                                </div>
                                                            </div>
                                                        </div>
                                                    </div>
                                                    <div class="row mt-4 ml-3">
                                                        <button type="submit" class="btn btn__tambah__profile_tutor">Tambah</button>
                                                    </div>
                                                </div>
                                            </div>
                                        </div>

                                        <div class="form-group">
                                            <div class="row none__mode">
                                                <div class="col-xs-3 col-lg-3 col-sm-3">
                                                    <div class="nama__parent" style="margin-right: 25px;">
                                                        <p class="font-weight-bold" style="margin-top: 8px; color: black;">Materi Yang Dikuasai</p>
                                                    </div>
                                                </div>
                                                <div class="materi__bahasa__dikuasai">
                                                    <div class="row ml-3">
                                                        <div class="text__input__profile">
                                                            <input type="email" disabled class="form-control email__ text__input__height custom__width_" id="exampleInputEmail1" aria-describedby="emailHelp" placeholder="Bahasa Keperluan Akademik(Academic), Materi Beginner">
                                                        </div>
                                                        <div class="btn__delete">
                                                            <button type="submit" class="btn btn__delete__"><i class="fas fa-times"></i></button>
                                                        </div>
                                                    </div>

                                                    <div class="row mt-4 ml-3">
                                                        <div class="text__input__profile">
                                                            <input type="email" disabled class="form-control email__ text__input__height custom__width_" id="exampleInputEmail1" aria-describedby="emailHelp" placeholder="Bahasa Keperluan Perjalanan(Traveler), Materi Intermediate">
                                                        </div>
                                                        <div class="btn__delete">
                                                            <button type="submit" class="btn btn__delete__"><i class="fas fa-times"></i></button>
                                                        </div>
                                                    </div>

                                                    <div class="row mt-4 ml-3">
                                                        <div class="text__input__profile">
                                                            <input type="email" disabled class="form-control email__ text__input__height custom__width_" id="exampleInputEmail1" aria-describedby="emailHelp" placeholder="Bahasa Keperluan Karir(Career Builder), Materi Proficient">
                                                        </div>
                                                        <div class="btn__delete">
                                                            <button type="submit" class="btn btn__delete__"><i class="fas fa-times"></i></button>
                                                        </div>
                                                    </div>
                                                    <div class="row mt-4 ml-3">
                                                        <button type="submit" class="btn btn__tambah__profile_tutor">Tambah</button>
                                                    </div>
                                                </div>
                                            </div>
                                        </div>

                                        <div class="form-group">
                                            <div class="row none__mode">
                                                <div class="col-xs-3 col-lg-3 col-sm-3">
                                                    <div class="nama__parent" style="margin-right: 25px;">
                                                        <p class="font-weight-bold" style="margin-top: 8px; color: black;">Biaya Pengajaran / Jam</p>
                                                    </div>
                                                </div>
                                                <div class="biaya_perjam">
                                                    <div class="row ml-3">
                                                        <div class="text__input__profile">
                                                            <input type="email" disabled class="form-control email__ text__input__height custom__width_" id="exampleInputEmail1" aria-describedby="emailHelp" placeholder="Rp 100.000">
                                                        </div>
                                                        <div class="btn__delete">
                                                            <button type="submit" class="btn btn__delete__"><i class="fas fa-pencil-alt"></i></button>
                                                        </div>
                                                    </div>
                                                </div>
                                            </div>
                                        </div>

                                        <div class="form-group">
                                            <div class="row none__mode">
                                                <div class="col-xs-3 col-lg-3 col-sm-3">
                                                    <div class="nama__parent" style="margin-right: 25px;">
                                                        <p class="font-weight-bold" style="margin-top: 8px; color: black;">Bahasa Yang Dikuasai</p>
                                                    </div>
                                                </div>
                                                <div class="bahasa_yang_dikuasai">
                                                    <div class="row  ml-4">
                                                        <div class="item__list_1">
                                                            <div class="row">
                                                                <div class="text_form_bahasa__profile_tanggal">
                                                                    <input type="email" disabled class="form-control email__ text__input__height" id="exampleInputEmail1" aria-describedby="emailHelp" placeholder="30 Juli 2019, Pukul 19.00 WIB">
                                                                </div>
                                                                <div class="btn__delete">
                                                                    <button type="submit" class="btn btn__delete__"><i class="fas fa-times"></i></button>
                                                                </div>
                                                            </div>
                                                        </div>
                                                        <div class="item__list_1 ml-5">
                                                            <div class="row">
                                                                <div class="text_form_bahasa__profile_tanggal">
                                                                    <input type="email" disabled class="form-control email__ text__input__height" id="exampleInputEmail1" aria-describedby="emailHelp" placeholder="30 Juli 2019, Pukul 21.00 WIB">
                                                                </div>
                                                                <div class="btn__delete">
                                                                    <button type="submit" class="btn btn__delete__"><i class="fas fa-times"></i></button>
                                                                </div>
                                                            </div>
                                                        </div>
                                                    </div>

                                                    <div class="row ml-4 mt-4">
                                                        <div class="item__list_1">
                                                            <div class="row">
                                                                <div class="text_form_bahasa__profile_tanggal">
                                                                    <input type="email" disabled class="form-control email__ text__input__height" id="exampleInputEmail1" aria-describedby="emailHelp" placeholder="29 Juli 2019, Pukul 13.00 WIB">
                                                                </div>
                                                                <div class="btn__delete">
                                                                    <button type="submit" class="btn btn__delete__"><i class="fas fa-times"></i></button>
                                                                </div>
                                                            </div>
                                                        </div>
                                                    </div>
                                                    <div class="row mt-4 ml-3">
                                                        <button type="submit" class="btn btn__tambah__profile_tutor">Tambah</button>
                                                    </div>
                                                    <div class="row mt-5">
                                                        <div class="button__mode__choose">
                                                            <div class="row">
                                                                <button type="submit" class="btn btn_edit_murid__" style="margin-right: 15px;">Edit</button>
                                                                <button type="submit" class="btn btn__perbarui___profile" style="">Perbaruai</button>
                                                            </div>
                                                        </div>
                                                    </div>
                                                </div>
                                            </div>
                                        </div>

                                        <!-- <div class="button__mode__choose">
                                            <div class="row">
                                                <button type="submit" class="btn btn_edit_murid__" style="margin-right: 15px;">Edit</button>
                                                <button type="submit" class="btn btn__perbarui___profile" style="">Perbaruai</button>
                                            </div>
                                        </div> -->
                                    </form>
                                </div>
                            </div>
                        </div>
                    </div>
                </div>

                <!-- <div class="row">
                    <div class="col-lg-12 col-xl-4">
                        <div class="card">
                            <div class="card-body">
                                <h4 class="card-title">BTC</h4>
                                <div id="morris-area-chart1"></div>
                            </div>
                        </div>
                    </div>
                    <div class="col-lg-12 col-xl-4">
                        <div class="card">
                            <div class="card-body">
                                <h4 class="card-title">BTC vs LTC</h4>
                                <div id="morris-area-chart2"></div>
                            </div>
                        </div>
                    </div>
                    <div class="col-lg-12 col-xl-4">
                        <div class="card">
                            <div class="card-body">
                                <h4 class="card-title">BTC vs LTC vs NEO</h4>
                                <div id="morris-area-chart3"></div>
                            </div>
                        </div>
                    </div>
                </div>
                <div class="row">
                    <div class="col-lg-12 col-xl-5">
                        <div class="accordion" id="accordion-two">
                            <div class="card">
                                <div class="card-header">
                                    <h4 aria-controls="collapseOne1" aria-expanded="false" data-target="#collapseOne1" data-toggle="collapse" class="mb-0 collapsed">
                                        <i aria-hidden="true" class="fa"></i>
                                        Limit
                                    </h4>
                                </div>

                                <div data-parent="#accordion-two" class="collapse" id="collapseOne1" style="">
                                    <div class="card-body">
                                        <div class="row">
                                            <div class="col-12 col-xl-6">
                                                <div class="row m-b-20">
                                                    <div class="col-4">
                                                        <h5 class="text-bold-600 mb-0">Buy BTC</h5>
                                                    </div>
                                                    <div class="col-8 text-right">
                                                        <p class="text-muted mb-0">USD Balance: $ 5000.00</p>
                                                    </div>
                                                </div>
                                                <form class="form form-horizontal">
                                                    <div class="form-body">
                                                        <div class="form-group row">
                                                            <label class="col-md-4 col-form-label" for="btc-limit-buy-price">Price</label>
                                                            <div class="col-md-8">
                                                                <input type="number" id="btc-limit-buy-price" class="form-control" placeholder="$ 11916.9" name="btc-limit-buy-price">
                                                            </div>
                                                        </div>
                                                        <div class="form-group row">
                                                            <label class="col-md-4 col-form-label" for="btc-limit-buy-amount">Amount</label>
                                                            <div class="col-md-8">
                                                                <input type="number" id="btc-limit-buy-amount" class="form-control" placeholder="0.026547 BTC" name="btc-limit-buy-amount">
                                                            </div>
                                                        </div>
                                                        <div class="form-group row">
                                                            <label class="col-md-4 col-form-label" for="btc-limit-buy-total">Total</label>
                                                            <div class="col-md-8">
                                                                <input type="number" disabled id="btc-limit-buy-total" class="form-control" placeholder="$ 318.1856" name="btc-limit-buy-total">
                                                            </div>
                                                        </div>
                                                        <div class="form-actions pb-0">
                                                            <button type="submit" class="btn round btn-success btn-block btn-glow"> Buy BTC </button>
                                                        </div>
                                                    </div>
                                                </form>
                                            </div>
                                            <div class="col-12 col-xl-6">
                                                <div class="row m-b-20">
                                                    <div class="col-4">
                                                        <h5 class="text-bold-600 mb-0">Sell BTC</h5>
                                                    </div>
                                                    <div class="col-8 text-right">
                                                        <p class="text-muted mb-0">BTC Balance: 1.2654898</p>
                                                    </div>
                                                </div>
                                                <form class="form form-horizontal">
                                                    <div class="form-body">
                                                        <div class="form-group row">
                                                            <label class="col-md-4 col-form-label">Price</label>
                                                            <div class="col-md-8">
                                                                <input type="number" id="btc-limit-sell-price" class="form-control" placeholder="$ 11916.9" name="btc-limit-sell-price">
                                                            </div>
                                                        </div>
                                                        <div class="form-group row">
                                                            <label class="col-md-4 col-form-label" for="btc-limit-sell-amount">Amount</label>
                                                            <div class="col-md-8">
                                                                <input type="number" id="btc-limit-sell-amount" class="form-control" placeholder="0.026547 BTC" name="btc-limit-sell-amount">
                                                            </div>
                                                        </div>
                                                        <div class="form-group row">
                                                            <label class="col-md-4 col-form-label" for="btc-limit-sell-total">Total</label>
                                                            <div class="col-md-8">
                                                                <input type="number" disabled id="btc-limit-sell-total" class="form-control" placeholder="$ 318.1856" name="btc-limit-sell-total">
                                                            </div>
                                                        </div>
                                                        <div class="form-actions pb-0">
                                                            <button type="submit" class="btn round btn-danger btn-block btn-glow"> Sell BTC </button>
                                                        </div>
                                                    </div>
                                                </form>
                                            </div>
                                        </div>
                                    </div>
                                </div>
                            </div>
                            <div class="card">
                                <div class="card-header">
                                    <h4 aria-controls="collapseTwo2" aria-expanded="true" data-target="#collapseTwo2" data-toggle="collapse" class="mb-0">
                                        <i aria-hidden="true" class="fa"></i>
                                        Market
                                    </h4>
                                </div>
                                <div data-parent="#accordion-two" class="collapse show" id="collapseTwo2" style="">
                                    <div class="card-body">
                                        <div class="row">
                                            <div class="col-12 col-xl-6">
                                                <div class="row m-b-20">
                                                    <div class="col-4">
                                                        <h5 class="text-bold-600 mb-0">Buy BTC</h5>
                                                    </div>
                                                    <div class="col-8 text-right">
                                                        <p class="text-muted mb-0">USD Balance: $ 5000.00</p>
                                                    </div>
                                                </div>
                                                <form class="form form-horizontal">
                                                    <div class="form-body">
                                                        <div class="form-group row">
                                                            <label class="col-md-4 col-form-label" for="btc-market-buy-price">Price</label>
                                                            <div class="col-md-8">
                                                                <input type="number" disabled id="btc-market-buy-price" class="form-control" placeholder="Market prise $" name="btc-market-buy-price">
                                                            </div>
                                                        </div>
                                                        <div class="form-group row">
                                                            <label class="col-md-4 col-form-label" for="btc-market-buy-amount">Amount</label>
                                                            <div class="col-md-8">
                                                                <input type="number" id="btc-market-buy-amount" class="form-control" placeholder="0.026547 BTC" name="btc-market-buy-amount">
                                                            </div>
                                                        </div>
                                                        <div class="form-actions pb-0">
                                                            <button type="submit" class="btn round btn-success btn-block btn-glow"> Buy BTC </button>
                                                        </div>
                                                    </div>
                                                </form>
                                            </div>
                                            <div class="col-12 col-xl-6">
                                                <div class="row my-2 p-r-15 p-l-15">
                                                    <div class="col-4">
                                                        <h5 class="text-bold-600 mb-0">Sell BTC</h5>
                                                    </div>
                                                    <div class="col-8 text-right">
                                                        <p class="text-muted mb-0">BTC Balance: 1.2654898</p>
                                                    </div>
                                                </div>
                                                <form class="form form-horizontal">
                                                    <div class="form-body">
                                                        <div class="form-group row">
                                                            <label class="col-md-4 col-form-label">Price</label>
                                                            <div class="col-md-8">
                                                                <input type="number" disabled id="btc-market-sell-price" class="form-control" placeholder="Market prise $" name="btc-market-sell-price">
                                                            </div>
                                                        </div>
                                                        <div class="form-group row">
                                                            <label class="col-md-4 col-form-label" for="btc-market-sell-amount">Amount</label>
                                                            <div class="col-md-8">
                                                                <input type="number" id="btc-market-sell-amount" class="form-control" placeholder="0.026547 BTC" name="btc-market-sell-amount">
                                                            </div>
                                                        </div>
                                                        <div class="form-actions pb-0">
                                                            <button type="submit" class="btn round btn-danger btn-block btn-glow"> Sell BTC </button>
                                                        </div>
                                                    </div>
                                                </form>
                                            </div>
                                        </div>
                                    </div>
                                </div>
                            </div>
                            <div class="card">
                                <div class="card-header">
                                    <h4 aria-controls="collapseThree3" aria-expanded="false" data-target="#collapseThree3" data-toggle="collapse" class="mb-0 collapsed">
                                        <i aria-hidden="true" class="fa"></i>
                                        Stop Limit
                                    </h4>
                                </div>
                                <div data-parent="#accordion-two" class="collapse" id="collapseThree3">
                                    <div class="card-body">
                                        <div class="row">
                                            <div class="col-12 col-xl-6">
                                                <div class="row m-b-20">
                                                    <div class="col-4">
                                                        <h5 class="text-bold-600 mb-0">Buy BTC</h5>
                                                    </div>
                                                    <div class="col-8 text-right">
                                                        <p class="text-muted mb-0">USD Balance: $ 5000.00</p>
                                                    </div>
                                                </div>
                                                <form class="form form-horizontal">
                                                    <div class="form-body">
                                                        <div class="form-group row">
                                                            <label class="col-md-4 col-form-label" for="btc-stop-buy">Stop</label>
                                                            <div class="col-md-8">
                                                                <input type="number" id="btc-stop-buy" class="form-control" placeholder="$ 11916.9" name="btc-stop-buy">
                                                            </div>
                                                        </div>
                                                        <div class="form-group row">
                                                            <label class="col-md-4 col-form-label" for="btc-stop-buy-limit">Limit</label>
                                                            <div class="col-md-8">
                                                                <input type="number" id="btc-stop-buy-limit" class="form-control" placeholder="$ 12000.0" name="btc-stop-buy-limit">
                                                            </div>
                                                        </div>
                                                        <div class="form-group row">
                                                            <label class="col-md-4 col-form-label" for="btc-stop-buy-amount">Amount</label>
                                                            <div class="col-md-8">
                                                                <input type="number" id="btc-stop-buy-amount" class="form-control" placeholder="0.026547 BTC" name="btc-stop-buy-amount">
                                                            </div>
                                                        </div>
                                                        <div class="form-group row">
                                                            <label class="col-md-4 col-form-label" for="btc-stop-buy-total">Total</label>
                                                            <div class="col-md-8">
                                                                <input type="number" disabled id="btc-stop-buy-total" class="form-control" placeholder="$ 318.1856" name="btc-stop-buy-total">
                                                            </div>
                                                        </div>
                                                        <div class="form-actions pb-0">
                                                            <button type="submit" class="btn round btn-success btn-block btn-glow"> Buy BTC </button>
                                                        </div>
                                                    </div>
                                                </form>
                                            </div>
                                            <div class="col-12 col-xl-6">
                                                <div class="row my-2 p-r-15 p-l-15">
                                                    <div class="col-4">
                                                        <h5 class="text-bold-600 mb-0">Sell BTC</h5>
                                                    </div>
                                                    <div class="col-8 text-right">
                                                        <p class="text-muted mb-0">BTC Balance: 1.2654898</p>
                                                    </div>
                                                </div>
                                                <form class="form form-horizontal">
                                                    <div class="form-body">
                                                        <div class="form-group row">
                                                            <label class="col-md-4 col-form-label" for="btc-stop-sell">Stop</label>
                                                            <div class="col-md-8">
                                                                <input type="number" id="btc-stop-sell" class="form-control" placeholder="$ 11916.9" name="btc-stop-sell">
                                                            </div>
                                                        </div>
                                                        <div class="form-group row">
                                                            <label class="col-md-4 col-form-label" for="btc-stop-sell-limit">Limit</label>
                                                            <div class="col-md-8">
                                                                <input type="number" id="btc-stop-sell-limit" class="form-control" placeholder="$ 12000.0" name="btc-stop-sell-limit">
                                                            </div>
                                                        </div>
                                                        <div class="form-group row">
                                                            <label class="col-md-4 col-form-label" for="btc-stop-sell-amount">Amount</label>
                                                            <div class="col-md-8">
                                                                <input type="number" id="btc-stop-sell-amount" class="form-control" placeholder="0.026547 BTC" name="btc-stop-sell-amount">
                                                            </div>
                                                        </div>
                                                        <div class="form-group row">
                                                            <label class="col-md-4 col-form-label" for="btc-stop-sell-total">Total</label>
                                                            <div class="col-md-8">
                                                                <input type="number" disabled id="btc-stop-sell-total" class="form-control" placeholder="$ 318.1856" name="btc-stop-sell-total">
                                                            </div>
                                                        </div>
                                                        <div class="form-actions pb-0">
                                                            <button type="submit" class="btn round btn-danger btn-block btn-glow"> Sell BTC </button>
                                                        </div>
                                                    </div>
                                                </form>
                                            </div>
                                        </div>
                                    </div>
                                </div>
                            </div>
                        </div>
                    </div>
                    <div class="col-lg-12 col-xl-7">
                        <div class="card">
                            <div class="card-body">
                                <div class="card-title">
                                    <h4>Active Order</h4>
                                </div>
                                <div class="table-responsive">
                                    <table class="table table-de mb-0">
                                        <thead>
                                            <tr>
                                                <th>Date</th>
                                                <th>Type</th>
                                                <th>Amount BTC</th>
                                                <th>BTC Remaining</th>
                                                <th>Price</th>
                                                <th>USD</th>
                                                <th>Fee (%)</th>
                                                <th>Cancel</th>
                                            </tr>
                                        </thead>
                                        <tbody>
                                            <tr>
                                                <td>2018-01-31 06:51:51</td>
                                                <td class="success">Buy</td>
                                                <td>
                                                    <i class="cc BTC"></i> 0.58647</td>
                                                <td>
                                                    <i class="cc BTC"></i> 0.58647</td>
                                                <td>11900.12</td>
                                                <td>$ 6979.78</td>
                                                <td>0.2</td>
                                                <td>
                                                    <button class="btn btn-sm round btn-outline-danger"> Cancel</button>
                                                </td>
                                            </tr>
                                            <tr>
                                                <td>2018-01-31 06:50:50</td>
                                                <td class="danger">Sell</td>
                                                <td>
                                                    <i class="cc BTC"></i> 1.38647</td>
                                                <td>
                                                    <i class="cc BTC"></i> 0.38647</td>
                                                <td>11905.09</td>
                                                <td>$ 4600.97</td>
                                                <td>0.2</td>
                                                <td>
                                                    <button class="btn btn-sm round btn-outline-danger"> Cancel</button>
                                                </td>
                                            </tr>
                                            <tr>
                                                <td>2018-01-31 06:49:51</td>
                                                <td class="success">Buy</td>
                                                <td>
                                                    <i class="cc BTC"></i> 0.45879</td>
                                                <td>
                                                    <i class="cc BTC"></i> 0.45879</td>
                                                <td>11901.85</td>
                                                <td>$ 5460.44</td>
                                                <td>0.2</td>
                                                <td>
                                                    <button class="btn btn-sm round btn-outline-danger"> Cancel</button>
                                                </td>
                                            </tr>
                                            <tr>
                                                <td>2018-01-31 06:51:51</td>
                                                <td class="success">Buy</td>
                                                <td>
                                                    <i class="cc BTC"></i> 0.89877</td>
                                                <td>
                                                    <i class="cc BTC"></i> 0.89877</td>
                                                <td>11899.25</td>
                                                <td>$ 10694.6</td>
                                                <td>0.2</td>
                                                <td>
                                                    <button class="btn btn-sm round btn-outline-danger"> Cancel</button>
                                                </td>
                                            </tr>
                                            <tr>
                                                <td>2018-01-31 06:51:51</td>
                                                <td class="danger">Sell</td>
                                                <td>
                                                    <i class="cc BTC"></i> 0.45712</td>
                                                <td>
                                                    <i class="cc BTC"></i> 0.45712</td>
                                                <td>11908.58</td>
                                                <td>$ 5443.65</td>
                                                <td>0.2</td>
                                                <td>
                                                    <button class="btn btn-sm round btn-outline-danger"> Cancel</button>
                                                </td>
                                            </tr>
                                            <tr>
                                                <td>2018-01-31 06:51:51</td>
                                                <td class="success">Buy</td>
                                                <td>
                                                    <i class="cc BTC"></i> 0.58647</td>
                                                <td>
                                                    <i class="cc BTC"></i> 0.58647</td>
                                                <td>11900.12</td>
                                                <td>$ 6979.78</td>
                                                <td>0.2</td>
                                                <td>
                                                    <button class="btn btn-sm round btn-outline-danger"> Cancel</button>
                                                </td>
                                            </tr>
                                        </tbody>
                                    </table>
                                </div>
                            </div>
                        </div>
                    </div>
                </div> -->
            </div>
            <!-- #/ container -->
        </div>
        <!-- #/ content body -->

        <!-- MODAL BATAL -->
            <!-- Modal -->
            <div class="modal fade" id="exampleModalCenter">
                <div class="modal-dialog modal-dialog-centered" role="document">
                    <div class="modal-content">
                        <div class="modal-header diplasy__off">
                            <h2 class="modal-title text-center">Apa Kamu Yakin?</h5>
                            <button type="button" class="close btn__close__" data-dismiss="modal">
                                <span>&times;</span>
                            </button>
                        </div>
                        <div class="d-flex justify-content-center">
                            <p class="text-center" style="width: 340px;">Dengan membatalkan pesanan ini uang pendaftaran kamu tidak dapat dikembalikan</p>
                        </div>
                        <div class="modal-footer border_bottom__off">
                            <button type="button" class="btn btn_kembali__modal__murid" data-dismiss="modal">Kembali</button>
                            <button type="button" class="btn btn_oke__modal__murid">Oke</button>
                        </div>
                    </div>
                </div>
            </div>
        <!-- END MODAL BATAL -->

        <!-- footer -->
        <div class="footer">
            <div class="copyright d-flex justify-content-center">
                <p>Copyright &copy;
                    <a href="home">Halloworld</a> 2019</p>
            </div>
        </div>
        <!-- #/ footer -->

    </div>

    <!-- Common JS -->
    <script src="plugins/common/common.min.js"></script>
    <!-- Custom script -->
    <script src="js/custom.min.js"></script>

    <!-- Morris Chart -->
    <script src="plugins/morris/raphael-min.js"></script>
    <script src="plugins/morris/morris.js"></script> 
    <!-- Custom dashboard script -->
    <script src="js/dashboard-1.js"></script>
    <script src="js/dashboard-2.js"></script>

    <!-- dataTables -->
    <script src="plugins/datatables/datatables.min.js"></script>
    <script src="plugins/datatables/datatables-init.js"></script>

    <script src="plugins/sparkline/jquery.sparkline.min.js"></script>

    <script>

// $(function () {
//     "use strict";
//     function sparkline_charts() {
//         $('.sparkline-marketcap').sparkline('html');
//     }
//     if ($('.sparkline-marketcap').length) {
//         sparkline_charts();
//     }

// }); 
</script>

</body>

</html>