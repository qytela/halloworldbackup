<!DOCTYPE html>
<html lang="en">

<head>
    <meta charset="utf-8">
    <meta http-equiv="X-UA-Compatible" content="IE=edge">
    <meta name="viewport" content="width=device-width, initial-scale=1">
    <title>Dashboard Tutor</title>
    <!-- Favicon icon -->
    <!-- <link rel="icon" type="image/png" sizes="16x16" href="../assets/images/favicon.png"> -->
    <!-- Custom Stylesheet -->
    <link href="{{ asset('css/style.css') }}" rel="stylesheet">
    <script src="{{ asset('js/modernizr-3.6.0.min.js') }}"></script>

    <!-- RANTING JQUERY -->
    <link rel="stylesheet" href="https://cdnjs.cloudflare.com/ajax/libs/rateYo/2.3.2/jquery.rateyo.min.css">

</head>

<body>

    <div id="preloader">
        <div class="loader">
            <div class="loader__bar"></div>
            <div class="loader__bar"></div>
            <div class="loader__bar"></div>
            <div class="loader__bar"></div>
            <div class="loader__bar"></div>
            <div class="loader__ball"></div>
        </div>
    </div>

    <div id="main-wrapper">

        <!-- header -->
        <div class="header">
            <div class="nav-header">
                <div class="brand-logo">
                    <a href="index.html">
                        <!-- <i class="cc BTC"></i> -->
                        <img src="{{ asset('img/korea_tutor.png') }}" class="cc BTC"  alt="">
                        <span class="brand-title">
                            <img src="{{ asset('img/Logo-Halloworld.png') }}" alt="">
                        </span>
                    </a>
                </div>

                <div class="nav-control">
                    <div class="hamburger">
                        <span class="line"></span>
                        <span class="line"></span>
                        <span class="line"></span>
                    </div>
                </div>
            </div>

            <div class="header-content">
                <div class="header-left">
                    <ul>
                        <li class="icons position-relative">
                            <a href="javascript:void(0)">
                                <i class="icon-magnifier f-s-16"></i>
                            </a>
                            <div class="drop-down animated bounceInDown">
                                <div class="dropdown-content-body">
                                    <div class="header-search" id="header-search">
                                        <form action="#">
                                            <div class="input-group">
                                                <input type="text" class="form-control" placeholder="Search">
                                                <div class="input-group-append">
                                                    <span class="input-group-text">
                                                        <i class="icon-magnifier"></i>
                                                    </span>
                                                </div>
                                            </div>
                                        </form>
                                    </div>
                                </div>
                            </div>
                        </li>
                    </ul>

                </div>
                <div class="header-right">
                    <ul>
                        <li class="icons">
                            <a href="javascript:void(0)">
                                <i class="icon-bell f-s-18" aria-hidden="true"></i>
                                <div class="pulse-css"></div>
                            </a>
                            <div class="drop-down animated bounceInDown">
                                <div class="dropdown-content-heading">
                                    <span class="text-left">Recent Notifications</span>
                                </div>
                                <div class="dropdown-content-body">
                                    <ul>
                                        <li>
                                            <a href="#">
                                                <img class="pull-left m-r-10 avatar-img" src="../assets/images/avatar/1.jpg" alt="" />
                                                <div class="notification-content">
                                                    <small class="notification-timestamp pull-right">02:34 PM</small>
                                                    <div class="notification-heading">Mr. Saifun</div>
                                                    <div class="notification-text">5 members joined today </div>
                                                </div>
                                            </a>
                                        </li>
                                        <li>
                                            <a href="#">
                                                <img class="pull-left m-r-10 avatar-img" src="../assets/images/avatar/2.jpg" alt="" />
                                                <div class="notification-content">
                                                    <small class="notification-timestamp pull-right">02:34 PM</small>
                                                    <div class="notification-heading">Mariam</div>
                                                    <div class="notification-text">likes a photo of you</div>
                                                </div>
                                            </a>
                                        </li>
                                        <li>
                                            <a href="#">
                                                <img class="pull-left m-r-10 avatar-img" src="../assets/images/avatar/3.jpg" alt="" />
                                                <div class="notification-content">
                                                    <small class="notification-timestamp pull-right">02:34 PM</small>
                                                    <div class="notification-heading">Tasnim</div>
                                                    <div class="notification-text">Hi Teddy, Just wanted to let you ...</div>
                                                </div>
                                            </a>
                                        </li>
                                        <li>
                                            <a href="#">
                                                <img class="pull-left m-r-10 avatar-img" src="../assets/images/avatar/4.jpg" alt="" />
                                                <div class="notification-content">
                                                    <small class="notification-timestamp pull-right">02:34 PM</small>
                                                    <div class="notification-heading">Ishrat Jahan</div>
                                                    <div class="notification-text">Hi Teddy, Just wanted to let you ...</div>
                                                </div>
                                            </a>
                                        </li>
                                        <li class="text-center">
                                            <a href="#" class="more-link">See All</a>
                                        </li>
                                    </ul>
                                </div>
                            </div>
                        </li>
                        <li class="icons">
                            <a href="javascript:void(0)">
                                <i class="icon-envelope f-s-18" aria-hidden="true"></i>
                                <div class="pulse-css"></div>
                            </a>
                            <div class="drop-down animated bounceInDown">
                                <div class="dropdown-content-heading">
                                    <span class="text-left">2 New Messages</span>
                                </div>
                                <div class="dropdown-content-body">
                                    <ul>
                                        <li class="notification-unread">
                                            <a href="#">
                                                <img class="pull-left m-r-10 avatar-img" src="../assets/images/avatar/1.jpg" alt="" />
                                                <div class="notification-content">
                                                    <small class="notification-timestamp pull-right">02:34 PM</small>
                                                    <div class="notification-heading">Saiul Islam</div>
                                                    <div class="notification-text">Hi Teddy, Just wanted to let you ...</div>
                                                </div>
                                            </a>
                                        </li>
                                        <li class="notification-unread">
                                            <a href="#">
                                                <img class="pull-left m-r-10 avatar-img" src="../assets/images/avatar/2.jpg" alt="" />
                                                <div class="notification-content">
                                                    <small class="notification-timestamp pull-right">02:34 PM</small>
                                                    <div class="notification-heading">Ishrat Jahan</div>
                                                    <div class="notification-text">Hi Teddy, Just wanted to let you ...</div>
                                                </div>
                                            </a>
                                        </li>
                                        <li>
                                            <a href="#">
                                                <img class="pull-left m-r-10 avatar-img" src="../assets/images/avatar/3.jpg" alt="" />
                                                <div class="notification-content">
                                                    <small class="notification-timestamp pull-right">02:34 PM</small>
                                                    <div class="notification-heading">Saiul Islam</div>
                                                    <div class="notification-text">Hi Teddy, Just wanted to let you ...</div>
                                                </div>
                                            </a>
                                        </li>
                                        <li>
                                            <a href="#">
                                                <img class="pull-left m-r-10 avatar-img" src="../assets/images/avatar/4.jpg" alt="" />
                                                <div class="notification-content">
                                                    <small class="notification-timestamp pull-right">02:34 PM</small>
                                                    <div class="notification-heading">Ishrat Jahan</div>
                                                    <div class="notification-text">Hi Teddy, Just wanted to let you ...</div>
                                                </div>
                                            </a>
                                        </li>
                                        <li class="text-center">
                                            <a href="#" class="more-link">See All</a>
                                        </li>
                                    </ul>
                                </div>
                            </div>
                        </li>
                        <li class="icons">
                            <a href="javascript:void(0)">
                                <i class="icon-note f-s-18" aria-hidden="true"></i>
                                <div class="pulse-css"></div>
                            </a>
                            <div class="drop-down dropdown-task animated bounceInDown">
                                <div class="dropdown-content-heading">
                                    <span class="text-left">Task Update</span>
                                </div>
                                <div class="dropdown-content-body">
                                    <ul>
                                        <li>
                                            <a href="#">
                                                <div class="notification-content">
                                                    <small class="notification-timestamp pull-right">85% Complete</small>
                                                    <div class="notification-heading">Task One</div>
                                                    <div class="progress">
                                                        <div style="width: 85%;" aria-valuemax="100" aria-valuemin="0" aria-valuenow="85" role="progressbar" class="progress-bar progress-bar-success"></div>
                                                    </div>
                                                </div>
                                            </a>
                                        </li>
                                        <li>
                                            <a href="#">
                                                <div class="notification-content">
                                                    <small class="notification-timestamp pull-right">60% Complete</small>
                                                    <div class="notification-heading">Task Two</div>
                                                    <div class="progress">
                                                        <div style="width: 60%;" aria-valuemax="100" aria-valuemin="0" aria-valuenow="60" role="progressbar" class="progress-bar progress-bar-primary"></div>
                                                    </div>
                                                </div>
                                            </a>
                                        </li>
                                        <li>
                                            <a href="#">
                                                <div class="notification-content">
                                                    <small class="notification-timestamp pull-right">25% Complete</small>
                                                    <div class="notification-heading">Task Three</div>
                                                    <div class="progress">
                                                        <div style="width: 25%;" aria-valuemax="100" aria-valuemin="0" aria-valuenow="25" role="progressbar" class="progress-bar progress-bar-warning"></div>
                                                    </div>
                                                </div>
                                            </a>
                                        </li>
                                        <li>
                                            <a href="#">
                                                <div class="notification-content">
                                                    <small class="notification-timestamp pull-right">75% Complete</small>
                                                    <div class="notification-heading">Task Four</div>
                                                    <div class="progress">
                                                        <div style="width: 75%;" aria-valuemax="100" aria-valuemin="0" aria-valuenow="75" role="progressbar" class="progress-bar progress-bar-danger"></div>
                                                    </div>
                                                </div>
                                            </a>
                                        </li>
                                        <li class="text-center">
                                            <a href="#" class="more-link">See All</a>
                                        </li>
                                    </ul>
                                </div>
                            </div>
                        </li>
                        <li class="icons">
                            <a href="javascript:void(0)">
                                <i class="icon-user f-s-18" aria-hidden="true"></i>
                            </a>
                            <div class="drop-down dropdown-profile animated bounceInDown">
                                <div class="dropdown-content-body">
                                    <ul>
                                        <li>
                                            <a href="#">
                                                <i class="icon-envelope"></i>
                                                <span>Inbox</span>
                                            </a>
                                        </li>
                                        <li>
                                            <a href="#">
                                                <i class="icon-settings"></i>
                                                <span>Setting</span>
                                            </a>
                                        </li>
                                        <li>
                                            <a href="#">
                                                <i class="icon-lock"></i>
                                                <span>Lock Screen</span>
                                            </a>
                                        </li>
                                        <li>
                                            <a href="home">
                                                <i class="icon-power"></i>
                                                <span>Logout</span>
                                            </a>
                                        </li>
                                    </ul>
                                </div>
                            </div>
                        </li>
                    </ul>
                </div>
            </div>
        </div>
        <!-- #/ header -->

        <!-- sidebar -->
        <div class="nk-sidebar">
            <div class="nk-nav-scroll">

                <div class="nav-user">
                    <img src="{{ asset('images/'.$dataTutor->photo_profile) }}" alt="" class="rounded-circle">
                    <h5>Hi, {{ $dataTutor->fullname }}</h5>
                    <!-- <p>Web Developer</p> -->

                    <div class="nav-user-option" style="margin-top: 10px;">
                        <div class="setting-option">
                            <div data-toggle="dropdown">
                                <i class="icon-settings"></i>
                            </div>
                            <div class="dropdown-menu animated flipInX">
                                <a class="dropdown-item" href="#">Account</a>
                                <a class="dropdown-item" href="#">Lock</a>
                                <a class="dropdown-item" href="home">Logout</a>
                            </div>
                        </div>
                        <div class="notification-option">
                            <div data-toggle="dropdown">
                                <i class="icon-bell"></i>
                            </div>
                            <div class="dropdown-menu animated flipInX">
                                <a class="dropdown-item" href="#">Email
                                    <span class="badge badge-primary pull-right m-t-3">05</span>
                                </a>
                                <a class="dropdown-item" href="#">Feed back
                                    <span class="badge badge-danger pull-right m-t-3">02</span>
                                </a>
                                <a class="dropdown-item" href="#">Report
                                    <span class="badge badge-warning pull-right m-t-3">02</span>
                                </a>
                            </div>
                        </div>
                    </div>
                </div>

                <ul class="metismenu" id="menu">
                    <li class="nav-label">Menu</li>
                    <li>
                        <a href="{{ URL::to('/tutor/home') }}" class="text-center">
                            <span class="nav-text text-center">Dashboard</span>
                        </a>
                    </li>
                    <li>
                        <a href="{{ URL::to('/tutor/table')}}" class="text-center">
                            <span class="nav-text text-center">Pesanan Masuk</span>
                        </a>
                    </li>
                    <li>
                        <a href="profiletutor" class="text-center">
                            <span class="nav-text text-center">Profil</span>
                        </a>
                    </li>
                    <li class="keluar_dashboard">
                        <a href="{{ URL::to('/tutor/logout') }}" class="d-flex justify-content-center mt-2">
                            <i class="icon-logout "></i>
                            <!-- <i class="fas fa-sign-out-alt"></i> -->
                            <span class="nav-text">Keluar</span>
                        </a>
                    </li>
                </ul>
            </div>
            <!-- #/ nk nav scroll -->
        </div>
        <!-- #/ sidebar -->

        <!-- content body -->
        <div class="content-body">
            <div class="container-fluid">
                <div class="row page-titles">
                    <div class="col-md-3 col-sm-4 col-lg-3 col-xl-2 p-r-0 align-self-center">
                        <h3 class="text-primary">Dashboard</h3>
                        <ol class="breadcrumb">
                            <li class="breadcrumb-item">
                                <a href="javascript:void(0)">Home</a>
                            </li>
                            <li class="breadcrumb-item active">Tutor</li>
                        </ol>
                    </div>
                    <div class="col-md-9 col-sm-8 col-lg-9 col-xl-10 align-self-center">
                        <div class="crypto-ticker">
                            <ul id="webticker-dark-icons">
                                <li data-update="item1">
                                    <i class="cc BTC"></i> BTC
                                    <span class="coin-value"> $11.039232</span>
                                </li>
                                <li data-update="item2">
                                    <i class="cc ETH"></i> ETH
                                    <span class="coin-value"> $1.2792</span>
                                </li>
                                <li data-update="item3">
                                    <i class="cc GAME"></i> GAME
                                    <span class="coin-value"> $11.039232</span>
                                </li>
                                <li data-update="item4">
                                    <i class="cc LBC"></i> LBC
                                    <span class="coin-value"> $0.588418</span>
                                </li>
                                <li data-update="item5">
                                    <i class="cc NEO"></i> NEO
                                    <span class="coin-value"> $161.511</span>
                                </li>
                                <li data-update="item6">
                                    <i class="cc STEEM"></i> STE
                                    <span class="coin-value"> $0.551955</span>
                                </li>
                                <li data-update="item7">
                                    <i class="cc LTC"></i> LIT
                                    <span class="coin-value"> $177.80</span>
                                </li>
                                <li data-update="item8">
                                    <i class="cc NOTE"></i> NOTE
                                    <span class="coin-value"> $13.399</span>
                                </li>
                                <li data-update="item9">
                                    <i class="cc MINT"></i> MINT
                                    <span class="coin-value"> $0.880694</span>
                                </li>
                                <li data-update="item10">
                                    <i class="cc IOTA"></i> IOT
                                    <span class="coin-value"> $2.555</span>
                                </li>
                                <li data-update="item11">
                                    <i class="cc DASH"></i> DAS
                                    <span class="coin-value"> $769.22</span>
                                </li>
                            </ul>
                        </div>
                    </div>
                </div>

                <div class="row">
                    <div class="col-xl-3 col-lg-6 col-md-6 no-card-border">
                        <div class="card background_pesanan_aktif">
                            <div class="card-body ">
                                <h5 class="pull-left putih__">Pesanan Aktif</h5>
                                <!-- <div class="dropdown custom-dropdown pull-right">
                                    <div data-toggle="dropdown">
                                        <i class="ti-more-alt rotate-90 d-inline-block c-pointer"></i>
                                    </div>
                                    <div class="dropdown-menu dropdown-menu-right">
                                        <a class="dropdown-item f-s-12" href="#">Details</a>
                                        <a class="dropdown-item f-s-12" href="#">Add New</a>
                                        <a class="dropdown-item f-s-12" href="#">Refresh</a>
                                    </div>
                                </div> -->
                                <div class="clearfix"></div>
                                <div class="m-t-15 text-center">
                                    <h2 class="f-s-50 putih__">25</h2>
                                    <!-- <p class="f-s-14">Coin Purchase</p> -->
                                    </div>
                                
                            </div>
                            <!-- <div class="card-footer">
                                <h6 class="m-t-10 text-muted">Coin Status
                                    <span class="pull-right">50%</span>
                                </h6>
                            </div>
                            <div class="progress h-3px">
                                <div class="progress-bar animated progress-animated bg-primary w-50pc h-3px" role="progressbar">
                                </div>
                            </div> -->
                        </div>
                    </div>
                    <div class="col-xl-3 col-lg-6 col-md-6 no-card-border">
                        <div class="card background_pesanan_selesai">
                            <div class="card-body">
                                <h5 class="pull-left putih__">Pesanan Selesai</h5>
                                <!-- <div class="dropdown custom-dropdown pull-right">
                                    <div data-toggle="dropdown" aria-expanded="false">
                                        <i class="ti-more-alt rotate-90 d-inline-block c-pointer"></i>
                                    </div>
                                    <div class="dropdown-menu dropdown-menu-right">
                                        <a class="dropdown-item f-s-12" href="#">Details</a>
                                        <a class="dropdown-item f-s-12" href="#">Add New</a>
                                        <a class="dropdown-item f-s-12" href="#">Refresh</a>
                                    </div>
                                </div> -->
                                <div class="clearfix"></div>
                                <div class="m-t-15 text-center">
                                    <h2 class="f-s-50 putih__">15</h2>
                                    <!-- <p class="f-s-14">Coin Hold</p> -->
                                </div>
                            </div>
                            <!-- <div class="card-footer">
                                <h6 class="m-t-10 text-muted">Coin Status
                                    <span class="pull-right">50%</span>
                                </h6>
                            </div>
                            <div class="progress h-3px">
                                <div class="progress-bar animated progress-animated bg-success w-50pc h-3px" role="progressbar">
                                </div>
                            </div> -->
                        </div>
                    </div>
                    <div class="col-xl-3 col-lg-6 col-md-6 no-card-border">
                        <div class="card background_pesanan_batal">
                            <div class="card-body">
                                <h5 class="pull-left putih__">Pesanan Batal</h5>
                                <!-- <div class="dropdown custom-dropdown pull-right">
                                    <div data-toggle="dropdown">
                                        <i class="ti-more-alt rotate-90 d-inline-block c-pointer"></i>
                                    </div>
                                    <div class="dropdown-menu dropdown-menu-right">
                                        <a class="dropdown-item f-s-12" href="#">Details</a>
                                        <a class="dropdown-item f-s-12" href="#">Add New</a>
                                        <a class="dropdown-item f-s-12" href="#">Refresh</a>
                                    </div>
                                </div> -->
                                <div class="clearfix"></div>
                                <div class="m-t-15 text-center">
                                    <h2 class="f-s-50 putih__">10</h2>
                                    <!-- <p class="f-s-14">Coin Sold</p> -->
                                </div>
                            </div>
                            <!-- <div class="card-footer">
                                <h6 class="m-t-10 text-muted">Coin Status
                                    <span class="pull-right">50%</span>
                                </h6>
                            </div>
                            <div class="progress h-3px">
                                <div class="progress-bar animated progress-animated bg-danger w-50pc h-3px" role="progressbar">
                                </div>
                            </div> -->
                        </div>
                    </div>
                    <div class="col-xl-3 col-lg-6 col-md-6 no-card-border">
                        <div class="card background_total_pesanan">
                            <div class="card-body">
                                <h5 class="pull-left putih__">Total Pesanan</h5>
                                <!-- <div class="dropdown custom-dropdown pull-right">
                                    <div data-toggle="dropdown">
                                        <i class="ti-more-alt rotate-90 d-inline-block c-pointer"></i>
                                    </div>
                                    <div class="dropdown-menu dropdown-menu-right">
                                        <a class="dropdown-item f-s-12" href="#">Details</a>
                                        <a class="dropdown-item f-s-12" href="#">Add New</a>
                                        <a class="dropdown-item f-s-12" href="#">Refresh</a>
                                    </div>
                                </div> -->
                                <div class="clearfix"></div>
                                <div class="m-t-15 text-center">
                                    <h2 class="f-s-50 putih__">25</h2>
                                    <!-- <p class="f-s-14">Coin Back</p> -->
                                </div>
                            </div>
                            <!-- <div class="card-footer">
                                <h6 class="m-t-10 text-muted">Coin Status
                                    <span class="pull-right">50%</span>
                                </h6>
                            </div>
                            <div class="progress h-3px">
                                <div class="progress-bar animated progress-animated bg-warning w-50pc h-3px" role="progressbar">
                                </div>
                            </div> -->
                        </div>
                    </div>
                </div>

                <!-- Baris kedua dari atas -->

                <div class="row">
                    <div class="col-xl-5 col-lg-6 col-md-6 no-card-border">
                        <div class="card background_total_transaksi">
                            <div class="card-body">
                                <h5 class="pull-left putih__" style="width: 180px;">Total Penyeselesaian Dari Pesanan Masuk</h5>
                                
                                <div class="clearfix"></div>
                                <div class="m-t-15">
                                    <h2 class="f-s-50 putih__" style="margin-bottom: 45px;">25%</h2>
                                    <!-- <p class="f-s-14">Coin Sold</p> -->
                                </div>
                            </div>
                            <!-- <div class="card-footer">
                                <h6 class="m-t-10 text-muted">Coin Status
                                    <span class="pull-right">50%</span>
                                </h6>
                            </div>
                            <div class="progress h-3px">
                                <div class="progress-bar animated progress-animated bg-danger w-50pc h-3px" role="progressbar">
                                </div>
                            </div> -->
                        </div>
                    </div>
                    <div class="col-xl-6 col-lg-6 col-md-6 no-card-border">
                        <div class="card background_total_transaksi">
                            <div class="card-body">
                                <h5 class="pull-left putih__">Saldo Insentif</h5>
                                <!-- <div class="dropdown custom-dropdown pull-right">
                                    <div data-toggle="dropdown">
                                        <i class="ti-more-alt rotate-90 d-inline-block c-pointer"></i>
                                    </div>
                                    <div class="dropdown-menu dropdown-menu-right">
                                        <a class="dropdown-item f-s-12" href="#">Details</a>
                                        <a class="dropdown-item f-s-12" href="#">Add New</a>
                                        <a class="dropdown-item f-s-12" href="#">Refresh</a>
                                    </div>
                                </div> -->
                                <div class="clearfix"></div>
                                <div class="m-t-15">
                                    <h2 class="f-s-50 putih__">Rp. 20.315.000</h2>
                                    <!-- <p class="f-s-14">Coin Sold</p> -->
                                </div>
                                <div class="m-t-15">
                                    <a href="" class="btn btn-detail-dashboard-admin">Tarik Saldo</a>
                                    <!-- <p class="f-s-14">Coin Sold</p> -->
                                </div>
                            </div>
                        </div>
                    </div>
                </div>
                @foreach ($dataTable as $rows)
                <div class="row">
                    <div class="col-xl-2">
                        <div class="card item_background__color__murid">
                            <div class="card-body">
                                <h1 class="text-center text-white font-weight-bold" style="font-size: 24px;">{{ date('d M', strtotime($rows['transaction_date'])) }}</h1>
                            </div>
                        </div>
                    </div>
                    <div class="col-xl-10">
                        <div class="card">
                            <div class="card-body">
                                <div class="row">
                                    <div class="title_notification" style="margin-left: 20px;">
                                        <h4 class="card-title font-weight-bold">Kelas Dengan {{ $rows['name_student'] }} Pukul {{ $rows['time_create'] }} WIB</h4>
                                        <p><small>Kelas dimulai dalam Pukul {{ $rows['time_create'] }}</small></p>
                                    </div>
                                    <div class="button__notification__dashboard_tutor d-flex">
                                        <a href="" class="btn btn-masuk__dashboard__tutor">Masuk</a>
                                    </div>
                                </div>
                            </div>
                        </div>
                    </div>
                </div>
                @endforeach
                <div class="row">
                    <div class="col-lg-12">
                        <div class="card">
                            <div class="card-body">
                                <h4 class="card-title">Riwayat Pesanan</h4>
                                <div class="table-responsive">
                                    <table id="myTable" class="table table-xs">
                                        <thead>
                                            <tr>
                                                <th>No Tagihan</th>
                                                <th>Tanggal Transaksi</th>
                                                <th>Total Insentif</th>
                                                <th>Nama Murid</th>
                                                <th>Ulasan Murid</th>
                                                <th>Bintang</th>
                                            </tr>
                                        </thead>
                                        <tbody>
                                            <tr>
                                                <td class="">
                                                    <p>INV/20190727/VII/3062</p>
                                                </td>
                                                <td class="">
                                                    <p>27 Juli 2019</p>
                                                </td>
                                                <td class="">
                                                    <p>Rp 90.000</p>
                                                </td>
                                                <td class="">
                                                    <p>Mega</p>
                                                </td>
                                                <td class="">
                                                    <p>Recommended!!!</p>
                                                </td>
                                                <td class="">
                                                    <div id="rating__tutor" class="rateYo" style="margin-top: 10px;"></div>
                                                </td>
                                            </tr>
                                            <tr>
                                                <td class="">
                                                    <p>INV/20190727/VII/3062</p>
                                                </td>
                                                <td class="">
                                                    <p>27 Juli 2019</p>
                                                </td>
                                                <td class="">
                                                    <p>Rp 90.000</p>
                                                </td>
                                                <td class="">
                                                    <p>Mega</p>
                                                </td>
                                                <td class="">
                                                    <p>Recommended!!!</p>
                                                </td>
                                                <td class="">
                                                    <div id="rating__tutor" class="rateYo" style="margin-top: 10px;"></div>
                                                </td>
                                            </tr>
                                            <tr>
                                                <td class="">
                                                    <p>INV/20190727/VII/3062</p>
                                                </td>
                                                <td class="">
                                                    <p>27 Juli 2019</p>
                                                </td>
                                                <td class="">
                                                    <p>Rp 90.000</p>
                                                </td>
                                                <td class="">
                                                    <p>Mega</p>
                                                </td>
                                                <td class="">
                                                    <p>Recommended!!!</p>
                                                </td>
                                                <td class="">
                                                    <div id="rating__tutor" class="rateYo" style="margin-top: 10px;"></div>
                                                </td>
                                            </tr>
                                            <tr>
                                                <td class="">
                                                    <p>INV/20190727/VII/3062</p>
                                                </td>
                                                <td class="">
                                                    <p>27 Juli 2019</p>
                                                </td>
                                                <td class="">
                                                    <p>Rp 90.000</p>
                                                </td>
                                                <td class="">
                                                    <p>Mega</p>
                                                </td>
                                                <td class="">
                                                    <p>Recommended!!!</p>
                                                </td>
                                                <td class="">
                                                    <div id="rating__tutor" class="rateYo" style="margin-top: 10px;"></div>
                                                </td>
                                            </tr>
                                            <tr>
                                                <td class="">
                                                    <p>INV/20190727/VII/3062</p>
                                                </td>
                                                <td class="">
                                                    <p>27 Juli 2019</p>
                                                </td>
                                                <td class="">
                                                    <p>Rp 90.000</p>
                                                </td>
                                                <td class="">
                                                    <p>Mega</p>
                                                </td>
                                                <td class="">
                                                    <p>Recommended!!!</p>
                                                </td>
                                                <td class="">
                                                    <div id="rating__tutor" class="rateYo" style="margin-top: 10px;"></div>
                                                </td>
                                            </tr>
                                        </tbody>
                                    </table>
                                </div>
                            </div>
                        </div>
                    </div>
                </div>
            </div>
            <!-- #/ container -->
        </div>
        <!-- #/ content body -->

        <!-- footer -->
        <div class="footer">
            <div class="copyright d-flex justify-content-center">
                <p>Copyright &copy;
                    <a href="home">Halloworld</a> 2019</p>
            </div>
        </div>
        <!-- #/ footer -->

    </div>

    <!-- Common JS -->
    <script src="{{ asset('plugins/common/common.min.js') }}"></script>
    <!-- Custom script -->
    <script src="{{ asset('js/custom.min.js') }}"></script>

    <!-- Morris Chart -->
    <script src="{{ asset('plugins/morris/raphael-min.js') }}"></script>
    <script src="{{ asset('plugins/morris/morris.js') }}"></script> 
    <!-- Custom dashboard script -->
    <script src="{{ asset('js/dashboard-1.js') }}"></script>
    <script src="{{ asset('js/dashboard-2.js') }}"></script>

    <!-- dataTables -->
    <script src="{{ asset('plugins/datatables/datatables.min.js') }}"></script>
    <script src="{{ asset('plugins/datatables/datatables-init.js') }}"></script>

    <script src="{{ asset('plugins/sparkline/jquery.sparkline.min.js') }}"></script>

    <!-- RANTING JQUERY -->
    <script src="https://cdnjs.cloudflare.com/ajax/libs/rateYo/2.3.2/jquery.rateyo.min.js"></script>

    <script type="text/javascript">
            let demoRatings = [5, 4, 2],
                stars       = $('.rateYo');
                
            for (let i = 0; i < stars.length; i++) {
                // console.log(demoRatings[i]);
                $('.rateYo').rateYo({
                    fullStar: true,
                    rating: demoRatings[i],
                    starWidth: "18px",
                    readOnly: true
                });
            }
</script>

</body>

</html>