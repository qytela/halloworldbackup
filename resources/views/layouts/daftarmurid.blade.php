@extends('base.jaditutorbase')
@push('styles')

@endpush
@section('img_header')
<div id="background_pages_jaditutor"  class="background_orange_jaditutor">
    <h4>KAMU MEMILIKI KEMAMPUAN BERBAHASA ASING?</h4>
    <h6>DAFTARKAN DIRI KAMU SEGERA DAN DAPATKAN PENGHASILAN</h6>
</div>


@section('content')
<div id="section_1" class="inputan_jaditutor__pg mt-5">
    <div class="container test">
        <div class="row">
            <div class="input__namadepan">
                <p>NAMA DEPAN</p>
                <input type="text" class="form-control" placeholder="">
            </div>
            <div class="input_namabelakang">
                <p>NAMA BELAKANG</p>
                <input type="text" class="form-control" placeholde="">
            </div>
        </div>
        <div class="row">
            <div class="input__email mt-5">
                <p>EMAIL</p>
                <input type="email" name="email" class="form-control" id="email" placeholder="">
            </div>
        </div>
        <div class="row">
            <div class="choose__jenis-kelamin mt-5">
                <p>JENIS KELAMIN</p>
                <div class="row">
                    <div class="custom-control custom-radio mt-2 laki-laki-radiobutton">
                        <input type="radio" class="custom-control-input" id="defaultUnchecked" name="defaultExampleRadios">
                        <label class="custom-control-label lbl-radiobutton-lakilaki" for="defaultUnchecked">LAKI-LAKI</label>
                    </div>

                    <div class="custom-control custom-radio mt-2 perempuan-radiobutton">
                        <input type="radio" class="custom-control-input" id="defaultChecked" name="defaultExampleRadios" checked>
                        <label class="custom-control-label lbl-radiobutton-perempuan" for="defaultChecked">PEREMPUAN</label>
                    </div>
                </div>
            </div>
        </div>
        <div class="row">
            <div class="input_tgl-lahir mt-5">
                <p>TEMPAT, TANGGAL LAHIR</p>
               
                <div class="row">
                    <div class="col col-sm-5">
                        <div class="input_tempat">
                            <input type="text" class="form-control">
                        </div>
                    </div>
                    <div class="col col-sm-5">
                        <div class="input_tgl">
                            <input type="date" name="tgl_lahir" class="form-control"  id="tgl_lahir">
                        </div>
                    </div>
                </div>
            </div>
        </div>
        <div class="row">
            <div class="input_bahasayang_dikuasai mt-5">
                <p>BAHASA YANG DIKUASAI</p>
                <input type="text" class="form-control" placeholder="">
            </div>
        </div>
        <div class="row">
            <div class="link_url-introduction mt-5">
                <p>LINK URL INTRODUCTION</p>
                <input type="text" class="form-control" placeholder="">
            </div>
        </div>
        <div class="row">
            <div class="upload__image-tutor mt-5">
                <div class="show__foto">
                    <img src="{{ asset('img/tutor_kelasbahasa_1.jpg') }}" alt="">
                </div>
                <div class="file-upload-wrapper">
                    <input type="file" id="input-file-now-custom-2" class="file-upload"
                    data-height="500" data-default-file="https://mdbootstrap.com/img/Photos/Others/images/89.jpg" />
                </div>
            </div>
        </div>
        <div class="row">
            <div class="tentang__kamu mt-5">
                <p>TENTANG KAMU</p>
                <textarea class="form-control" name="tentang_kamu" id="" cols="30" rows="10"></textarea>
            </div>
        </div>
        <div class="row">
            <div class="biaya__ mt-5">
                <p>BIAYA PER/JAM</p>
                <input type="text" class="form-control" placeholder="">
            </div>
        </div>
        <div class="row">
            <div class="ketersediaan_mengajar mt-5">
                <p>KETERSEDIAN MENGAJAR</p>
                <div class="row">
                    <div class="col">
                        <input type="date" name="ketersediaan" id="dateformat" class="form-control" placeholder="">
                    </div>
                </div>
            </div>
        </div>
        <div class="row">
            <div class="col">
                <div class="check__ mt-5 mb-3">
                    <input type="checkbox" name="check" id="check">
                    <label for="check" class="lbl-text-check">TERMS AND CONDITION</label>
                </div>
            </div>
        </div>
        <div class="row">     
            <div class="button_submit">
                <button type="submit" class="btn btn_submit__">SUBMIT</button>
            </div>
        
            <div class="button_cancel">
                <button type="submit" class="btn btn-cancel">CANCEL</button>
            </div>
        </div>
    </div>
</div>

<!--=========MODAL===============MODAL===========MODAL==========MODAL============MODAL======-->
<div class="modal fade" id="perkenalantutor" tabindex="-1" role="dialog" ria-labelledby="exampleModalLabel" aria-hidden="true">
    <div class="modal-dialog modal-dialog_caritutor_perkenalan" role="document">
        <div class="modal-content modal__cariTutor-content">
            <div class="modal-header">
                <div class="d-flex justify-content-center video-container">
                <iframe 
                    class="video__yt"
                    width="600" height="370" 
                    src="https://www.youtube.com/embed/FcOctsNXyjk" frameborder="0" 
                    allow="accelerometer; autoplay; encrypted-media; 
                    gyroscope; picture-in-picture" allowfullscreen>
                </iframe>
                    
                </div>
                <button 
                    type="button" class="close btn_close-modal-caritutor" style="margin-left: -10px;" data-dismiss="modal" aria-label="Close">
                    <span aria-hidden="true">&times;</span>
                </button>
            </div>
            <div class="modal-body">
                <div class="my__information__modal">
                    <div class="row">
                        <div class="modal__profile-image__tutor">
                            <img src="{{ asset('img/tutor_kelasbahasa_3.jpg') }}" class="img__modal-tutor img-thumbnail" alt="img-thumbnail">
                            <div class="d-flex justify-content-center">
                                <div class="row">
                                    <small class="text-muted">BRITISH ENGLISH</small>
                                </div>
                            </div>
                            <div class="d-flex justify-content-center">
                                <div class="row">
                                    <small class="modal_tutor-item-waktu text-muted">05:49</small>
                                    <small class="text-muted" style="margin-left: 4px; margin-right: 4px;">UTC+</small>
                                    <small class="text-muted">06:00</small>
                                </div>
                            </div>
                        </div>
                        <div class="modal__profile-name">
                            <div class="row">
                                <h4 class="name__tutor-modal">BRITNEY</h4>
                                <div class="mr-auto" style="right: 15px; position: absolute;">
                                    <button type="submit" class="btn btn-modal__pesan btn_text-modal">
                                        <small class="text-button_modal">PESAN</small>
                                    </button>
                                </div>
                            </div>
                            <small class="text-muted">Online</small>
                            <div class="row">
                                <small style="margin-left: 15px; margin-top:15px; color: #01579B;" class="tutor___desc">Tutor Komunitas</small>
                                <small style="margin-left: 5px; margin-right: 5px; margin-top:15px; color: #01579B;" class="">From</small>
                                <small class="text-muted" style="margin-top:15px;">Cardiff,</small>
                                <small class="text-muted" style="margin-top:15px;">England</small>
                            </div>
                            <div class="row">
                                <small class="text-muted" style="margin-left: 15px; margin-top:15px;">Tutor Bahasa</small>
                                <small class="text-muted" style="margin-left: 5px; margin-top:15px;">Inggris (British)</small>
                            </div>
                            <div class="feature__tutor-penilaian">
                                <div class="row">
                                    <div class="penilaian__tutor-modal">
                                        <small class="penilai__feature">Penilaian</small>
                                        <div class="d-flex justify-content-center">
                                            <div class="row">
                                                <div id="rateYo" class="rating__jquery-modal mb-4"></div>
                                                <div style="font-size: 12px; font-weight: bold ">5.0</div>
                                            </div>
                                        </div>
                                    </div>

                                    <div class="murid__tutor-modal">
                                        <small class="penilai__feature">Murid</small>
                                        <div class="d-flex justify-content-center">
                                            <div class="row">
                                                
                                                <div style="font-size: 16px; font-weight: bold ">71</div>
                                            </div>
                                        </div>
                                    </div>

                                    <div class="kelas__tutor-modal">
                                        <small class="penilai__feature">Kelas</small>
                                        <div class="d-flex justify-content-center">
                                            <div class="row">
                                               
                                                <div style="font-size: 16px; font-weight: bold ">50</div>
                                            </div>
                                        </div>
                                    </div>
                                </div>
                            </div>
                        </div>
                    </div>
                </div>
            </div>
            <div class="modal-footer">
                <a href="#">Lupa Password?</a>
                
            </div>
        </div>
    </div>
</div>
<!--==========MODAL=======MODAL============MODAL===========MODAL================MODAL=======-->


@push('scripts')
    <script type="text/javascript">
        $('.file-upload').file_upload();
        $('#dateformat').datepicker({
            format: 'dd-mm-yyyy'
        });
    </script>
@endpush