@extends('base.enterprisebase')
@push('styles')

@endpush
@section('img_header')
<div id="background_pages_enterprise"  class="background_orange_enterprise">
    <h4>KELAS ENTERPRISE</h4>
    <h6>INGIN TINGKATKAN KEMAMPUAN BERBICARA ASING KARYAWAN ANDA?</h6>
</div>


@section('content')
<div id="section_1" class="inputan_enterprise__pg mt-5">
    <div class="container">
        <div class="row mt-5">
            <div class="col-md-3">
                <div class="nama__perusahaan">
                    <p>NAMA PERUSAHAAN</p>
                </div>
            </div>
            <div class="col-md-6">    
                <div class="inputan__nama_perusahaan">
                    <input type="text" class="form-control" placeholder="">
                </div>
            </div>
        </div>
        <div class="row mt-5">
            <div class="col-md-3">
                <div class="nomor__telepon">
                    <p>NOMOR TELEPON</p>
                </div>
            </div>
            <div class="col-md-6">
                <div class="inputan__nomor_telepon mt-2">
                    <input type="number" name="phone" class="form-control" id="">
                </div>
            </div>
        </div>
        <div class="row mt-5">
            <div class="col-md-3">
                <div class="nama__pesan__">
                    <p>PESAN</p>
                </div>
            </div>
            <div class="col-md-6">
                <div class="inputan__pesan__">
                    <textarea name="pesan" id="pesan" class="form-control" cols="30" rows="10"></textarea>
                    <button type="submit" class="btn btn-enterprise">SUBMIT</button>
                </div>
            </div>
        </div>
    </div>
</div>

<!--=========MODAL===============MODAL===========MODAL==========MODAL============MODAL======-->
<div class="modal fade" id="perkenalantutor" tabindex="-1" role="dialog" ria-labelledby="exampleModalLabel" aria-hidden="true">
    <div class="modal-dialog modal-dialog_caritutor_perkenalan" role="document">
        <div class="modal-content modal__cariTutor-content">
            <div class="modal-header">
                <div class="d-flex justify-content-center video-container">
                <iframe 
                    class="video__yt"
                    width="600" height="370" 
                    src="https://www.youtube.com/embed/FcOctsNXyjk" frameborder="0" 
                    allow="accelerometer; autoplay; encrypted-media; 
                    gyroscope; picture-in-picture" allowfullscreen>
                </iframe>
                    
                </div>
                <button 
                    type="button" class="close btn_close-modal-caritutor" style="margin-left: -10px;" data-dismiss="modal" aria-label="Close">
                    <span aria-hidden="true">&times;</span>
                </button>
            </div>
            <div class="modal-body">
                <div class="my__information__modal">
                    <div class="row">
                        <div class="modal__profile-image__tutor">
                            <img src="{{ asset('img/tutor_kelasbahasa_3.jpg') }}" class="img__modal-tutor img-thumbnail" alt="img-thumbnail">
                            <div class="d-flex justify-content-center">
                                <div class="row">
                                    <small class="text-muted">BRITISH ENGLISH</small>
                                </div>
                            </div>
                            <div class="d-flex justify-content-center">
                                <div class="row">
                                    <small class="modal_tutor-item-waktu text-muted">05:49</small>
                                    <small class="text-muted" style="margin-left: 4px; margin-right: 4px;">UTC+</small>
                                    <small class="text-muted">06:00</small>
                                </div>
                            </div>
                        </div>
                        <div class="modal__profile-name">
                            <div class="row">
                                <h4 class="name__tutor-modal">BRITNEY</h4>
                                <div class="mr-auto" style="right: 15px; position: absolute;">
                                    <button type="submit" class="btn btn-modal__pesan btn_text-modal">
                                        <small class="text-button_modal">PESAN</small>
                                    </button>
                                </div>
                            </div>
                            <small class="text-muted">Online</small>
                            <div class="row">
                                <small style="margin-left: 15px; margin-top:15px; color: #01579B;" class="tutor___desc">Tutor Komunitas</small>
                                <small style="margin-left: 5px; margin-right: 5px; margin-top:15px; color: #01579B;" class="">From</small>
                                <small class="text-muted" style="margin-top:15px;">Cardiff,</small>
                                <small class="text-muted" style="margin-top:15px;">England</small>
                            </div>
                            <div class="row">
                                <small class="text-muted" style="margin-left: 15px; margin-top:15px;">Tutor Bahasa</small>
                                <small class="text-muted" style="margin-left: 5px; margin-top:15px;">Inggris (British)</small>
                            </div>
                            <div class="feature__tutor-penilaian">
                                <div class="row">
                                    <div class="penilaian__tutor-modal">
                                        <small class="penilai__feature">Penilaian</small>
                                        <div class="d-flex justify-content-center">
                                            <div class="row">
                                                <div id="rateYo" class="rating__jquery-modal mb-4"></div>
                                                <div style="font-size: 12px; font-weight: bold ">5.0</div>
                                            </div>
                                        </div>
                                    </div>

                                    <div class="murid__tutor-modal">
                                        <small class="penilai__feature">Murid</small>
                                        <div class="d-flex justify-content-center">
                                            <div class="row">
                                                
                                                <div style="font-size: 16px; font-weight: bold ">71</div>
                                            </div>
                                        </div>
                                    </div>

                                    <div class="kelas__tutor-modal">
                                        <small class="penilai__feature">Kelas</small>
                                        <div class="d-flex justify-content-center">
                                            <div class="row">
                                               
                                                <div style="font-size: 16px; font-weight: bold ">50</div>
                                            </div>
                                        </div>
                                    </div>
                                </div>
                            </div>
                        </div>
                    </div>
                </div>
            </div>
            <div class="modal-footer">
                <a href="#">Lupa Password?</a>
                
            </div>
        </div>
    </div>
</div>
<!--==========MODAL=======MODAL============MODAL===========MODAL================MODAL=======-->


@push('scripts')
    <script type="text/javascript">
        $('.file-upload').file_upload();
        $('#dateformat').datepicker({
            format: 'dd-mm-yyyy'
        });
    </script>
@endpush