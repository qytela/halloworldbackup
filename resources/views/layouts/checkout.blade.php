@extends('base.apps')
@push('styles')
<style>
</style>

@endpush
@section('img_header')
<div id="background_pages_profiletutor"  class="background_orange_profile">
    
</div>
@endsection


@section('content')
<div id="section_1" class="main__section_1_checkout_pages">
    <div class="container">
        <h4 class="text_payment__checkout">Payment Details</h4>
            <div class="card card__checkOut__">
                <div class="card-body">
                    <div class="kode__checkOut__right">
                        <div class="row justify-content-end">
                            <div class="col-md-2 pr-0">
                                <p>Nomor Tagihan</p>
                            </div>
                            <div class="col-xs-2">
                                <p>:</p>
                            </div>
                            <div class="col-sm-6 col-xs-5 col-md-3">
                                <p>INV/20190727/VII/3062</p>
                            </div>
                        </div>
                        <div class="row justify-content-end">
                            <div class="col-md-2 pr-0">
                                <p>Tanggal Transaksi</p>
                            </div>
                            <div class="col-xs-2">
                                <p>:</p>
                            </div>
                            <div class="col-sm-6 col-xs-5 col-md-3">
                                <p>27 Juli 2019</p>
                            </div>
                        </div>
                    </div>
                    <hr class="mb-0">
                    <div class="table-responsive">
                        <table class="table ">
                            <thead>
                                <tr>
                                    <th class="text-center">Tutor</th>
                                    <th class="text-center">Bahasa Yang Dipelajari</th>
                                    <th class="text-center">Biaya Pengajaran/Jam</th>
                                    <th class="text-center">Durasi/Jam</th>
                                    <th class="text-center">Total</th>
                                    <!-- <th>Action</th> -->
                                </tr>
                            </thead>
                            <tbody>
                                <tr class="text-center">
                                    <td>
                                        <img src="{{ asset('img/reza.jpeg') }}" class="rounded__image_checkout">
                                        <p class="mt-2">Jongsuk Kim</p>
                                    </td>
                                    <td class="pt-4 d-flex justify-content-center">
                                        <select class="form-control mb-3 custon__width__dropdownCheckout">
                                            <option value="1">English (Bristish)</option>
                                            <option value="2">English (American)</option>
                                            <option value="3">Chinese (Mandarin)</option>
                                        </select>
                                    </td>
                                    <td class="pt-4">Rp 100.000</td>
                                    <td class="pt-4">1/Jam</td>
                                    <td class="pt-4">Rp 100.000</td>
                                </tr>
                            </tbody>
                        </table>
                    </div>
                    <hr>
                    <div class="penjumlahan__total__checkout">
                        <div class="container">
                            <div class="row">
                                <div class="col-xs-12 col-md-7 col-lg-6">
                                    <div class="pencarian__kode__promo__">
                                        <p>Kode Promo</p>
                                        <div class="input__text__kode mode__float__left">
                                            <input type="number" class="form-control">
                                        </div>
                                        <div class="button__search__kode mode__float__right">
                                            <button type="submit" class="btn btn_gunakan__kode_promo">Gunakan</button>
                                        </div>
                                    </div>
                                </div>
                                <div class="col-xs-12 col-md-7 col-lg-6">
                                    <div class="list__total__checkout__item__ mr-4">
                                        <div class="row justify-content-end text-right">
                                            <div class="col-sm-8 col-md-5 col-xs-5">
                                                <p style=" font-weight: bold;">Subtotal</p>
                                            </div>
                                            <div class="col-md-3 pl-0 pr-0">
                                                <p>Rp 100.000</p>
                                            </div>
                                        </div>
                                        <div class="row justify-content-end text-right">
                                            <div class="col-sm-8 col-md-5 col-xs-5">
                                                <p style=" font-weight: bold;">Kode Unik</p>
                                            </div>
                                            <div class="col-md-3 pl-0 pr-0">
                                                <p>Rp 62</p>
                                            </div>
                                        </div>
                                        <div class="row justify-content-end text-right">
                                            <div class="col-sm-8 col-md-5 col-xs-5">
                                                <p style="font-weight: bold;">Diskon</p>
                                            </div>
                                            <div class="col-md-3 pl-0 pr-0">
                                                <p style="color: #EF5350;">Rp 100.000</p>
                                            </div>
                                        </div>
                                        <div class="row justify-content-end text-right">
                                            <div class="col-sm-8 col-md-5 col-xs-5">
                                                <p style="font-weight: bold;">Total Tagihan</p>
                                            </div>
                                            <div class="col-md-3 pl-0 pr-0">
                                                <p style="font-weight: bold; font-size: 20px;">Rp 100.000</p>
                                            </div>
                                        </div>
                                    </div>
                                </div>
                            </div>
                        </div>
                    </div>
                    <hr>
                    <div class="list_item__dipilih__">
                        <div class="container">
                            <div class="row">
                                <div class="col-xs-6 col-md-5 col-lg-4">
                                    <div class="list__jadwal_pengajaran__">
                                        <p style="font-weight:bold;">Jadwal Pengajaran</p>
                                        <div class="top__header__list_item_dipilih">
                                            Pilih Tanggal dan Waktu
                                            <div class="icon__top_header">
                                                <i class="fas fa-calendar-alt"></i>
                                            </div>
                                        </div>
                                        <div class="body__header_list__item__dipilih">
                                            <select class="form-control mb-3">
                                                <option value="1">30 Juli 2019, Pukul 19.00 WIB</option>
                                                <option value="2">30 Juli 2019, Pukul 19.00 WIB</option>
                                                <option value="3">30 Juli 2019, Pukul 19.00 WIB</option>
                                            </select>
                                            <!-- <p style="font-size: 14px;">30 Juli 2019, Pukul 19.00 WIB</p> -->
                                        </div>
                                    </div>
                                </div>

                                <div class="col-xs-6 col-md-5 col-lg-4">
                                    <div class="list__jadwal_pengajaran__">
                                        <p style="font-weight:bold;">Jadwal Pengajaran</p>
                                        <div class="top__header__list_item_dipilih">
                                            Pilih Tanggal dan Waktu
                                            <div class="icon__top_header">
                                                <i class="fas fa-caret-down"></i>
                                            </div>
                                        </div>
                                        <div class="body__header_list__item__dipilih">
                                            <select class="form-control mb-3">
                                                <option value="1">Bahasa Keperluan Karir (Career Builder)</option>
                                                <option value="2">Bahasa Percakapan Perjalanan (Traveller)</option>
                                                <option value="3">Bahasa Percakapan Umum (Language Lover)</option>
                                            </select>
                                            <!-- <p style="font-size: 14px;">Bahasa Keperluan Karir (Career Builder)</p>
                                            <p style="font-size: 14px;">Bahasa Percakapan Perjalanan (Traveller)</p>
                                            <p style="font-size: 14px;">Bahasa Percakapan Umum (Language Lover)</p> -->
                                        </div>
                                    </div>
                                </div>
                                
                                <div class="col-xs-6 col-md-5 col-lg-4">
                                    <div class="list__jadwal_pengajaran__" style="margin-top: 40px;">
                                        
                                        <div class="top__header__list_item_dipilih">
                                            Pilih Tanggal dan Waktu
                                            <div class="icon__top_header">
                                                <i class="fas fa-caret-down"></i>
                                            </div>
                                        </div>
                                        <div class="body__header_list__item__dipilih">
                                            <select class="form-control mb-3">
                                                <option value="1">Materi Intermediete</option>
                                                <option value="2">Materi Profiecient</option>
                                                <!-- <option value="3">Bahasa Percakapan Umum (Language Lover)</option> -->
                                            </select>
                                            <!-- <p style="font-size: 14px;">Materi Intermediete</p>
                                            <p style="font-size: 14px;">Materi Profiecient</p> -->
                                        </div>
                                    </div>
                                </div>
                            </div>
                        </div>
                    </div>
                    <hr>
                    <div class="pilih_metode__pembayaran_dicheckout">
                        <p>Pilih Cara Pembayaran</p>
                        <form>
                            <div class="form-group">
                                <label class="radio-inline mr-3 off__position">
                                    <input type="radio" class="off__position" name="optradio">Bank BNI <br> No Rek 182020202 <br>A/N Halloworld Indonesia</label>
                                <label class="radio-inline mr-3 off__position">
                                    <input type="radio" class="off__position" name="optradio"> Bank BCA <br> No Rek 12314141 <br> A/N Halloworld Indoensia</label>
                            </div>

                            <div class="form-group">
                                <label class="radio-inline mr-3 off__position">
                                    <input type="radio" class="off__position" name="optradio">Bank Mandiri <br> No Rek 182020202 <br>A/N Halloworld Indonesia</label>
                                <label class="radio-inline mr-3 off__position">
                                    <input type="radio" class="off__position" name="optradio"> Bank Syariah Mandiri <br> No Rek 12314141 <br> A/N Halloworld Indoensia</label>
                            </div>
                        </form>

                        <!-- <form class="mt-4">
                            <div class="form-group">
                                <label class="radio-inline mr-3">
                                    <input type="radio" name="optradio">Bank Mandiri <br> No Rek 182020202 <br>A/N Halloworld Indonesia</label>
                                <label class="radio-inline mr-3">
                                    <input type="radio" name="optradio"> Bank Syariah Mandiri <br> No Rek 12314141 <br> A/N Halloworld Indoensia</label>
                            </div>
                        </form> -->
                            <!-- <div class="col-xs-5 col-md-5 col-lg-4">
                                <div class="custom-control custom-radio">
                                    <input type="radio" class="custom-control-input" id="TEST" name="defaultExampleRadios">
                                    <div class="detail__pembayaran custom-control-label" for="TEST">
                                        <p class="font-weight-bold">Bank Mandiri</p>
                                        <div class="rekening__">
                                            <div class="row ml-0">
                                                <p style="margin-right: 6px;">No Rek.</p>
                                                <p class="font-weight-bold">1234.9878.123</p>
                                            </div>
                                        </div>
                                        <div class="kepada__">
                                            <div class="row ml-0">
                                                <p class="font-weight-bold" style="margin-right: 6px;">A/N</p>
                                                <p class="font-weight-bold">Halloworld Indonesia</p>
                                            </div>
                                        </div>
                                    </div>
                                </div>
                            </div>

                            <div class="col-xs-5 col-md-5 col-lg-4">
                                <div class="custom-control custom-radio">
                                    <input type="radio" class="custom-control-input" id="TEST_2" name="defaultExampleRadios">
                                    <div class="detail__pembayaran custom-control-label" for="TEST_2">
                                        <p class="font-weight-bold">Bank Syariah Mandiri</p>
                                        <div class="rekening__">
                                            <div class="row ml-0">
                                                <p style="margin-right: 6px;">No Rek.</p>
                                                <p class="font-weight-bold">1234.9878.123</p>
                                            </div>
                                        </div>
                                        <div class="kepada__">
                                            <div class="row ml-0">
                                                <p class="font-weight-bold" style="margin-right: 6px;">A/N</p>
                                                <p class="font-weight-bold">Halloworld Indonesia</p>
                                            </div>
                                        </div>
                                    </div>
                                </div>
                            </div> -->
                    </div>

                    <div class="click__event__checkout mt-5 mr-4">
                        <div class="row justify-content-end">
                            <button type="submit" class="btn btn__pesan__checkout">Pesan</button>
                            <button type="submit" class="btn btn__batal__checkout">Batal</button>
                        </div>
                    </div>
                </div>
            </div>
        </div>
    </div>
</div>

<!--=========MODAL===============MODAL===========MODAL==========MODAL============MODAL======-->
<div class="modal fade" id="perkenalantutor" tabindex="-1" role="dialog" ria-labelledby="exampleModalLabel" aria-hidden="true">
    <div class="modal-dialog modal-dialog_caritutor_perkenalan" role="document">
        <div class="modal-content modal__cariTutor-content">
            <div class="modal-header">
                <div class="d-flex justify-content-center video-container">
                <iframe 
                    class="video__yt"
                    width="600" height="370" 
                    src="https://www.youtube.com/embed/FcOctsNXyjk" frameborder="0" 
                    allow="accelerometer; autoplay; encrypted-media; 
                    gyroscope; picture-in-picture" allowfullscreen>
                </iframe>
                    
                </div>
                <button 
                    type="button" class="close btn_close-modal-caritutor" style="margin-left: -10px;" data-dismiss="modal" aria-label="Close">
                    <span aria-hidden="true">&times;</span>
                </button>
            </div>
            <div class="modal-body">
                <div class="my__information__modal">
                    <div class="row">
                        <div class="modal__profile-image__tutor">
                            <img src="{{ asset('img/tutor_kelasbahasa_3.jpg') }}" class="img__modal-tutor img-thumbnail" alt="img-thumbnail">
                            <div class="d-flex justify-content-center">
                                <div class="row">
                                    <small class="text-muted">BRITISH ENGLISH</small>
                                </div>
                            </div>
                            <div class="d-flex justify-content-center">
                                <div class="row">
                                    <small class="modal_tutor-item-waktu text-muted">05:49</small>
                                    <small class="text-muted" style="margin-left: 4px; margin-right: 4px;">UTC+</small>
                                    <small class="text-muted">06:00</small>
                                </div>
                            </div>
                        </div>
                        <div class="modal__profile-name">
                            <div class="row">
                                <h4 class="name__tutor-modal">BRITNEY</h4>
                                <div class="mr-auto" style="right: 15px; position: absolute;">
                                    <button type="submit" class="btn btn-modal__pesan btn_text-modal">
                                        <small class="text-button_modal">PESAN</small>
                                    </button>
                                </div>
                            </div>
                            <small class="text-muted">Online</small>
                            <div class="row">
                                <small style="margin-left: 15px; margin-top:15px; color: #01579B;" class="tutor___desc">Tutor Komunitas</small>
                                <small style="margin-left: 5px; margin-right: 5px; margin-top:15px; color: #01579B;" class="">From</small>
                                <small class="text-muted" style="margin-top:15px;">Cardiff,</small>
                                <small class="text-muted" style="margin-top:15px;">England</small>
                            </div>
                            <div class="row">
                                <small class="text-muted" style="margin-left: 15px; margin-top:15px;">Tutor Bahasa</small>
                                <small class="text-muted" style="margin-left: 5px; margin-top:15px;">Inggris (British)</small>
                            </div>
                            <div class="feature__tutor-penilaian">
                                <div class="row">
                                    <div class="penilaian__tutor-modal">
                                        <small class="penilai__feature">Penilaian</small>
                                        <div class="d-flex justify-content-center">
                                            <div class="row">
                                                <div id="rateYo" class="rating__jquery-modal mb-4"></div>
                                                <div style="font-size: 12px; font-weight: bold ">5.0</div>
                                            </div>
                                        </div>
                                    </div>

                                    <div class="murid__tutor-modal">
                                        <small class="penilai__feature">Murid</small>
                                        <div class="d-flex justify-content-center">
                                            <div class="row">
                                                
                                                <div style="font-size: 16px; font-weight: bold ">71</div>
                                            </div>
                                        </div>
                                    </div>

                                    <div class="kelas__tutor-modal">
                                        <small class="penilai__feature">Kelas</small>
                                        <div class="d-flex justify-content-center">
                                            <div class="row">
                                               
                                                <div style="font-size: 16px; font-weight: bold ">50</div>
                                            </div>
                                        </div>
                                    </div>
                                </div>
                            </div>
                        </div>
                    </div>
                </div>
            </div>
            <div class="modal-footer">
                <a href="#">Lupa Password?</a>
                
            </div>
        </div>
    </div>
</div>
<div class="modal fade" id="doc-cal" role="dialog">
  <div class="modal-dialog">

    <!-- Modal content-->
    <div class="modal-content">
      <div class="modal-header">
        <button type="button" class="close" data-dismiss="modal">&times;</button>
        <h4 class="modal-title">Doctor's Appointments</h4>
      </div>

      <div class="modal-body">
        <div id='calendar'></div>
      </div>
      <div class="modal-footer">
        <!-- <input type="submit" class="btn btn-warning" id="doc-update" value="Update"> -->
        <button type="button" class="btn btn-default" id="plist-close" data-dismiss="modal">Close</button>
      </div>
    </div>
  </div>
</div>

<!--==========MODAL=======MODAL============MODAL===========MODAL================MODAL=======-->

@endsection


@push('scripts')
    <script type="text/javascript">
        document.addEventListener('DOMContentLoaded', function() {
            var calendarEl = document.getElementById('calendar');

            var calendar = new FullCalendar.Calendar(calendarEl, {
            plugins: [ 'interaction', 'dayGrid', 'timeGrid', 'list' ],
            header: {
                left: 'prev,next today',
                center: 'title',
                right: 'dayGridMonth,timeGridWeek,timeGridDay,listMonth'
            },
            defaultDate: '2019-06-12',
            navLinks: true, // can click day/week names to navigate views
            businessHours: true, // display business hours
            editable: true,
            events: [
                {
                title: 'Business Lunch',
                start: '2019-06-03T13:00:00',
                constraint: 'businessHours'
                },
                {
                title: 'Meeting',
                start: '2019-06-13T11:00:00',
                constraint: 'availableForMeeting', // defined below
                color: '#257e4a'
                },
                {
                title: 'Conference',
                start: '2019-06-18',
                end: '2019-06-20'
                },
                {
                title: 'Party',
                start: '2019-06-29T20:00:00'
                },

                // areas where "Meeting" must be dropped
                {
                groupId: 'availableForMeeting',
                start: '2019-06-11T10:00:00',
                end: '2019-06-11T16:00:00',
                rendering: 'background'
                },
                {
                groupId: 'availableForMeeting',
                start: '2019-06-13T10:00:00',
                end: '2019-06-13T16:00:00',
                rendering: 'background'
                },

                // red areas where no events can be dropped
                {
                start: '2019-06-24',
                end: '2019-06-28',
                overlap: false,
                rendering: 'background',
                color: '#ff9f89'
                },
                {
                start: '2019-06-06',
                end: '2019-06-08',
                overlap: false,
                rendering: 'background',
                color: '#ff9f89'
                }
            ]
            });

            calendar.render();
        });
        
        $(function () {
 
            $("#rateYo").rateYo({
                rating: 2,
                fullStar: true,
                starWidth: "14px",
                readOnly: true
            });
            $("#rating__tutor").rateYo({
                rating: 5,
                fullStar: true,
                starWidth: "18px",
            });
            $("#rating__tutor_2").rateYo({
                rating: 4,
                fullStar: true,
                starWidth: "18px",
            });
            $("#rating__tutor_3").rateYo({
                rating: 3,
                fullStar: true,
                starWidth: "18px",
            });
        });
        // Getter
        var normalFill = $("#rateYo").rateYo("option", "fullStar"); //returns true
        
        // Setter
        $("#rateYo").rateYo("option", "fullStar", true); //returns a jQuery Element
    </script>
@endpush