<!DOCTYPE html>
<html lang="en">

<head>
    <meta charset="utf-8">
    <meta http-equiv="X-UA-Compatible" content="IE=edge">
    <meta name="viewport" content="width=device-width, initial-scale=1">
    <title>Dashboard Murid</title>
    <!-- Favicon icon -->
    <!-- <link rel="icon" type="image/png" sizes="16x16" href="../assets/images/favicon.png"> -->
    <!-- Custom Stylesheet -->
    <link href="css/style.css" rel="stylesheet">
    <script src="js/modernizr-3.6.0.min.js"></script>

    <!-- Font Awesome -->
    <link rel="stylesheet" href="https://use.fontawesome.com/releases/v5.7.0/css/all.css">
    <link rel="stylesheet" href="https://use.fontawesome.com/releases/v5.7.2/css/all.css" integrity="sha384-fnmOCqbTlWIlj8LyTjo7mOUStjsKC4pOpQbqyi7RrhN7udi9RwhKkMHpvLbHG9Sr" crossorigin="anonymous">
</head>

<body>

    <div id="preloader">
        <div class="loader">
            <div class="loader__bar"></div>
            <div class="loader__bar"></div>
            <div class="loader__bar"></div>
            <div class="loader__bar"></div>
            <div class="loader__bar"></div>
            <div class="loader__ball"></div>
        </div>
    </div>

    <div id="main-wrapper">

        <!-- header -->
        <div class="header">
            <div class="nav-header">
                <div class="brand-logo">
                    <a href="index.html">
                        <!-- <i class="cc BTC"></i> -->
                        <img src="{{ asset('img/korea_tutor.png') }}" class="cc BTC"  alt="">
                        <span class="brand-title">
                            <img src="{{ asset('img/Logo-Halloworld.png') }}" alt="">
                        </span>
                    </a>
                </div>

                <div class="nav-control">
                    <div class="hamburger">
                        <span class="line"></span>
                        <span class="line"></span>
                        <span class="line"></span>
                    </div>
                </div>
            </div>

            <div class="header-content">
                <div class="header-left">
                    <ul>
                        <li class="icons position-relative">
                            <a href="javascript:void(0)">
                                <i class="icon-magnifier f-s-16"></i>
                            </a>
                            <div class="drop-down animated bounceInDown">
                                <div class="dropdown-content-body">
                                    <div class="header-search" id="header-search">
                                        <form action="#">
                                            <div class="input-group">
                                                <input type="text" class="form-control" placeholder="Search">
                                                <div class="input-group-append">
                                                    <span class="input-group-text">
                                                        <i class="icon-magnifier"></i>
                                                    </span>
                                                </div>
                                            </div>
                                        </form>
                                    </div>
                                </div>
                            </div>
                        </li>
                    </ul>

                </div>
                <div class="header-right">
                    <ul>
                        <li class="icons">
                            <a href="javascript:void(0)">
                                <i class="icon-bell f-s-18" aria-hidden="true"></i>
                                <div class="pulse-css"></div>
                            </a>
                            <div class="drop-down animated bounceInDown">
                                <div class="dropdown-content-heading">
                                    <span class="text-left">Recent Notifications</span>
                                </div>
                                <div class="dropdown-content-body">
                                    <ul>
                                        <li>
                                            <a href="#">
                                                <img class="pull-left m-r-10 avatar-img" src="../assets/images/avatar/1.jpg" alt="" />
                                                <div class="notification-content">
                                                    <small class="notification-timestamp pull-right">02:34 PM</small>
                                                    <div class="notification-heading">Mr. Saifun</div>
                                                    <div class="notification-text">5 members joined today </div>
                                                </div>
                                            </a>
                                        </li>
                                        <li>
                                            <a href="#">
                                                <img class="pull-left m-r-10 avatar-img" src="../assets/images/avatar/2.jpg" alt="" />
                                                <div class="notification-content">
                                                    <small class="notification-timestamp pull-right">02:34 PM</small>
                                                    <div class="notification-heading">Mariam</div>
                                                    <div class="notification-text">likes a photo of you</div>
                                                </div>
                                            </a>
                                        </li>
                                        <li>
                                            <a href="#">
                                                <img class="pull-left m-r-10 avatar-img" src="../assets/images/avatar/3.jpg" alt="" />
                                                <div class="notification-content">
                                                    <small class="notification-timestamp pull-right">02:34 PM</small>
                                                    <div class="notification-heading">Tasnim</div>
                                                    <div class="notification-text">Hi Teddy, Just wanted to let you ...</div>
                                                </div>
                                            </a>
                                        </li>
                                        <li>
                                            <a href="#">
                                                <img class="pull-left m-r-10 avatar-img" src="../assets/images/avatar/4.jpg" alt="" />
                                                <div class="notification-content">
                                                    <small class="notification-timestamp pull-right">02:34 PM</small>
                                                    <div class="notification-heading">Ishrat Jahan</div>
                                                    <div class="notification-text">Hi Teddy, Just wanted to let you ...</div>
                                                </div>
                                            </a>
                                        </li>
                                        <li class="text-center">
                                            <a href="#" class="more-link">See All</a>
                                        </li>
                                    </ul>
                                </div>
                            </div>
                        </li>
                        <li class="icons">
                            <a href="javascript:void(0)">
                                <i class="icon-envelope f-s-18" aria-hidden="true"></i>
                                <div class="pulse-css"></div>
                            </a>
                            <div class="drop-down animated bounceInDown">
                                <div class="dropdown-content-heading">
                                    <span class="text-left">2 New Messages</span>
                                </div>
                                <div class="dropdown-content-body">
                                    <ul>
                                        <li class="notification-unread">
                                            <a href="#">
                                                <img class="pull-left m-r-10 avatar-img" src="../assets/images/avatar/1.jpg" alt="" />
                                                <div class="notification-content">
                                                    <small class="notification-timestamp pull-right">02:34 PM</small>
                                                    <div class="notification-heading">Saiul Islam</div>
                                                    <div class="notification-text">Hi Teddy, Just wanted to let you ...</div>
                                                </div>
                                            </a>
                                        </li>
                                        <li class="notification-unread">
                                            <a href="#">
                                                <img class="pull-left m-r-10 avatar-img" src="../assets/images/avatar/2.jpg" alt="" />
                                                <div class="notification-content">
                                                    <small class="notification-timestamp pull-right">02:34 PM</small>
                                                    <div class="notification-heading">Ishrat Jahan</div>
                                                    <div class="notification-text">Hi Teddy, Just wanted to let you ...</div>
                                                </div>
                                            </a>
                                        </li>
                                        <li>
                                            <a href="#">
                                                <img class="pull-left m-r-10 avatar-img" src="../assets/images/avatar/3.jpg" alt="" />
                                                <div class="notification-content">
                                                    <small class="notification-timestamp pull-right">02:34 PM</small>
                                                    <div class="notification-heading">Saiul Islam</div>
                                                    <div class="notification-text">Hi Teddy, Just wanted to let you ...</div>
                                                </div>
                                            </a>
                                        </li>
                                        <li>
                                            <a href="#">
                                                <img class="pull-left m-r-10 avatar-img" src="../assets/images/avatar/4.jpg" alt="" />
                                                <div class="notification-content">
                                                    <small class="notification-timestamp pull-right">02:34 PM</small>
                                                    <div class="notification-heading">Ishrat Jahan</div>
                                                    <div class="notification-text">Hi Teddy, Just wanted to let you ...</div>
                                                </div>
                                            </a>
                                        </li>
                                        <li class="text-center">
                                            <a href="#" class="more-link">See All</a>
                                        </li>
                                    </ul>
                                </div>
                            </div>
                        </li>
                        <li class="icons">
                            <a href="javascript:void(0)">
                                <i class="icon-note f-s-18" aria-hidden="true"></i>
                                <div class="pulse-css"></div>
                            </a>
                            <div class="drop-down dropdown-task animated bounceInDown">
                                <div class="dropdown-content-heading">
                                    <span class="text-left">Task Update</span>
                                </div>
                                <div class="dropdown-content-body">
                                    <ul>
                                        <li>
                                            <a href="#">
                                                <div class="notification-content">
                                                    <small class="notification-timestamp pull-right">85% Complete</small>
                                                    <div class="notification-heading">Task One</div>
                                                    <div class="progress">
                                                        <div style="width: 85%;" aria-valuemax="100" aria-valuemin="0" aria-valuenow="85" role="progressbar" class="progress-bar progress-bar-success"></div>
                                                    </div>
                                                </div>
                                            </a>
                                        </li>
                                        <li>
                                            <a href="#">
                                                <div class="notification-content">
                                                    <small class="notification-timestamp pull-right">60% Complete</small>
                                                    <div class="notification-heading">Task Two</div>
                                                    <div class="progress">
                                                        <div style="width: 60%;" aria-valuemax="100" aria-valuemin="0" aria-valuenow="60" role="progressbar" class="progress-bar progress-bar-primary"></div>
                                                    </div>
                                                </div>
                                            </a>
                                        </li>
                                        <li>
                                            <a href="#">
                                                <div class="notification-content">
                                                    <small class="notification-timestamp pull-right">25% Complete</small>
                                                    <div class="notification-heading">Task Three</div>
                                                    <div class="progress">
                                                        <div style="width: 25%;" aria-valuemax="100" aria-valuemin="0" aria-valuenow="25" role="progressbar" class="progress-bar progress-bar-warning"></div>
                                                    </div>
                                                </div>
                                            </a>
                                        </li>
                                        <li>
                                            <a href="#">
                                                <div class="notification-content">
                                                    <small class="notification-timestamp pull-right">75% Complete</small>
                                                    <div class="notification-heading">Task Four</div>
                                                    <div class="progress">
                                                        <div style="width: 75%;" aria-valuemax="100" aria-valuemin="0" aria-valuenow="75" role="progressbar" class="progress-bar progress-bar-danger"></div>
                                                    </div>
                                                </div>
                                            </a>
                                        </li>
                                        <li class="text-center">
                                            <a href="#" class="more-link">See All</a>
                                        </li>
                                    </ul>
                                </div>
                            </div>
                        </li>
                        <li class="icons">
                            <a href="javascript:void(0)">
                                <i class="icon-user f-s-18" aria-hidden="true"></i>
                            </a>
                            <div class="drop-down dropdown-profile animated bounceInDown">
                                <div class="dropdown-content-body">
                                    <ul>
                                        <li>
                                            <a href="#">
                                                <i class="icon-envelope"></i>
                                                <span>Inbox</span>
                                            </a>
                                        </li>
                                        <li>
                                            <a href="#">
                                                <i class="icon-settings"></i>
                                                <span>Setting</span>
                                            </a>
                                        </li>
                                        <li>
                                            <a href="#">
                                                <i class="icon-lock"></i>
                                                <span>Lock Screen</span>
                                            </a>
                                        </li>
                                        <li>
                                            <a href="home">
                                                <i class="icon-power"></i>
                                                <span>Logout</span>
                                            </a>
                                        </li>
                                    </ul>
                                </div>
                            </div>
                        </li>
                    </ul>
                </div>
            </div>
        </div>
        <!-- #/ header -->


        <!-- sidebar -->
        <div class="nk-sidebar">
            <div class="nk-nav-scroll">

                <div class="nav-user">
                    <img src="{{ asset('img/reza.jpeg') }}" alt="" class="rounded-circle">
                    <h5 style="color: #F6921E;">Mega Kurnia</h5>
                    <div class="row mt-5">
                        <div class="col-xl-3 pr-0">
                            <i class="fas fa-envelope"></i>
                        </div>
                        <div class="col-xl-9 pl-0">
                            <p class="text-left center__text_">mega.kurnia@gmail.com</p>
                        </div>
                    </div>
                    <div class="row mt-2">
                        <div class="col-xl-3 pr-0">
                            <i class="fas fa-mobile"></i>
                        </div>
                        <div class="col-xl-9 pl-0">
                            <p class="text-left center__text_">083878331195</p>
                        </div>
                    </div>
                    <div class="row mt-2">
                        <div class="col-xl-3 pr-0">
                            <i class="fas fa-venus-mars"></i>
                        </div>
                        <div class="col-xl-9 pl-0">
                            <p class="text-left center__text_">Perempuan</p>
                        </div>
                    </div>
                    <div class="row mt-2">
                        <div class="col-xl-3 pr-0">
                            <i class="fas fa-calendar-alt"></i>
                        </div>
                        <div class="col-xl-9 pl-0">
                            <p class="text-left center__text_">Jakarta, 12 Oktober 2000</p>
                        </div>
                    </div>
                    <div class="row mt-2">
                        <div class="col-xl-6">
                            <a href="" class="btn btn_edit_murid__">Edit</a>
                        </div>
                        <div class="col-xl-6">
                            <a href="" class="btn btn__perbarui__">Perbarui</a>
                        </div>
                    </div>
                </div>

                <ul class="metismenu" id="menu">
                    <li class="keluar_dashboard">
                        <a href="home" class="d-flex justify-content-center mt-2">
                            <i class="icon-logout "></i>
                            <!-- <i class="fas fa-sign-out-alt"></i> -->
                            <span class="nav-text">Keluar</span>
                        </a>
                    </li>
                </ul>
            </div>
            <!-- #/ nk nav scroll -->
        </div>
        <!-- #/ sidebar -->

        <!-- content body -->
        <div class="content-body">
            <div class="container-fluid">
                <div class="row page-titles">
                    <div class="col-md-3 col-sm-4 col-lg-3 col-xl-2 p-r-0 align-self-center">
                        <h3 class="text-primary">Dashboard</h3>
                        <ol class="breadcrumb">
                            <li class="breadcrumb-item">
                                <a href="javascript:void(0)">Home</a>
                            </li>
                            <li class="breadcrumb-item active">Murid</li>
                        </ol>
                    </div>
                </div>
                <div class="row">
                    <div class="col-xl-2">
                        <div class="card item_background__color__murid">
                            <div class="card-body">
                                <h1 class="text-center text-white font-weight-bold" style="font-size: 24px;">30 Juli</h1>
                            </div>
                        </div>
                    </div>
                    <div class="col-xl-10">
                        <div class="card">
                            <div class="card-body">
                                <h4 class="card-title font-weight-bold">Kelas Dengan Jongsuk Kim Pukul 19.00 WIB</h4>
                                <p><small>Kelas dimulai dalam Besok Pukul 19.00</small></p>
                            </div>
                        </div>
                    </div>
                </div>

                <div class="row">
                    <div class="col-xl-2">
                        <div class="card item_background__color__murid">
                            <div class="card-body">
                                <h1 class="text-center text-white font-weight-bold" style="font-size: 24px;">29 Juli</h1>
                            </div>
                        </div>
                    </div>
                    <div class="col-xl-10">
                        <div class="card">
                            <div class="card-body">
                                <h4 class="card-title font-weight-bold">Kelas Dengan Jongsuk Kim Pukul 20.30 WIB</h4>
                                <p><small>Kelas dimulai dalam 1 Jam 30 Menit</small></p>
                            </div>
                        </div>
                    </div>
                </div>

                <div class="row">
                    <div class="col-lg-12">
                        <div class="card">
                            <div class="card-body">
                                <h4 class="card-title dashboard_admin">Pesanan Kamu</h4>
                                <div class="table-responsive">
                                    <table id="myTable" class="table table-xs">
                                        <thead>
                                            <tr>
                                                <th class="text-center dashboard_admin">No Tagihan</th>
                                                <th class="text-center dashboard_admin">Tanggal Transaksi</th>
                                                <th class="text-center dashboard_admin">Total Tagihan</th>
                                                <th class="text-center dashboard_admin">Nama Tutor</th>
                                                <th class="text-center dashboard_admin">Status</th>
                                                <th class="text-center dashboard_admin">Action</th>
                                                <!-- <th class="text-right">Change % (7D)</th>
                                                <th class="text-right">Chart</th> -->
                                            </tr>
                                        </thead>
                                        <tbody>
                                            <tr>
                                                <td>
                                                    <!-- <a href="#" class="color-warning"> Bitcoin</a> -->
                                                    <p class="text-muted text-center">INV/20190727/VII/3062</p>
                                                </td>
                                                <td class="text-center">
                                                    <p>
                                                        <!-- <span>$</span> 25,458.40 -->
                                                        27 Juli 2019
                                                    </p>
                                                </td>
                                                <td class="text-center">
                                                    <p>
                                                        <!-- <span>$</span> 232,078,267,295 -->
                                                        Rp 100.620
                                                    </p>
                                                </td>
                                                <td class="text-center">
                                                    <p>
                                                        <!-- <span>$</span> 23,568,900,000</p> -->
                                                        Jongsuk Kim
                                                    </p>
                                                </td>
                                                <td class="text-center">
                                                    <p style="width: 200px;">
                                                        <!-- <span>$</span> 23,568,900,000</p> -->
                                                        Proses Pemesanan Tutor Telah Dilakukan. Menunggu Konfirmasi Ketersediaan Tutor.
                                                    </p>
                                                </td>
                                                <td class="text-center">
                                                    <div class="text-center">
                                                        <a href="" type="submit" class="btn btn_batalkan__" data-toggle="modal" data-target="#exampleModalCenter">Batal</a>
                                                    </div>
                                                </td>
                                                
                                            </tr>
                                            <tr>
                                                <td>
                                                    <!-- <a href="#" class="color-warning"> Bitcoin</a> -->
                                                    <p class="text-muted text-center">INV/20190727/VII/3062</p>
                                                </td>
                                                <td class="text-center">
                                                    <p>
                                                        <!-- <span>$</span> 25,458.40 -->
                                                        27 Juli 2019
                                                    </p>
                                                </td>
                                                <td class="text-center">
                                                    <p>
                                                        <!-- <span>$</span> 232,078,267,295 -->
                                                        Rp 100.620
                                                    </p>
                                                </td>
                                                <td class="text-center">
                                                    <p>
                                                        <!-- <span>$</span> 23,568,900,000</p> -->
                                                        Jongsuk Kim
                                                    </p>
                                                </td>
                                                <td class="text-center">
                                                    <p style="width: 200px;">
                                                        <!-- <span>$</span> 23,568,900,000</p> -->
                                                        Proses Pemesanan Tutor Telah Dilakukan. Menunggu Konfirmasi Ketersediaan Tutor.
                                                    </p>
                                                </td>
                                                <td class="text-center">
                                                    <div class="text-center">
                                                        <a href="" type="submit" class="btn btn_konfirmasi__">Konfirmasi</a>
                                                    </div>
                                                </td>
                                                
                                            </tr>
                                            <tr>
                                                <td>
                                                    <!-- <a href="#" class="color-warning"> Bitcoin</a> -->
                                                    <p class="text-muted text-center">INV/20190727/VII/3062</p>
                                                </td>
                                                <td class="text-center">
                                                    <p>
                                                        <!-- <span>$</span> 25,458.40 -->
                                                        27 Juli 2019
                                                    </p>
                                                </td>
                                                <td class="text-center">
                                                    <p>
                                                        <!-- <span>$</span> 232,078,267,295 -->
                                                        Rp 100.620
                                                    </p>
                                                </td>
                                                <td class="text-center">
                                                    <p>
                                                        <!-- <span>$</span> 23,568,900,000</p> -->
                                                        Jongsuk Kim
                                                    </p>
                                                </td>
                                                <td class="text-center">
                                                    <p style="width: 200px;">
                                                        <!-- <span>$</span> 23,568,900,000</p> -->
                                                        Proses Pemesanan Tutor Telah Dilakukan. Menunggu Konfirmasi Ketersediaan Tutor.
                                                    </p>
                                                </td>
                                                <td class="text-center">
                                                    <div class="text-center">
                                                        <!-- <a href="" type="submit" class="btn btn_batalkan__">Batalkan</a> -->
                                                        <p style="color: #F9A825; font-weight: bold; font-size: 20px;">Pending</p>
                                                    </div>
                                                </td>
                                                
                                            </tr>
                                            <tr>
                                                <td>
                                                    <!-- <a href="#" class="color-warning"> Bitcoin</a> -->
                                                    <p class="text-muted text-center">INV/20190727/VII/3062</p>
                                                </td>
                                                <td class="text-center">
                                                    <p>
                                                        <!-- <span>$</span> 25,458.40 -->
                                                        27 Juli 2019
                                                    </p>
                                                </td>
                                                <td class="text-center">
                                                    <p>
                                                        <!-- <span>$</span> 232,078,267,295 -->
                                                        Rp 100.620
                                                    </p>
                                                </td>
                                                <td class="text-center">
                                                    <p>
                                                        <!-- <span>$</span> 23,568,900,000</p> -->
                                                        Jongsuk Kim
                                                    </p>
                                                </td>
                                                <td class="text-center">
                                                    <p style="width: 200px;">
                                                        <!-- <span>$</span> 23,568,900,000</p> -->
                                                        Proses Pemesanan Tutor Telah Dilakukan. Menunggu Konfirmasi Ketersediaan Tutor.
                                                    </p>
                                                </td>
                                                <td class="text-center">
                                                    <div class="text-center">
                                                        <!-- <a href="" type="submit" class="btn btn_batalkan__">Batalkan</a> -->
                                                        <p style="color: #388E3C; font-weight: bold; font-size: 20px;">Berhasil</p>
                                                    </div>
                                                </td>
                                                
                                            </tr>
                                            <tr>
                                                <td>
                                                    <!-- <a href="#" class="color-warning"> Bitcoin</a> -->
                                                    <p class="text-muted text-center">INV/20190727/VII/3062</p>
                                                </td>
                                                <td class="text-center">
                                                    <p>
                                                        <!-- <span>$</span> 25,458.40 -->
                                                        27 Juli 2019
                                                    </p>
                                                </td>
                                                <td class="text-center">
                                                    <p>
                                                        <!-- <span>$</span> 232,078,267,295 -->
                                                        Rp 100.620
                                                    </p>
                                                </td>
                                                <td class="text-center">
                                                    <p>
                                                        <!-- <span>$</span> 23,568,900,000</p> -->
                                                        Jongsuk Kim
                                                    </p>
                                                </td>
                                                <td class="text-center">
                                                    <p style="width: 200px;">
                                                        <!-- <span>$</span> 23,568,900,000</p> -->
                                                        Proses Pemesanan Tutor Telah Dilakukan. Menunggu Konfirmasi Ketersediaan Tutor.
                                                    </p>
                                                </td>
                                                <td class="text-center">
                                                    <div class="text-center">
                                                        <a href="" type="submit" class="btn btn_masuk__">Masuk</a>
                                                    </div>
                                                    <div class="text-center">
                                                        <a href="" type="submit" class="btn btn_reschedule__">Reschedule</a>
                                                    </div>
                                                </td>
                                                
                                            </tr>
                                            <tr>
                                                <td>
                                                    <!-- <a href="#" class="color-warning"> Bitcoin</a> -->
                                                    <p class="text-muted text-center">INV/20190727/VII/3062</p>
                                                </td>
                                                <td class="text-center">
                                                    <p>
                                                        <!-- <span>$</span> 25,458.40 -->
                                                        27 Juli 2019
                                                    </p>
                                                </td>
                                                <td class="text-center">
                                                    <p>
                                                        <!-- <span>$</span> 232,078,267,295 -->
                                                        Rp 100.620
                                                    </p>
                                                </td>
                                                <td class="text-center">
                                                    <p>
                                                        <!-- <span>$</span> 23,568,900,000</p> -->
                                                        Jongsuk Kim
                                                    </p>
                                                </td>
                                                <td class="text-center">
                                                    <p style="width: 200px;">
                                                        <!-- <span>$</span> 23,568,900,000</p> -->
                                                        Proses Pemesanan Tutor Telah Dilakukan. Menunggu Konfirmasi Ketersediaan Tutor.
                                                    </p>
                                                </td>
                                                <td class="text-center">
                                                    <div class="text-center">
                                                        <a href="" type="submit" class="btn btn_ulasan__">Ulasan</a>
                                                    </div>
                                                </td>
                                                
                                            </tr>
                                            <tr>
                                                <td>
                                                    <!-- <a href="#" class="color-warning"> Bitcoin</a> -->
                                                    <p class="text-muted text-center">INV/20190727/VII/3062</p>
                                                </td>
                                                <td class="text-center">
                                                    <p>
                                                        <!-- <span>$</span> 25,458.40 -->
                                                        27 Juli 2019
                                                    </p>
                                                </td>
                                                <td class="text-center">
                                                    <p>
                                                        <!-- <span>$</span> 232,078,267,295 -->
                                                        Rp 100.620
                                                    </p>
                                                </td>
                                                <td class="text-center">
                                                    <p>
                                                        <!-- <span>$</span> 23,568,900,000</p> -->
                                                        Jongsuk Kim
                                                    </p>
                                                </td>
                                                <td class="text-center">
                                                    <p style="width: 200px;">
                                                        <!-- <span>$</span> 23,568,900,000</p> -->
                                                        Proses Pemesanan Tutor Telah Dilakukan. Menunggu Konfirmasi Ketersediaan Tutor.
                                                    </p>
                                                </td>
                                                <td class="text-center">
                                                    <div class="text-center">
                                                        <a href="" type="submit" class="btn btn_selesai__">Selesai</a>
                                                    </div>
                                                </td>
                                                
                                            </tr>
                                            <tr>
                                                <td>
                                                    <!-- <a href="#" class="color-warning"> Bitcoin</a> -->
                                                    <p class="text-muted text-center">INV/20190727/VII/3062</p>
                                                </td>
                                                <td class="text-center">
                                                    <p>
                                                        <!-- <span>$</span> 25,458.40 -->
                                                        27 Juli 2019
                                                    </p>
                                                </td>
                                                <td class="text-center">
                                                    <p>
                                                        <!-- <span>$</span> 232,078,267,295 -->
                                                        Rp 100.620
                                                    </p>
                                                </td>
                                                <td class="text-center">
                                                    <p>
                                                        <!-- <span>$</span> 23,568,900,000</p> -->
                                                        Jongsuk Kim
                                                    </p>
                                                </td>
                                                <td class="text-center">
                                                    <p style="width: 200px;">
                                                        <!-- <span>$</span> 23,568,900,000</p> -->
                                                        Proses Pemesanan Tutor Telah Dilakukan. Menunggu Konfirmasi Ketersediaan Tutor.
                                                    </p>
                                                </td>
                                                <td class="text-center">
                                                    <div class="text-center">
                                                        <a href="" type="submit" class="btn btn_batalkan__">Batalkan</a>
                                                    </div>
                                                </td>
                                                
                                            </tr>
                                            <tr>
                                                <td>
                                                    <!-- <a href="#" class="color-warning"> Bitcoin</a> -->
                                                    <p class="text-muted text-center">INV/20190727/VII/3062</p>
                                                </td>
                                                <td class="text-center">
                                                    <p>
                                                        <!-- <span>$</span> 25,458.40 -->
                                                        27 Juli 2019
                                                    </p>
                                                </td>
                                                <td class="text-center">
                                                    <p>
                                                        <!-- <span>$</span> 232,078,267,295 -->
                                                        Rp 100.620
                                                    </p>
                                                </td>
                                                <td class="text-center">
                                                    <p>
                                                        <!-- <span>$</span> 23,568,900,000</p> -->
                                                        Jongsuk Kim
                                                    </p>
                                                </td>
                                                <td class="text-center">
                                                    <p style="width: 200px;">
                                                        <!-- <span>$</span> 23,568,900,000</p> -->
                                                        Proses Pemesanan Tutor Telah Dilakukan. Menunggu Konfirmasi Ketersediaan Tutor.
                                                    </p>
                                                </td>
                                                <td class="text-center">
                                                    <div class="text-center">
                                                        <a href="" type="submit" class="btn btn_batalkan__">Batalkan</a>
                                                    </div>
                                                </td>
                                                
                                            </tr>
                                            <tr>
                                                <td>
                                                    <!-- <a href="#" class="color-warning"> Bitcoin</a> -->
                                                    <p class="text-muted text-center">INV/20190727/VII/3062</p>
                                                </td>
                                                <td class="text-center">
                                                    <p>
                                                        <!-- <span>$</span> 25,458.40 -->
                                                        27 Juli 2019
                                                    </p>
                                                </td>
                                                <td class="text-center">
                                                    <p>
                                                        <!-- <span>$</span> 232,078,267,295 -->
                                                        Rp 100.620
                                                    </p>
                                                </td>
                                                <td class="text-center">
                                                    <p>
                                                        <!-- <span>$</span> 23,568,900,000</p> -->
                                                        Jongsuk Kim
                                                    </p>
                                                </td>
                                                <td class="text-center">
                                                    <p style="width: 200px;">
                                                        <!-- <span>$</span> 23,568,900,000</p> -->
                                                        Proses Pemesanan Tutor Telah Dilakukan. Menunggu Konfirmasi Ketersediaan Tutor.
                                                    </p>
                                                </td>
                                                <td class="text-center">
                                                    <div class="text-center">
                                                        <a href="" type="submit" class="btn btn_batalkan__">Batalkan</a>
                                                    </div>
                                                </td>
                                                
                                            </tr>
                                            <tr>
                                                <td>
                                                    <!-- <a href="#" class="color-warning"> Bitcoin</a> -->
                                                    <p class="text-muted text-center">INV/20190727/VII/3062</p>
                                                </td>
                                                <td class="text-center">
                                                    <p>
                                                        <!-- <span>$</span> 25,458.40 -->
                                                        27 Juli 2019
                                                    </p>
                                                </td>
                                                <td class="text-center">
                                                    <p>
                                                        <!-- <span>$</span> 232,078,267,295 -->
                                                        Rp 100.620
                                                    </p>
                                                </td>
                                                <td class="text-center">
                                                    <p>
                                                        <!-- <span>$</span> 23,568,900,000</p> -->
                                                        Jongsuk Kim
                                                    </p>
                                                </td>
                                                <td class="text-center">
                                                    <p style="width: 200px;">
                                                        <!-- <span>$</span> 23,568,900,000</p> -->
                                                        Proses Pemesanan Tutor Telah Dilakukan. Menunggu Konfirmasi Ketersediaan Tutor.
                                                    </p>
                                                </td>
                                                <td class="text-center">
                                                    <div class="text-center">
                                                        <a href="" type="submit" class="btn btn_batalkan__">Batalkan</a>
                                                    </div>
                                                </td>
                                                
                                            </tr>
                                        </tbody>
                                    </table>
                                </div>
                            </div>
                        </div>
                    </div>
                </div>

                <!-- <div class="row">
                    <div class="col-lg-12 col-xl-4">
                        <div class="card">
                            <div class="card-body">
                                <h4 class="card-title">BTC</h4>
                                <div id="morris-area-chart1"></div>
                            </div>
                        </div>
                    </div>
                    <div class="col-lg-12 col-xl-4">
                        <div class="card">
                            <div class="card-body">
                                <h4 class="card-title">BTC vs LTC</h4>
                                <div id="morris-area-chart2"></div>
                            </div>
                        </div>
                    </div>
                    <div class="col-lg-12 col-xl-4">
                        <div class="card">
                            <div class="card-body">
                                <h4 class="card-title">BTC vs LTC vs NEO</h4>
                                <div id="morris-area-chart3"></div>
                            </div>
                        </div>
                    </div>
                </div>
                <div class="row">
                    <div class="col-lg-12 col-xl-5">
                        <div class="accordion" id="accordion-two">
                            <div class="card">
                                <div class="card-header">
                                    <h4 aria-controls="collapseOne1" aria-expanded="false" data-target="#collapseOne1" data-toggle="collapse" class="mb-0 collapsed">
                                        <i aria-hidden="true" class="fa"></i>
                                        Limit
                                    </h4>
                                </div>

                                <div data-parent="#accordion-two" class="collapse" id="collapseOne1" style="">
                                    <div class="card-body">
                                        <div class="row">
                                            <div class="col-12 col-xl-6">
                                                <div class="row m-b-20">
                                                    <div class="col-4">
                                                        <h5 class="text-bold-600 mb-0">Buy BTC</h5>
                                                    </div>
                                                    <div class="col-8 text-right">
                                                        <p class="text-muted mb-0">USD Balance: $ 5000.00</p>
                                                    </div>
                                                </div>
                                                <form class="form form-horizontal">
                                                    <div class="form-body">
                                                        <div class="form-group row">
                                                            <label class="col-md-4 col-form-label" for="btc-limit-buy-price">Price</label>
                                                            <div class="col-md-8">
                                                                <input type="number" id="btc-limit-buy-price" class="form-control" placeholder="$ 11916.9" name="btc-limit-buy-price">
                                                            </div>
                                                        </div>
                                                        <div class="form-group row">
                                                            <label class="col-md-4 col-form-label" for="btc-limit-buy-amount">Amount</label>
                                                            <div class="col-md-8">
                                                                <input type="number" id="btc-limit-buy-amount" class="form-control" placeholder="0.026547 BTC" name="btc-limit-buy-amount">
                                                            </div>
                                                        </div>
                                                        <div class="form-group row">
                                                            <label class="col-md-4 col-form-label" for="btc-limit-buy-total">Total</label>
                                                            <div class="col-md-8">
                                                                <input type="number" disabled id="btc-limit-buy-total" class="form-control" placeholder="$ 318.1856" name="btc-limit-buy-total">
                                                            </div>
                                                        </div>
                                                        <div class="form-actions pb-0">
                                                            <button type="submit" class="btn round btn-success btn-block btn-glow"> Buy BTC </button>
                                                        </div>
                                                    </div>
                                                </form>
                                            </div>
                                            <div class="col-12 col-xl-6">
                                                <div class="row m-b-20">
                                                    <div class="col-4">
                                                        <h5 class="text-bold-600 mb-0">Sell BTC</h5>
                                                    </div>
                                                    <div class="col-8 text-right">
                                                        <p class="text-muted mb-0">BTC Balance: 1.2654898</p>
                                                    </div>
                                                </div>
                                                <form class="form form-horizontal">
                                                    <div class="form-body">
                                                        <div class="form-group row">
                                                            <label class="col-md-4 col-form-label">Price</label>
                                                            <div class="col-md-8">
                                                                <input type="number" id="btc-limit-sell-price" class="form-control" placeholder="$ 11916.9" name="btc-limit-sell-price">
                                                            </div>
                                                        </div>
                                                        <div class="form-group row">
                                                            <label class="col-md-4 col-form-label" for="btc-limit-sell-amount">Amount</label>
                                                            <div class="col-md-8">
                                                                <input type="number" id="btc-limit-sell-amount" class="form-control" placeholder="0.026547 BTC" name="btc-limit-sell-amount">
                                                            </div>
                                                        </div>
                                                        <div class="form-group row">
                                                            <label class="col-md-4 col-form-label" for="btc-limit-sell-total">Total</label>
                                                            <div class="col-md-8">
                                                                <input type="number" disabled id="btc-limit-sell-total" class="form-control" placeholder="$ 318.1856" name="btc-limit-sell-total">
                                                            </div>
                                                        </div>
                                                        <div class="form-actions pb-0">
                                                            <button type="submit" class="btn round btn-danger btn-block btn-glow"> Sell BTC </button>
                                                        </div>
                                                    </div>
                                                </form>
                                            </div>
                                        </div>
                                    </div>
                                </div>
                            </div>
                            <div class="card">
                                <div class="card-header">
                                    <h4 aria-controls="collapseTwo2" aria-expanded="true" data-target="#collapseTwo2" data-toggle="collapse" class="mb-0">
                                        <i aria-hidden="true" class="fa"></i>
                                        Market
                                    </h4>
                                </div>
                                <div data-parent="#accordion-two" class="collapse show" id="collapseTwo2" style="">
                                    <div class="card-body">
                                        <div class="row">
                                            <div class="col-12 col-xl-6">
                                                <div class="row m-b-20">
                                                    <div class="col-4">
                                                        <h5 class="text-bold-600 mb-0">Buy BTC</h5>
                                                    </div>
                                                    <div class="col-8 text-right">
                                                        <p class="text-muted mb-0">USD Balance: $ 5000.00</p>
                                                    </div>
                                                </div>
                                                <form class="form form-horizontal">
                                                    <div class="form-body">
                                                        <div class="form-group row">
                                                            <label class="col-md-4 col-form-label" for="btc-market-buy-price">Price</label>
                                                            <div class="col-md-8">
                                                                <input type="number" disabled id="btc-market-buy-price" class="form-control" placeholder="Market prise $" name="btc-market-buy-price">
                                                            </div>
                                                        </div>
                                                        <div class="form-group row">
                                                            <label class="col-md-4 col-form-label" for="btc-market-buy-amount">Amount</label>
                                                            <div class="col-md-8">
                                                                <input type="number" id="btc-market-buy-amount" class="form-control" placeholder="0.026547 BTC" name="btc-market-buy-amount">
                                                            </div>
                                                        </div>
                                                        <div class="form-actions pb-0">
                                                            <button type="submit" class="btn round btn-success btn-block btn-glow"> Buy BTC </button>
                                                        </div>
                                                    </div>
                                                </form>
                                            </div>
                                            <div class="col-12 col-xl-6">
                                                <div class="row my-2 p-r-15 p-l-15">
                                                    <div class="col-4">
                                                        <h5 class="text-bold-600 mb-0">Sell BTC</h5>
                                                    </div>
                                                    <div class="col-8 text-right">
                                                        <p class="text-muted mb-0">BTC Balance: 1.2654898</p>
                                                    </div>
                                                </div>
                                                <form class="form form-horizontal">
                                                    <div class="form-body">
                                                        <div class="form-group row">
                                                            <label class="col-md-4 col-form-label">Price</label>
                                                            <div class="col-md-8">
                                                                <input type="number" disabled id="btc-market-sell-price" class="form-control" placeholder="Market prise $" name="btc-market-sell-price">
                                                            </div>
                                                        </div>
                                                        <div class="form-group row">
                                                            <label class="col-md-4 col-form-label" for="btc-market-sell-amount">Amount</label>
                                                            <div class="col-md-8">
                                                                <input type="number" id="btc-market-sell-amount" class="form-control" placeholder="0.026547 BTC" name="btc-market-sell-amount">
                                                            </div>
                                                        </div>
                                                        <div class="form-actions pb-0">
                                                            <button type="submit" class="btn round btn-danger btn-block btn-glow"> Sell BTC </button>
                                                        </div>
                                                    </div>
                                                </form>
                                            </div>
                                        </div>
                                    </div>
                                </div>
                            </div>
                            <div class="card">
                                <div class="card-header">
                                    <h4 aria-controls="collapseThree3" aria-expanded="false" data-target="#collapseThree3" data-toggle="collapse" class="mb-0 collapsed">
                                        <i aria-hidden="true" class="fa"></i>
                                        Stop Limit
                                    </h4>
                                </div>
                                <div data-parent="#accordion-two" class="collapse" id="collapseThree3">
                                    <div class="card-body">
                                        <div class="row">
                                            <div class="col-12 col-xl-6">
                                                <div class="row m-b-20">
                                                    <div class="col-4">
                                                        <h5 class="text-bold-600 mb-0">Buy BTC</h5>
                                                    </div>
                                                    <div class="col-8 text-right">
                                                        <p class="text-muted mb-0">USD Balance: $ 5000.00</p>
                                                    </div>
                                                </div>
                                                <form class="form form-horizontal">
                                                    <div class="form-body">
                                                        <div class="form-group row">
                                                            <label class="col-md-4 col-form-label" for="btc-stop-buy">Stop</label>
                                                            <div class="col-md-8">
                                                                <input type="number" id="btc-stop-buy" class="form-control" placeholder="$ 11916.9" name="btc-stop-buy">
                                                            </div>
                                                        </div>
                                                        <div class="form-group row">
                                                            <label class="col-md-4 col-form-label" for="btc-stop-buy-limit">Limit</label>
                                                            <div class="col-md-8">
                                                                <input type="number" id="btc-stop-buy-limit" class="form-control" placeholder="$ 12000.0" name="btc-stop-buy-limit">
                                                            </div>
                                                        </div>
                                                        <div class="form-group row">
                                                            <label class="col-md-4 col-form-label" for="btc-stop-buy-amount">Amount</label>
                                                            <div class="col-md-8">
                                                                <input type="number" id="btc-stop-buy-amount" class="form-control" placeholder="0.026547 BTC" name="btc-stop-buy-amount">
                                                            </div>
                                                        </div>
                                                        <div class="form-group row">
                                                            <label class="col-md-4 col-form-label" for="btc-stop-buy-total">Total</label>
                                                            <div class="col-md-8">
                                                                <input type="number" disabled id="btc-stop-buy-total" class="form-control" placeholder="$ 318.1856" name="btc-stop-buy-total">
                                                            </div>
                                                        </div>
                                                        <div class="form-actions pb-0">
                                                            <button type="submit" class="btn round btn-success btn-block btn-glow"> Buy BTC </button>
                                                        </div>
                                                    </div>
                                                </form>
                                            </div>
                                            <div class="col-12 col-xl-6">
                                                <div class="row my-2 p-r-15 p-l-15">
                                                    <div class="col-4">
                                                        <h5 class="text-bold-600 mb-0">Sell BTC</h5>
                                                    </div>
                                                    <div class="col-8 text-right">
                                                        <p class="text-muted mb-0">BTC Balance: 1.2654898</p>
                                                    </div>
                                                </div>
                                                <form class="form form-horizontal">
                                                    <div class="form-body">
                                                        <div class="form-group row">
                                                            <label class="col-md-4 col-form-label" for="btc-stop-sell">Stop</label>
                                                            <div class="col-md-8">
                                                                <input type="number" id="btc-stop-sell" class="form-control" placeholder="$ 11916.9" name="btc-stop-sell">
                                                            </div>
                                                        </div>
                                                        <div class="form-group row">
                                                            <label class="col-md-4 col-form-label" for="btc-stop-sell-limit">Limit</label>
                                                            <div class="col-md-8">
                                                                <input type="number" id="btc-stop-sell-limit" class="form-control" placeholder="$ 12000.0" name="btc-stop-sell-limit">
                                                            </div>
                                                        </div>
                                                        <div class="form-group row">
                                                            <label class="col-md-4 col-form-label" for="btc-stop-sell-amount">Amount</label>
                                                            <div class="col-md-8">
                                                                <input type="number" id="btc-stop-sell-amount" class="form-control" placeholder="0.026547 BTC" name="btc-stop-sell-amount">
                                                            </div>
                                                        </div>
                                                        <div class="form-group row">
                                                            <label class="col-md-4 col-form-label" for="btc-stop-sell-total">Total</label>
                                                            <div class="col-md-8">
                                                                <input type="number" disabled id="btc-stop-sell-total" class="form-control" placeholder="$ 318.1856" name="btc-stop-sell-total">
                                                            </div>
                                                        </div>
                                                        <div class="form-actions pb-0">
                                                            <button type="submit" class="btn round btn-danger btn-block btn-glow"> Sell BTC </button>
                                                        </div>
                                                    </div>
                                                </form>
                                            </div>
                                        </div>
                                    </div>
                                </div>
                            </div>
                        </div>
                    </div>
                    <div class="col-lg-12 col-xl-7">
                        <div class="card">
                            <div class="card-body">
                                <div class="card-title">
                                    <h4>Active Order</h4>
                                </div>
                                <div class="table-responsive">
                                    <table class="table table-de mb-0">
                                        <thead>
                                            <tr>
                                                <th>Date</th>
                                                <th>Type</th>
                                                <th>Amount BTC</th>
                                                <th>BTC Remaining</th>
                                                <th>Price</th>
                                                <th>USD</th>
                                                <th>Fee (%)</th>
                                                <th>Cancel</th>
                                            </tr>
                                        </thead>
                                        <tbody>
                                            <tr>
                                                <td>2018-01-31 06:51:51</td>
                                                <td class="success">Buy</td>
                                                <td>
                                                    <i class="cc BTC"></i> 0.58647</td>
                                                <td>
                                                    <i class="cc BTC"></i> 0.58647</td>
                                                <td>11900.12</td>
                                                <td>$ 6979.78</td>
                                                <td>0.2</td>
                                                <td>
                                                    <button class="btn btn-sm round btn-outline-danger"> Cancel</button>
                                                </td>
                                            </tr>
                                            <tr>
                                                <td>2018-01-31 06:50:50</td>
                                                <td class="danger">Sell</td>
                                                <td>
                                                    <i class="cc BTC"></i> 1.38647</td>
                                                <td>
                                                    <i class="cc BTC"></i> 0.38647</td>
                                                <td>11905.09</td>
                                                <td>$ 4600.97</td>
                                                <td>0.2</td>
                                                <td>
                                                    <button class="btn btn-sm round btn-outline-danger"> Cancel</button>
                                                </td>
                                            </tr>
                                            <tr>
                                                <td>2018-01-31 06:49:51</td>
                                                <td class="success">Buy</td>
                                                <td>
                                                    <i class="cc BTC"></i> 0.45879</td>
                                                <td>
                                                    <i class="cc BTC"></i> 0.45879</td>
                                                <td>11901.85</td>
                                                <td>$ 5460.44</td>
                                                <td>0.2</td>
                                                <td>
                                                    <button class="btn btn-sm round btn-outline-danger"> Cancel</button>
                                                </td>
                                            </tr>
                                            <tr>
                                                <td>2018-01-31 06:51:51</td>
                                                <td class="success">Buy</td>
                                                <td>
                                                    <i class="cc BTC"></i> 0.89877</td>
                                                <td>
                                                    <i class="cc BTC"></i> 0.89877</td>
                                                <td>11899.25</td>
                                                <td>$ 10694.6</td>
                                                <td>0.2</td>
                                                <td>
                                                    <button class="btn btn-sm round btn-outline-danger"> Cancel</button>
                                                </td>
                                            </tr>
                                            <tr>
                                                <td>2018-01-31 06:51:51</td>
                                                <td class="danger">Sell</td>
                                                <td>
                                                    <i class="cc BTC"></i> 0.45712</td>
                                                <td>
                                                    <i class="cc BTC"></i> 0.45712</td>
                                                <td>11908.58</td>
                                                <td>$ 5443.65</td>
                                                <td>0.2</td>
                                                <td>
                                                    <button class="btn btn-sm round btn-outline-danger"> Cancel</button>
                                                </td>
                                            </tr>
                                            <tr>
                                                <td>2018-01-31 06:51:51</td>
                                                <td class="success">Buy</td>
                                                <td>
                                                    <i class="cc BTC"></i> 0.58647</td>
                                                <td>
                                                    <i class="cc BTC"></i> 0.58647</td>
                                                <td>11900.12</td>
                                                <td>$ 6979.78</td>
                                                <td>0.2</td>
                                                <td>
                                                    <button class="btn btn-sm round btn-outline-danger"> Cancel</button>
                                                </td>
                                            </tr>
                                        </tbody>
                                    </table>
                                </div>
                            </div>
                        </div>
                    </div>
                </div> -->
            </div>
            <!-- #/ container -->
        </div>
        <!-- #/ content body -->

        <!-- MODAL BATAL -->
            <!-- Modal -->
            <div class="modal fade" id="exampleModalCenter">
                <div class="modal-dialog modal-dialog-centered" role="document">
                    <div class="modal-content">
                        <div class="modal-header diplasy__off">
                            <h2 class="modal-title text-center">Apa Kamu Yakin?</h5>
                            <button type="button" class="close btn__close__" data-dismiss="modal">
                                <span>&times;</span>
                            </button>
                        </div>
                        <div class="d-flex justify-content-center">
                            <p class="text-center" style="width: 340px;">Dengan membatalkan pesanan ini uang pendaftaran kamu tidak dapat dikembalikan</p>
                        </div>
                        <div class="modal-footer border_bottom__off">
                            <button type="button" class="btn btn_kembali__modal__murid" data-dismiss="modal">Kembali</button>
                            <button type="button" class="btn btn_oke__modal__murid">Oke</button>
                        </div>
                    </div>
                </div>
            </div>
        <!-- END MODAL BATAL -->

        <!-- footer -->
        <div class="footer">
            <div class="copyright d-flex justify-content-center">
                <p>Copyright &copy;
                    <a href="home">Halloworld</a> 2019</p>
            </div>
        </div>
        <!-- #/ footer -->

    </div>

    <!-- Common JS -->
    <script src="plugins/common/common.min.js"></script>
    <!-- Custom script -->
    <script src="js/custom.min.js"></script>

    <!-- Morris Chart -->
    <script src="plugins/morris/raphael-min.js"></script>
    <script src="plugins/morris/morris.js"></script> 
    <!-- Custom dashboard script -->
    <script src="js/dashboard-1.js"></script>
    <script src="js/dashboard-2.js"></script>

    <!-- dataTables -->
    <script src="plugins/datatables/datatables.min.js"></script>
    <script src="plugins/datatables/datatables-init.js"></script>

    <script src="plugins/sparkline/jquery.sparkline.min.js"></script>

    <script>

// $(function () {
//     "use strict";
//     function sparkline_charts() {
//         $('.sparkline-marketcap').sparkline('html');
//     }
//     if ($('.sparkline-marketcap').length) {
//         sparkline_charts();
//     }

// }); 
</script>

</body>

</html>