@extends('user::base.caritutorbase')
@push('styles')
<style>
</style>

@endpush
@section('img_header')
<div id="background_pages_tutor"  class="background_orange_caritutor">
    TEMUKAN TUTOR TERBAIKMU
</div>


@section('content')
<div id="section_1" class="main_content_parent_1">
    <div class="container">
        <div class="row">
            <div class="menu_item_pilihbahasa">
                <div class="btn-group">
                    <button type="button" class="btn btn-danger btn__pilih_bahasa dropdown-toggle" data-toggle="dropdown" aria-haspopup="true" aria-expanded="false">
                    <i class="fas fa-search"></i>
                        BAHASA YANG INGIN DIPELAJARI
                    </button>
                    <div class="dropdown-menu">
                        <a class="dropdown-item" href="#">English (Bristish)</a>
                        <a class="dropdown-item" href="#">English (American)</a>
                        <a class="dropdown-item" href="#">Chinese (Mandarin)</a>
                        <a class="dropdown-item" href="#">Japanese</a>
                        <a class="dropdown-item" href="#">South Korea</a>
                        <a class="dropdown-item" href="#">Arabic</a>
                        <a class="dropdown-item" href="#">French</a>
                        <a class="dropdown-item" href="#">German</a>
                        <a class="dropdown-item" href="#">Indonesia</a>
                        <a class="dropdown-item" href="#">ALL LANGUAGES</a>
                    </div>
                </div>
            </div>
            <div class="menu_item_ketersediaan">
                <button type="button" class="btn btn-ketersediaan btn_item_menu" data-toggle="modal" data-target="#ketersediaantutor">
                <i class="fas fa-calendar-alt"></i>
                    KETERSEDIAAN
                </button>
            </div>
            <div class="menu_item_hargaorjam">
                <button type="button" class="btn btn-hargaorjam btn_item_menu" data-toggle="modal" data-target="#harga_per__jam_modal">
                    HARGA / JAM
                </button>
            </div>
            <div class="inputan__cari_tutor">
                <input type="text" class="form-control input__cari-tutor __focus_cari-tutor" placeholder="Cari Nama Tutor" aria-label="Username" aria-describedby="basic-addon1">
            </div>
            <!-- <div class="menu_item_carikan_tutor">
                <button type="button" class="btn btn-carikan_tutor">
                    CARIKAN TUTOR
                </button>
            </div> -->
        </div>
    </div>
</div>
<div class="container">
    <div class="notifikasi__ mt-5">
        KAMI MENEMUKAN 3 TUTOR UNTUKMU
    </div>
</div>
<div id="section_2" class="main_content_parent_2 mt-5">
    <div class="container">
        <div class="row">
            <div class="item__menu__">
                <div class="input-group">
                    
                    <!-- <div class="input-group-append">
                        <button class="btn btn-outline-secondary" type="button" id="button-addon2">Button</button>
                    </div> -->
                </div>
                <div class="menu__btn___">
                    <!-- <button type="submit" class="btn btn__daftar-tutor">
                        DAFTAR MENJADI TUTOR
                    </button> -->
                </div>
            </div>
            <div class="list__tutor__parrent">
                <div class="row">
                    <div class="list_card_tutor__">
                        <div class="card">
                            <div class="card-body">
                                <div class="row">
                                    <img src="{{ asset('img/cari_tutor_1.png') }}" class="img_tutor img-thumbnail" alt="">
                                    <div class="informasi__tutor__">
                                        <div class="country_tutor__">
                                            <div class="row">
                                                <!-- <img src="{{ asset('img/icon_english.png') }}" class="icon__country" alt=""> -->
                                                <h2>BRITNEY</h2>
                                            </div>
                                            <div class="row list__size___ responsive___item">
                                                <div class="list_item_negara mt-2">
                                                    <a href="#" class="item__sub_negara">ENGLISH (AMERICAN)</a>
                                                </div>
                                                <div class="list_item_negara mt-2">
                                                    <a href="#" class="item__sub_negara">GERMAN</a>
                                                </div>
                                                <div class="list_item_negara mt-2">
                                                    <a href="#" class="item__sub_negara">GERMAN</a>
                                                </div>
                                                <div class="list_item_negara mt-2">
                                                    <a href="#" class="item__sub_negara">ENGLISH (AMERICAN)</a>
                                                </div>
                                                <!-- <p class="sub_country">- ENGLISH (BRITISH)</p>
                                                <p class="sub_country">- SPANYOL</p>
                                                <p class="sub_country">- GERMAN</p>
                                                <p class="sub_country">- GERMAN</p> -->
                                                <!-- <p class="jumlah__">2</p>
                                                <p class="text_kelas">Kelas</p> -->
                                            </div>
                                            <div class="row">
                                                <div class="harga__perjam mt-2">
                                                    <p class="judul___text_harga">HARGA/JAM</p>
                                                    <p class="value__text_harga">RP. 500.000</p>
                                                </div>
                                            </div>
                                            <div class="row">
                                                <div class="menu__list-pesan-rate mb-3">
                                                    <div class="row">
                                                        <a type="submit" href="profile" class="btn btn-profile">
                                                            <small class="text__btnprofile_tutor">PROFILE</small>
                                                        </a>
                                                        <div id="rating__tutor" class="rateYo" style="margin-top: 15px;"></div>
                                                    </div>
                                                </div>
                                            </div>
                                        </div>
                                        <div class="message__tutor___">
                                            <p class="message__active">
                                                Saya memiliki pengalaman mengajar 5 tahun.
                                                Pernah mengajar di tempat les X sebagai native
                                                speaker untuk keperluan percakapan sehari-hari.
                                            </p>
                                        </div>
                                        <div class="list_kemampuan">
                                            <div class="row">
                                                <div class="list_item_kemampuan">
                                                    <p class="item_kemampuan">AKADEMIK</p>
                                                </div>
                                                <div class="list_item_kemampuan">
                                                    <p class="item_kemampuan">PERCAKAPAN</p>
                                                </div>
                                            </div>
                                        </div>
                                        <div class="item_list__menu_ketersediaan">
                                            <div class="row">
                                                <a href="events" type="submit" class="btn btn-ketersediaan__tutor" id="openCalendar">
                                                    KETERSEDIAAN
                                                </a>
                                                <button type="submit" class="btn btn-video-perkenalan" data-toggle="modal" data-target="#perkenalantutor">
                                                    VIDEO PERKENALAN
                                                </button>
                                            </div>
                                        </div>
                                    </div>
                                    
                                </div>
                            </div>
                        </div>
                    </div>
                </div>
                <div class="row">
                    <div class="list_card_tutor__ mt-5">
                        <div class="card">
                            <div class="card-body">
                                <div class="row">
                                    <img src="{{ asset('img/cari_tutor_2.png') }}" class="img_tutor img-thumbnail" alt="">
                                    <div class="informasi__tutor__">
                                        <div class="country_tutor__">
                                            <div class="row">
                                                <!-- <img src="{{ asset('img/icon_korea.png') }}" class="icon__country" alt=""> -->
                                                <h2>JONGSUK KIM</h2>
                                            </div>
                                            <div class="row list__size___ responsive___item">
                                                <div class="list_item_negara mt-2">
                                                    <a href="#" class="item__sub_negara">KOREAN</a>
                                                </div>
                                                <div class="list_item_negara mt-2">
                                                    <a href="#" class="item__sub_negara">GERMAN</a>
                                                </div>
                                                <div class="list_item_negara mt-2">
                                                    <a href="#" class="item__sub_negara">JEPANG</a>
                                                </div>
                                                <div class="list_item_negara mt-2">
                                                    <a href="#" class="item__sub_negara">ENGLISH (AMERICAN)</a>
                                                </div>
                                                <!-- <p class="jumlah__">1</p>
                                                <p class="text_kelas">Kelas</p> -->
                                            </div>
                                            <div class="row">
                                                <div class="harga__perjam mt-2">
                                                    <p class="judul___text_harga">HARGA/JAM</p>
                                                    <p class="value__text_harga">RP. 500.000</p>
                                                </div>
                                                <!-- <div class="harga__perjam">
                                                    <p class="judul___text_harga">HARGA/JAM</p>
                                                    <p class="value__text_harga">RP. 500.000</p>
                                                </div> -->
                                            </div>
                                            <div class="row">
                                                <div class="menu__list-pesan-rate mb-3">
                                                    <div class="row">
                                                        <a type="submit" href="profile" class="btn btn-profile">
                                                            <small class="text__btnprofile_tutor">PROFILE</small>
                                                        </a>
                                                        <div id="rating__tutor" class="rateYo" style="margin-top: 15px;"></div>
                                                    </div>
                                                </div>
                                            </div>
                                        </div>
                                        <div class="message__tutor___">
                                            <p class="message__active">
                                                Saya sudah menginjakan kaki ke 15 negara dan
                                                memiliki pengalaman mengajar bahasa korea ke banyak
                                                anak-anak.
                                            </p>
                                        </div>
                                        <div class="list_kemampuan">
                                            <div class="row">
                                                <div class="list_item_kemampuan">
                                                    <p class="item_kemampuan">TRAVELER</p>
                                                </div>
                                                <div class="list_item_kemampuan">
                                                    <p class="item_kemampuan">PERCAKAPAN</p>
                                                </div>
                                            </div>
                                        </div>
                                        <div class="item_list__menu_ketersediaan">
                                            <div class="row">
                                                <button type="submit" class="btn btn-ketersediaan__tutor">
                                                    KETERSEDIAAN
                                                </button>
                                                <button type="submit" class="btn btn-video-perkenalan">
                                                    VIDEO PERKENALAN
                                                </button>
                                            </div>
                                        </div>
                                    </div>
                                </div>
                            </div>
                        </div>
                    </div>
                </div>
                <div class="row">
                    <div class="list_card_tutor__ mt-5">
                        <div class="card">
                            <div class="card-body">
                                <div class="row">
                                    <img src="{{ asset('img/cari_tutor_3.png') }}" class="img_tutor img-thumbnail" alt="">
                                    <div class="informasi__tutor__">
                                        <div class="country_tutor__">
                                            <div class="row">
                                                <!-- <img src="{{ asset('img/icon_english.png') }}" class="icon__country" alt=""> -->
                                                <h2>LAURA</h2>
                                            </div>
                                            <div class="row list__size___ responsive___item">
                                                <div class="list_item_negara mt-2">
                                                    <a href="#" class="item__sub_negara">ENGLISH (BRITISH)</a>
                                                </div>
                                                <div class="list_item_negara mt-2">
                                                    <a href="#" class="item__sub_negara">ENGLISH (AMERICAN)</a>
                                                </div>
                                            </div>
                                            <div class="row">
                                                <div class="harga__perjam mt-2">
                                                    <p class="judul___text_harga">HARGA/JAM</p>
                                                    <p class="value__text_harga">RP. 500.000</p>
                                                </div>
                                            </div>
                                            <div class="row">
                                                <div class="menu__list-pesan-rate mb-3">
                                                    <div class="row">
                                                        <a type="submit" href="profile" class="btn btn-profile">
                                                            <small class="text__btnprofile_tutor">PROFILE</small>
                                                        </a>
                                                        <div id="rating__tutor" class="rateYo" style="margin-top: 15px;"></div>
                                                    </div>
                                                </div>
                                            </div>
                                        </div>
                                        <div class="message__tutor___">
                                            <p class="message__active">
                                                Saya sudah menjadi guru sejak 10 tahun yang lalu.
                                                Sudah meganjar banyak teman-teman lokal dan membantu mereka untuk
                                                meningkatkan kemampuan berbicara bahasa inggris.
                                            </p>
                                        </div>
                                        <div class="list_kemampuan">
                                            <div class="row">
                                                <div class="list_item_kemampuan">
                                                    <p class="item_kemampuan">AKADEMIK</p>
                                                </div>
                                                <div class="list_item_kemampuan">
                                                    <p class="item_kemampuan">BISNIS</p>
                                                </div>
                                            </div>
                                        </div>
                                        <div class="item_list__menu_ketersediaan">
                                            <div class="row">
                                                <button type="submit" class="btn btn-ketersediaan__tutor">
                                                    KETERSEDIAAN
                                                </button>
                                                <button type="submit" class="btn btn-video-perkenalan">
                                                    VIDEO PERKENALAN
                                                </button>
                                            </div>
                                        </div>
                                    </div>
                                </div>
                            </div>
                        </div>
                    </div>
                </div>
            </div>
        </div>
    </div>
</div>

<!--=========MODAL===============MODAL===========MODAL==========MODAL============MODAL======-->
<div class="modal fade" id="perkenalantutor" tabindex="-1" role="dialog" ria-labelledby="exampleModalLabel" aria-hidden="true">
    <div class="modal-dialog modal-dialog_caritutor_perkenalan" role="document">
        <div class="modal-content modal__cariTutor-content">
            <div class="modal-header">
                <div class="d-flex justify-content-center video-container">
                <iframe 
                    class="video__yt"
                    width="600" height="370" 
                    src="https://www.youtube.com/embed/FcOctsNXyjk" frameborder="0" 
                    allow="accelerometer; autoplay; encrypted-media; 
                    gyroscope; picture-in-picture" allowfullscreen>
                </iframe>
                    
                </div>
                <button 
                    type="button" class="close btn_close-modal-caritutor" style="margin-left: -10px;" data-dismiss="modal" aria-label="Close">
                    <span aria-hidden="true">&times;</span>
                </button>
            </div>
            <div class="modal-body">
                <div class="my__information__modal">
                    <div class="row">
                        <div class="modal__profile-image__tutor">
                            <img src="{{ asset('img/cari_tutor_1.png') }}" class="img__modal-tutor img-thumbnail" alt="img-thumbnail">
                            <div class="d-flex justify-content-center">
                                <div class="row">
                                    <small class="text-muted">BRITISH ENGLISH</small>
                                </div>
                            </div>
                            <div class="d-flex justify-content-center">
                                <div class="row">
                                    <small class="modal_tutor-item-waktu text-muted">05:49</small>
                                    <small class="text-muted" style="margin-left: 4px; margin-right: 4px;">UTC+</small>
                                    <small class="text-muted">06:00</small>
                                </div>
                            </div>
                        </div>
                        <div class="modal__profile-name">
                            <div class="row">
                                <h4 class="name__tutor-modal">BRITNEY</h4>
                                <div class="mr-auto" style="right: 15px; position: absolute;">
                                    <button type="submit" class="btn btn-modal__pesan btn_text-modal">
                                        <small class="text-button_modal">PESAN</small>
                                    </button>
                                </div>
                            </div>
                            <small class="text-muted">Online</small>
                            <div class="row">
                                <small style="margin-left: 15px; margin-top:15px; color: #01579B;" class="tutor___desc">Tutor Komunitas</small>
                                <small style="margin-left: 5px; margin-right: 5px; margin-top:15px; color: #01579B;" class="">From</small>
                                <small class="text-muted" style="margin-top:15px;">Cardiff,</small>
                                <small class="text-muted" style="margin-top:15px;">England</small>
                            </div>
                            <div class="row">
                                <small class="text-muted" style="margin-left: 15px; margin-top:15px;">Tutor Bahasa</small>
                                <small class="text-muted" style="margin-left: 5px; margin-top:15px;">Inggris (British)</small>
                            </div>
                            <div class="feature__tutor-penilaian">
                                <div class="row">
                                    <div class="penilaian__tutor-modal">
                                        <small class="penilai__feature">Penilaian</small>
                                        <div class="d-flex justify-content-center">
                                            <div class="row">
                                                <div id="rateYo" class="rating__jquery-modal mb-4"></div>
                                                <div style="font-size: 12px; font-weight: bold ">5.0</div>
                                            </div>
                                        </div>
                                    </div>

                                    <div class="murid__tutor-modal">
                                        <small class="penilai__feature">Murid</small>
                                        <div class="d-flex justify-content-center">
                                            <div class="row">
                                                
                                                <div style="font-size: 16px; font-weight: bold ">71</div>
                                            </div>
                                        </div>
                                    </div>

                                    <div class="kelas__tutor-modal">
                                        <small class="penilai__feature">Kelas</small>
                                        <div class="d-flex justify-content-center">
                                            <div class="row">
                                               
                                                <div style="font-size: 16px; font-weight: bold ">50</div>
                                            </div>
                                        </div>
                                    </div>
                                </div>
                            </div>
                        </div>
                    </div>
                </div>
            </div>
            <div class="modal-footer">
                <a href="#">Lupa Password?</a>
                
            </div>
        </div>
    </div>
</div>
<div class="modal fade" id="doc-cal" role="dialog">
  <div class="modal-dialog">

    <!-- Modal content-->
    <div class="modal-content">
      <div class="modal-header">
        <button type="button" class="close" data-dismiss="modal">&times;</button>
        <h4 class="modal-title">Doctor's Appointments</h4>
      </div>

      <div class="modal-body">
        <div id='calendar'></div>
      </div>
      <div class="modal-footer">
        <!-- <input type="submit" class="btn btn-warning" id="doc-update" value="Update"> -->
        <button type="button" class="btn btn-default" id="plist-close" data-dismiss="modal">Close</button>
      </div>
    </div>
  </div>
</div>
<div class="modal fade" id="ketersediaantutor" tabindex="-1" role="dialog" ria-labelledby="exampleModalLabel" aria-hidden="true">
    <div class="modal-dialog modal-dialog_caritutor_perkenalan" role="document">
        <div class="modal-content modal__cariTutor-content">
            <div class="modal-header">
                <button 
                    type="button" class="close btn_close_modal_ketersediaan" style="margin-left: -10px;" data-dismiss="modal" aria-label="Close">
                    <span aria-hidden="true">&times;</span>
                </button>
            </div>
            <div class="modal-body">
                <form>
                    <div class="form-group">
                        <label for="exampleInputEmail1">Tanggal</label>
                        <input type="date" class="form-control" id="exampleInputEmail1" aria-describedby="emailHelp" placeholder="Enter email">
                    </div>
                    <div class="form-group">
                        <label for="exampleInputPassword1">Waktu</label>
                        <input type="time" class="form-control" id="exampleInputPassword1" placeholder="Password">
                    </div>
                    <button type="submit" class="btn btn_modal_ketersediaan">Cari</button>
                </form>
            </div>
            <div class="modal-footer">
                
            </div>
        </div>
    </div>
</div>
<div class="modal fade" id="harga_per__jam_modal" tabindex="-1" role="dialog" ria-labelledby="exampleModalLabel" aria-hidden="true">
    <div class="modal-dialog modal-dialog_caritutor_perkenalan" role="document">
        <div class="modal-content modal__cariTutor-content">
            <div class="modal-header">
                <button 
                    type="button" class="close btn_close_modal_ketersediaan" style="margin-left: -10px;" data-dismiss="modal" aria-label="Close">
                    <span aria-hidden="true">&times;</span>
                </button>
            </div>
            <div class="modal-body">
                <form>
                    <div class="form-group">
                        <label for="exampleInputEmail1">Dari Waktu</label>
                        <input type="time" class="form-control" id="exampleInputEmail1" aria-describedby="emailHelp" placeholder="Enter email">
                    </div>
                    <div class="form-group">
                        <label for="exampleInputPassword1">Sampai Waktu</label>
                        <input type="time" class="form-control" id="exampleInputPassword1" placeholder="Password">
                    </div>
                    <button type="submit" class="btn btn_modal_ketersediaan">Cari</button>
                </form>
            </div>
            <div class="modal-footer">
                
            </div>
        </div>
    </div>
</div>

<!--==========MODAL=======MODAL============MODAL===========MODAL================MODAL=======-->


@push('scripts')
    <script type="text/javascript">
            let demoRatings = [5, 4, 2],
                stars       = $('.rateYo');
                
            for (let i = 0; i < stars.length; i++) {
                // console.log(demoRatings[i]);
                $('.rateYo').rateYo({
                    fullStar: true,
                    rating: demoRatings[i],
                    starWidth: "18px",
                });
            }

        document.addEventListener('DOMContentLoaded', function() {
            var calendarEl = document.getElementById('calendar');

            var calendar = new FullCalendar.Calendar(calendarEl, {
            plugins: [ 'interaction', 'dayGrid', 'timeGrid', 'list' ],
            header: {
                left: 'prev,next today',
                center: 'title',
                right: 'dayGridMonth,timeGridWeek,timeGridDay,listMonth'
            },
            defaultDate: '2019-06-12',
            navLinks: true, // can click day/week names to navigate views
            businessHours: true, // display business hours
            editable: true,
            events: [
                {
                title: 'Business Lunch',
                start: '2019-06-03T13:00:00',
                constraint: 'businessHours'
                },
                {
                title: 'Meeting',
                start: '2019-06-13T11:00:00',
                constraint: 'availableForMeeting', // defined below
                color: '#257e4a'
                },
                {
                title: 'Conference',
                start: '2019-06-18',
                end: '2019-06-20'
                },
                {
                title: 'Party',
                start: '2019-06-29T20:00:00'
                },

                // areas where "Meeting" must be dropped
                {
                groupId: 'availableForMeeting',
                start: '2019-06-11T10:00:00',
                end: '2019-06-11T16:00:00',
                rendering: 'background'
                },
                {
                groupId: 'availableForMeeting',
                start: '2019-06-13T10:00:00',
                end: '2019-06-13T16:00:00',
                rendering: 'background'
                },

                // red areas where no events can be dropped
                {
                start: '2019-06-24',
                end: '2019-06-28',
                overlap: false,
                rendering: 'background',
                color: '#ff9f89'
                },
                {
                start: '2019-06-06',
                end: '2019-06-08',
                overlap: false,
                rendering: 'background',
                color: '#ff9f89'
                }
            ]
            });

            calendar.render();
        });
        
        $(function () {
 
            // $("#rateYo").rateYo({
            //     rating: 2,
            //     fullStar: true,
            //     starWidth: "14px",
            //     readOnly: true
            // });
            // let rateArr = [
            //     5, 3, 4
            // ]
            //     stars = $("#rating__tutor");
            // for(let i = 0; i < rateArr.length; i++) {
            //     // console.log(rateArr[i]);
            //     $("#rating__tutor").rateYo({
            //         rating: rateArr[i],
            //         fullStar: true,
            //         starWidth: "18px",
            //     });
            // }
           
            
            // $("#rating__tutor_2").rateYo({
            //     rating: 4,
            //     fullStar: true,
            //     starWidth: "18px",
            // });
            // $("#rating__tutor_3").rateYo({
            //     rating: 3,
            //     fullStar: true,
            //     starWidth: "18px",
            // });
            // Getter
            // var normalFill = $("#rateYo").rateYo("option", "fullStar"); //returns true
            
            // Setter
            // $("#rateYo").rateYo("option", "fullStar", true); //returns a jQuery Element
        });
        
    </script>
@endpush