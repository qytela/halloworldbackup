@extends('base.jaditutorbase')
@push('styles')
<style type="text/css">

    .slider {
        width: 100%;
        margin: 100px auto;
    }

    .slick-slide {
      margin: 0px 20px;
    }

    .slick-slide img {
      width: 100%;
    }

    .slick-prev:before,
    .slick-next:before {
      color: #F6921E;
      
    }
    .slick-prev {

    }
    

    .slick-slide {
      transition: all ease-in-out .3s;
      /* opacity: .2; */
      margin-bottom: 50px;
    }

    .item-slider {
        width: 30%;
        height: 50%;
        display: inline-block;
        /* display: none; */
        margin-right: 20px;
        /* margin-top: 48px; */
    }
    .img-slider {
        width: 100%;
        height: 100%;
    }
    .item-slider .card_slider_1 .card-body {
        padding: 0 !important;
    }
   

  </style>
@endpush
@section('img_header')
<div id="background_pages_iklanjaditutor"  class="background_iklanjaditutor">
    <div class="container">
        <h4>DAPATKAN PENGHASILAN<br/> MELALUI MENGAJAR</h4>
        <h6>Gabung menjadi Tutor dan dapatkan keuntungannya</h6>
    </div>
</div>


@section('content')
<div id="section_1" class="iklanjaditutor__pg mt-5">
    <div class="d-flex justify-content-center bottom__">
        <div class="card card__custom__iklanjaditutor">
            <div class="card-body">
                <div class="row mt-5">
                    <div class="col-md-2">
                        <div class="img__bagmoney">
                            <img src="{{ asset('img/money-bag.png') }}" class="img-thumbnail img__moneybag"  alt="">
                        </div>
                    </div>
                    <div class="col-md-4">
                        <div class="text__bagmoney">
                            <p class="title__bagmoney">Dapatkan Penghasilan Ekstra</p>
                            <p class="subtitle__bagmoney">Dapatkan penghasilan ekstra melalui Halloworld</p>
                        </div>
                    </div>
                    <div class="col-md-2">
                        <div class="img__student">
                            <img src="{{ asset('img/student.png') }}" class="img-thumbnail img__student"  alt="">
                        </div>
                    </div>
                    <div class="col-md-4">
                        <div class="text__student">
                            <p class="title__student">Temukan Murid</p>
                            <p class="subtitle__student">Hallowolrd memiliki banyak pengguna yang bisa menjadi muridmu.</p>
                        </div>
                    </div>
                </div>
                <div class="row mt-5">
                    <div class="col-md-2">
                        <div class="img__calendar">
                            <img src="{{ asset('img/calendar.png') }}" class="img-thumbnail img__calendar"  alt="">
                        </div>
                    </div>
                    <div class="col-md-4">
                        <div class="text__calendar">
                            <p class="title__calendar">Atur Jadwalmu</p>
                            <p class="subtitle__calendar">Kamu bisa menentukan sendiri kapan kamu akan mengajar.</p>
                        </div>
                    </div>
                    <div class="col-md-2">
                        <div class="img__house">
                            <img src="{{ asset('img/house.png') }}" class="img-thumbnail img__house"  alt="">
                        </div>
                    </div>
                    <div class="col-md-4">
                        <div class="text__house">
                            <p class="title__house">Bekerja Dirumah</p>
                            <p class="subtitle__house">Kamu bisa mengajar dari mana saja yang kamu butuhkan hanya komputer, headset dan internet.</p>
                        </div>
                    </div>
                </div>
                <div class="row mt-5">
                    <div class="col-md-2">
                        <div class="img__shield">
                            <img src="{{ asset('img/shield.png') }}" class="img-thumbnail img__shield"  alt="">
                        </div>
                    </div>
                    <div class="col-md-4">
                        <div class="text__shield">
                            <p class="title__shield">Aman Dan Terpercaya</p>
                            <p class="subtitle__shield">Kamu tidak perlu khawatir soal pembayaran. Halloworld akan membayar kamu tepat waktu.</p>
                        </div>
                    </div>
                    <div class="col-md-2">
                        <div class="img__badge">
                            <img src="{{ asset('img/badge.png') }}" class="img-thumbnail img__badge"  alt="">
                        </div>
                    </div>
                    <div class="col-md-4">
                        <div class="text__badge">
                            <p class="title__badge">Personal Branding</p>
                            <p class="subtitle__badge">Bangun sendiri personal brandmu melalui Halloworld.</p>
                        </div>
                    </div>
                </div>
                <div class="row mt-5 d-flex justify-content-center">
                    <a href="{{ URL::to('/user/jaditutor') }}" class="btn btn__daftarjaditutor_iklan">DAFTAR MENJADI TUTOR</a>
                </div>
            </div>
        </div>
    </div>
</div>
<div id="section_2" class="apa_kata__mereka___">
    <div class="d-flex justify-content-center mt-5">
        <p class="text__apakatamereka">APA KATA MEREKA?</p>
    </div>
    <div class="list___section_2 mt-5">
        <div class="container">
            <!-- <div class="row">
                <div class="col-md-4">
                    <div class="img__list__pertama d-flex justify-content-center">
                        <img src="{{ asset('img/tutor_kelasbahasa_1.jpg') }}" class="img-thumbnail img__list_section_2" alt="">
                    </div>
                    <div class="text__list__pertama d-flex justify-content-center">
                        <p>Jongsuk Kim</p>
                    </div>
                    <div class="subtext__list__pertama d-flex justify-content-center">
                        <p>'...Saya sangat senang mengajar di Halloworld. Karena bisa Bekerja
                            di mana saja dan kapan saja tanpa mengganggu pekerjaan utama saya.'
                        </p>
                    </div>
                </div>
                <div class="col-md-4">
                    <div class="img__list__pertama d-flex justify-content-center">
                        <img src="{{ asset('img/tutor_kelasbahasa_2.jpg') }}" class="img-thumbnail img__list_section_2" alt="">
                    </div>
                    <div class="text__list__pertama d-flex justify-content-center">
                        <p>Britney</p>
                    </div>
                    <div class="subtext__list__pertama d-flex justify-content-center">
                        <p>'.Berawal dari hobi dan sekarang menjadi penghasilan. Terimakasih Halloworld.'</p>
                    </div>
                </div>
                <div class="col-md-4">
                    <div class="img__list__pertama d-flex justify-content-center">
                        <img src="{{ asset('img/tutor_kelasbahasa_3.jpg') }}" class="img-thumbnail img__list_section_2" alt="">
                    </div>
                    <div class="text__list__pertama d-flex justify-content-center">
                        <p>Laura</p>
                    </div>
                    <div class="subtext__list__pertama d-flex justify-content-center">
                        <p>'..Halloworld membantu saya menemukan banyak murid.'
                        </p>
                    </div>
                </div>
            </div> -->
            <div id="mySlider" class="slider__testimoni_tutor">
                <div class="item-slider ml-4">
                    <div class="img__list__pertama d-flex justify-content-center">
                        <img src="{{ asset('img/cari_tutor_2.png') }}" class="img-thumbnail img__list_section_2" alt="">
                    </div>
                    <div class="text__list__pertama d-flex justify-content-center">
                        <p>Jongsuk Kim</p>
                    </div>
                    <div class="subtext__list__pertama d-flex justify-content-center">
                        <p>'...Saya sangat senang mengajar di Halloworld. Karena bisa Bekerja
                            di mana saja dan kapan saja tanpa mengganggu pekerjaan utama saya.'
                        </p>
                    </div>
                </div>
                <div class="item-slider">
                    <div class="img__list__pertama d-flex justify-content-center">
                        <img src="{{ asset('img/cari_tutor_1.png') }}" class="img-thumbnail img__list_section_2" alt="">
                    </div>
                    <div class="text__list__pertama d-flex justify-content-center">
                        <p>Britney</p>
                    </div>
                    <div class="subtext__list__pertama d-flex justify-content-center">
                        <p>
                            '..berawal dari hobi dan sekarang menjadi penghasilan. Terima Kasih Halloworld'
                        </p>
                    </div>
                </div>
                <div class="item-slider">
                    <div class="img__list__pertama d-flex justify-content-center">
                        <img src="{{ asset('img/cari_tutor_3.png') }}" class="img-thumbnail img__list_section_2" alt="">
                    </div>
                    <div class="text__list__pertama d-flex justify-content-center">
                        <p>Laura</p>
                    </div>
                    <div class="subtext__list__pertama d-flex justify-content-center">
                        <p>
                            '...Halloworld membantu saya menemukan banyak murid.'
                        </p>
                    </div>
                </div>
            </div>
            <div class="row mt-5 d-flex justify-content-center">
                <a href="{{ URL::to('/user/jaditutor') }}" class="btn btn__tertarik_mendaftar">TERTARIK MENDAFTAR</a>
            </div>
        </div>
    </div>
</div>

<!--=========MODAL===============MODAL===========MODAL==========MODAL============MODAL======-->
<div class="modal fade" id="perkenalantutor" tabindex="-1" role="dialog" ria-labelledby="exampleModalLabel" aria-hidden="true">
    <div class="modal-dialog modal-dialog_caritutor_perkenalan" role="document">
        <div class="modal-content modal__cariTutor-content">
            <div class="modal-header">
                <div class="d-flex justify-content-center video-container">
                <iframe 
                    class="video__yt"
                    width="600" height="370" 
                    src="https://www.youtube.com/embed/FcOctsNXyjk" frameborder="0" 
                    allow="accelerometer; autoplay; encrypted-media; 
                    gyroscope; picture-in-picture" allowfullscreen>
                </iframe>
                    
                </div>
                <button 
                    type="button" class="close btn_close-modal-caritutor" style="margin-left: -10px;" data-dismiss="modal" aria-label="Close">
                    <span aria-hidden="true">&times;</span>
                </button>
            </div>
            <div class="modal-body">
                <div class="my__information__modal">
                    <div class="row">
                        <div class="modal__profile-image__tutor">
                            <img src="{{ asset('img/tutor_kelasbahasa_3.jpg') }}" class="img__modal-tutor img-thumbnail" alt="img-thumbnail">
                            <div class="d-flex justify-content-center">
                                <div class="row">
                                    <small class="text-muted">BRITISH ENGLISH</small>
                                </div>
                            </div>
                            <div class="d-flex justify-content-center">
                                <div class="row">
                                    <small class="modal_tutor-item-waktu text-muted">05:49</small>
                                    <small class="text-muted" style="margin-left: 4px; margin-right: 4px;">UTC+</small>
                                    <small class="text-muted">06:00</small>
                                </div>
                            </div>
                        </div>
                        <div class="modal__profile-name">
                            <div class="row">
                                <h4 class="name__tutor-modal">BRITNEY</h4>
                                <div class="mr-auto" style="right: 15px; position: absolute;">
                                    <button type="submit" class="btn btn-modal__pesan btn_text-modal">
                                        <small class="text-button_modal">PESAN</small>
                                    </button>
                                </div>
                            </div>
                            <small class="text-muted">Online</small>
                            <div class="row">
                                <small style="margin-left: 15px; margin-top:15px; color: #01579B;" class="tutor___desc">Tutor Komunitas</small>
                                <small style="margin-left: 5px; margin-right: 5px; margin-top:15px; color: #01579B;" class="">From</small>
                                <small class="text-muted" style="margin-top:15px;">Cardiff,</small>
                                <small class="text-muted" style="margin-top:15px;">England</small>
                            </div>
                            <div class="row">
                                <small class="text-muted" style="margin-left: 15px; margin-top:15px;">Tutor Bahasa</small>
                                <small class="text-muted" style="margin-left: 5px; margin-top:15px;">Inggris (British)</small>
                            </div>
                            <div class="feature__tutor-penilaian">
                                <div class="row">
                                    <div class="penilaian__tutor-modal">
                                        <small class="penilai__feature">Penilaian</small>
                                        <div class="d-flex justify-content-center">
                                            <div class="row">
                                                <div id="rateYo" class="rating__jquery-modal mb-4"></div>
                                                <div style="font-size: 12px; font-weight: bold ">5.0</div>
                                            </div>
                                        </div>
                                    </div>

                                    <div class="murid__tutor-modal">
                                        <small class="penilai__feature">Murid</small>
                                        <div class="d-flex justify-content-center">
                                            <div class="row">
                                                
                                                <div style="font-size: 16px; font-weight: bold ">71</div>
                                            </div>
                                        </div>
                                    </div>

                                    <div class="kelas__tutor-modal">
                                        <small class="penilai__feature">Kelas</small>
                                        <div class="d-flex justify-content-center">
                                            <div class="row">
                                               
                                                <div style="font-size: 16px; font-weight: bold ">50</div>
                                            </div>
                                        </div>
                                    </div>
                                </div>
                            </div>
                        </div>
                    </div>
                </div>
            </div>
            <div class="modal-footer">
                <a href="#">Lupa Password?</a>
                
            </div>
        </div>
    </div>
</div>
<!--==========MODAL=======MODAL============MODAL===========MODAL================MODAL=======-->


@push('scripts')
    <script type="text/javascript">
       
       if(window.matchMedia("(max-width: 360px)").matches) {
        $('#mySlider').slick({
        dots: false,
        autoplay: true,
        infinite: false,
        speed: 500,
        mobileFirst: false,
        slidesToShow: 3,
        slidesToScroll: 3,
        responsive: [
            {
                breakpoint: 1024,
                settings: {
                    slidesToShow: 2,
                    slidesToScroll: 2,
                    infinite: false,
                    // dots: true
                }
            },
            {
                breakpoint: 375,
                settings: {
                    slidesToShow: 1,
                    slidesToScroll: 1
                }
            },
            {
                breakpoint: 600,
                settings: {
                    slidesToShow: 1,
                    slidesToScroll: 1
                }
            },
            {
                breakpoint: 480,
                settings: {
                    slidesToShow: 1,
                    slidesToScroll: 1
                }
            }
        ]
        });
        }else if(window.matchMedia("(max-width: 414px)").matches){
            $('#mySlider').slick({
            dots: false,
            autoplay: true,
            infinite: false,
            speed: 500,
            mobileFirst: false,
            slidesToShow: 3,
            slidesToScroll: 3,
            responsive: [
                {
                    breakpoint: 1024,
                    settings: {
                        slidesToShow: 2,
                        slidesToScroll: 2,
                        infinite: false,
                        // dots: true
                    }
                },
                {
                    breakpoint: 375,
                    settings: {
                        slidesToShow: 1,
                        slidesToScroll: 1
                    }
                },
                {
                    breakpoint: 600,
                    settings: {
                        slidesToShow: 1,
                        slidesToScroll: 1
                    }
                },
                {
                    breakpoint: 480,
                    settings: {
                        slidesToShow: 1,
                        slidesToScroll: 1
                    }
                }
            ]
            });
        }else if(window.matchMedia("(max-width: 768px)").matches){
            $('#mySlider').slick({
            dots: false,
            autoplay: true,
            infinite: false,
            speed: 500,
            mobileFirst: false,
            slidesToShow: 3,
            slidesToScroll: 3,
            responsive: [
                {
                    breakpoint: 1024,
                    settings: {
                        slidesToShow: 2,
                        slidesToScroll: 2,
                        infinite: false,
                        // dots: true
                    }
                },
                {
                    breakpoint: 375,
                    settings: {
                        slidesToShow: 1,
                        slidesToScroll: 1
                    }
                },
                {
                    breakpoint: 600,
                    settings: {
                        slidesToShow: 1,
                        slidesToScroll: 1
                    }
                },
                {
                    breakpoint: 480,
                    settings: {
                        slidesToShow: 1,
                        slidesToScroll: 1
                    }
                }
            ]
            });
        }
    </script>
@endpush