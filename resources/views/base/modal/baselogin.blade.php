<!--=========MODAL===============MODAL===========MODAL==========MODAL============MODAL======-->
<div class="modal fade" id="loginModal" tabindex="-1" role="dialog" ria-labelledby="exampleModalLabel" aria-hidden="true">
        <div class="modal-dialog" role="document">
            <div class="modal-content">
                <div class="modal-header">
                    <img src="{{ asset('img/Logo-Halloworld.png') }}" alt="">
                    
                    <button type="button" class="close" data-dismiss="modal" aria-label="Close">
                    <span aria-hidden="true">&times;</span>
                    </button>
                </div>
                <div class="modal-body">
                    <div class="container">
                        <div class="row d-flex justify-content-center">
                            <div class="login_with__">     
                                <p>Log in with:</p>
                            </div>
                        </div>
                        <div class="row">
                            <div class="col-md-6">
                                <div class="btn__with-login-facebook">     
                                    <button type="submit" class="btn btn-facebook"> 
                                        <i class="fab fa-facebook mr-3 fa-2x" style="margin-left: -20px;"></i>
                                        <small style="font-size: 16px;">Facebook</small>
                                    </button>
                                </div>
                            </div>
                            <div class="col-md-6">
                                <div class="btn__with-login-google">     
                                    <button type="submit" class="btn btn-google"> 
                                        <i class="fab fa-google mr-3 fa-2x" style="margin-left: -20px;"></i>
                                        <small style="font-size: 16px;">Google</small>
                                    </button>
                                </div>
                            </div>
                        </div>
                        <div class="row white-color">
                            <div class="col-md-5 pr-0">
                                <hr>
                            </div>
                            <div class="col-md-2 pr-0 pl-0">
                                <div class="d-flex justify-content-center or__">
                                    <p class="text-white">Or</p>
                                </div>
                            </div>
                            <div class="col-md-5 pl-0">
                                <hr>
                            </div>
                        </div>
                        <div class="row">
                            <div id="background__login">
                                <input type="email" name="email" id="email" placeholder="Email" class="email__input form-control" style="padding-left: 15px;">
                                <input type="password" name="password" id="password" placeholder="Password" class="password__input form-control">

                                <div class="d-flex justify-content-center">
                                    <button type="submit" class="btn btn-masuk" style="margin-top: 25px;">Masuk</button>
                                </div>
                            </div>
                        </div>
                    </div>
                </div>
                <div class="modal-footer">
                    <a href="#">Lupa Password?</a>
                    
                </div>
            </div>
        </div>
    </div>
  <!--==========MODAL=======MODAL============MODAL===========MODAL================MODAL=======-->